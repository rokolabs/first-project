﻿using Dapper;
using Microsoft.Extensions.Logging;
using Rokolabs.Library.Dal.DTOs;
using Rokolabs.Library.Dal.Interfaces;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Rokolabs.Library.Dal
{
    public class NewspaperRepository : INewspaperRepository
    {
        private readonly ILogger<NewspaperRepository> _logger;
        private readonly DBOptions _options;

        public NewspaperRepository(ILogger<NewspaperRepository> logger, DBOptions options)
        {
            _logger = logger;
            _options = options;
        }

        public void DeleteNewspaper(int newspaperId)
        {
            using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
                var query = "DELETE FROM [Newspapers] WHERE [PaperID] = @newspaperId";
                db.Execute(query, new { newspaperId });
            }
        }

        public List<NewspaperDto> GetAllNewspapers()
        {
            using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
                return db.Query<NewspaperDto>("GetAllNewspapers",
                    commandType: CommandType.StoredProcedure
                    ).ToList();
            }
        }

        public List<NewspaperDto> GetAllNewspapersDateAsc()
        {
            using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
                return db.Query<NewspaperDto>("GetAllNewspapersDateASC",
                    commandType: CommandType.StoredProcedure
                    ).ToList();
            }
        }

        public List<NewspaperDto> GetAllNewspapersDateDesc()
        {
            using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
                return db.Query<NewspaperDto>("GetAllNewspapersDateDESC",
                    commandType: CommandType.StoredProcedure
                    ).ToList();
            }
        }

        public int InsertNewspaper(NewspaperDto newspaperDto)
        {
            using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
                var query = "INSERT INTO [Newspapers] ([Title], [CityID], [YearOfPublishing], [PublishingOfficeID], [PagesCount], [Description], [PaperNumber], [Date], [ISSN]) " +
                    "VALUES (@title, @cityID, @yearofpublishing, @publishingOfficeID, @pagesCount, @description, @paperNumber, @date, @issn); SELECT CAST(SCOPE_IDENTITY() as int);";
                return db.Query<int>(query, newspaperDto).FirstOrDefault();
            }
        }

        public List<NewspaperDto> SearchNewspapers(NewspaperDto newspaper)
        {
            using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
                return db.Query<NewspaperDto>("GetNewspapersByTitle",
                    new { newspaper.Title },
                    commandType: CommandType.StoredProcedure
                    ).ToList();
            }
        }

        public List<NewspaperDto> SearchNewspapersByPublicationOffice(string publishingOfficeName)
        {
            using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
                return db.Query<NewspaperDto>("GetNewspapersByPublishingOffice",
                    new { publishingOfficeName },
                    commandType: CommandType.StoredProcedure
                    ).ToList();
            }
        }

        public List<NewspaperDto> SearchNewspapersByPublicationOfficeDateAsc(string publishingOfficeName)
        {
            using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
                return db.Query<NewspaperDto>("GetNewspapersByPublishingOfficeDateASC",
                    new { publishingOfficeName },
                    commandType: CommandType.StoredProcedure
                    ).ToList();
            }
        }

        public List<NewspaperDto> SearchNewspapersByPublicationOfficeDateDesc(string publishingOfficeName)
        {
            using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
                return db.Query<NewspaperDto>("GetNewspapersByPublishingOfficeDateDESC",
                    new { publishingOfficeName },
                    commandType: CommandType.StoredProcedure
                    ).ToList();
            }
        }

        public List<NewspaperDto> SearchNewspapersDateAsc(NewspaperDto newspaper)
        {
            using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
                return db.Query<NewspaperDto>("GetNewspapersByTitleDateASC",
                    new { newspaper.Title },
                    commandType: CommandType.StoredProcedure
                    ).ToList();
            }
        }

        public List<NewspaperDto> SearchNewspapersDateDesc(NewspaperDto newspaper)
        {
            using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
                return db.Query<NewspaperDto>("GetNewspapersByTitleDateDESC",
                    new { newspaper.Title },
                    commandType: CommandType.StoredProcedure
                    ).ToList();
            }
        }

        public NewspaperDto GetNewspaper(NewspaperDto newspaper)
        {
            using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
                var query = "SELECT [Newspapers].[Title], [Newspapers].[Date], [Newspapers].[ISSN], [Newspapers].[PublishingOfficeID] FROM[dbo].[Newspapers] WHERE [Newspapers].[PublishingOfficeID] = @PublishingOfficeID AND [Newspapers].[Title] = @title AND [Newspapers].[Date] = @date";
                return db.Query<NewspaperDto>(query, newspaper).FirstOrDefault();
            }
        }

        public NewspaperDto GetNewspaperByISSN(NewspaperDto newspaper)
        {
            using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
                var query = "SELECT [Newspapers].[Title], [Newspapers].[Date], [Newspapers].[ISSN], [Cities].[PublishingOfficeID] FROM[dbo].[Newspapers] WHERE [PublishingOffices].[ISSN] = @ISSN";
                return db.Query<NewspaperDto>(query, newspaper).FirstOrDefault();
            }
        }
    }
}
