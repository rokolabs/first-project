﻿using Rokolabs.Library.Dal.DTOs;
using System.Collections.Generic;

namespace Rokolabs.Library.Dal.Interfaces
{
    public interface IPublishngOfficeRepository
    {
        List<PublishingOfficeDto> GetSuitablePublishingOffices(string name);
        int FindPublishingOffice(string publishingOfficeName);
        int InsertPublishingOffice(string publishingOfficeName);
    }
}
