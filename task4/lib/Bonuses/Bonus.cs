﻿namespace Lib
{
    public abstract class Bonus : BaseObject
    {
        public int Energy { get; set; }
        public bool Availiable { get; set; }
        public Bonus(int x, int y, int energy)
            : base(x, y)
        {
            Availiable = true;
            Energy = energy;
        }
        public Bonus()
        { }
    }
}
