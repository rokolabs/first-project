﻿using Rokolabs.Library.Data;
using System.Collections.Generic;

namespace Rokolabs.Library.Bll.Interfaces
{
    public interface IBookService
    {
        List<Book> GetAllBooks();
        List<Book> GetAllBooksDateAsc();
        List<Book> GetAllBooksDateDesc();
        int InsertBook(Book book);
        Book GetBook(Book book);
        Book GetBookByISBN(Book book);
        List<Book> SearchBook(Book book);
        List<Book> SearchBookDateAsc(Book book);
        List<Book> SearchBookDateDesc(Book book);
        void DeleteBook(int bookId);
        List<Book> SearchBookByAuthor(Author author);
        List<Book> SearchBookByAuthorDateAsc(Author author);
        List<Book> SearchBookByAuthorDateDesc(Author author);
        List<Book> SearchBooksByPublishingOffice(string publishingOfficeName);
        List<Book> SearchBooksByPublishingOfficeDateAsc(string publishingOfficeName);
        List<Book> SearchBooksByPublishingOfficeDateDesc(string publishingOfficeName);
    }
}
