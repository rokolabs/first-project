﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Rokolabs.Library.Data
{
    public class Patent : IComparable
    {
        public Patent()
        {
            Authors = new List<Author>();
        }

        public string Title { get; set; }
        public int CountryID { get; set; }
        public string Description { get; set; }

        [DisplayName("Country")]
        public string CountryName { get; set; }

        [DisplayName("Registration number")]
        public int PatentRegistrationNumber { get; set; }

        [DisplayName("Title")]
        public string FullName { 
            
            get
            {
                string name = $"«{Title}» от {PublicationDate:dd.MM.yyyy}";
                return name;
            }
        
        }

        [DisplayName("Application date")]
        [DataType (DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        public DateTime ApplicationDate
        {
            get
            {
                return applicationDate;
            }
            set
            {
                if (value > DateTime.Today)
                    applicationDate = DateTime.Today;
                else
                    applicationDate = value;
            }
        }

        [DisplayName("Publication date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        public DateTime PublicationDate
        {
            get
            {
                return publicationDate;
            }
            set
            {
                if (value < ApplicationDate)
                    publicationDate = ApplicationDate;
                else if (value > DateTime.Today)
                    publicationDate = DateTime.Today;
                else
                    publicationDate = value;
            }
        }

        [DisplayName("Pages")]
        public int PagesCount
        {
            get
            {
                return pagesCount;
            }
            set
            {
                if (value <= 0)
                    pagesCount = 1;
                else
                    pagesCount = value;
            }
        }

        [DisplayName("Id")]
        public int PatentID
        {
            get
            { return patentID; }
            set
            {
                if (value < 0)
                    patentID = 0;
                else
                    patentID = value;
            }
        }

        [DisplayName("Authors")]
        public string AuthorsInfo
        {
            get
            {
                if (authorsInfo == null)
                {
                    StringBuilder data = new StringBuilder();
                    for (int i = 0; i < Authors.Count; i++)
                    {
                        data.Append(Authors[0].FirstName[0] + ". " + Authors[i].LastName);
                        if (i != Authors.Count - 1)
                            data.Append(", ");
                    }
                    authorsInfo = data.ToString();
                }
                return authorsInfo;
            }
            set
            {
                authorsInfo = value;
            }
        }

        public List<Author> Authors { get; set; }
        private DateTime publicationDate;
        private DateTime applicationDate;
        private int pagesCount;
        private int patentID;
        private string authorsInfo;


        public int CompareTo(object obj)
        {
            Patent patent = obj as Patent;
            if (patent != null)
                return this.PublicationDate.Year.CompareTo(patent.PublicationDate.Year);
            else
                throw new Exception("Unable to compare two objects");
        }
    }
}
