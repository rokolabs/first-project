﻿using Rokolabs.Library.Dal.DTOs;
using System.Collections.Generic;

namespace Rokolabs.Library.Dal.Interfaces
{
    public interface INewspaperRepository
    {
        void DeleteNewspaper(int newspaperId);
        List<NewspaperDto> GetAllNewspapers();
        List<NewspaperDto> GetAllNewspapersDateAsc();
        List<NewspaperDto> GetAllNewspapersDateDesc();
        int InsertNewspaper(NewspaperDto newspaperDto);
        List<NewspaperDto> SearchNewspapers(NewspaperDto newspaper);
        List<NewspaperDto> SearchNewspapersDateAsc(NewspaperDto newspaper);
        List<NewspaperDto> SearchNewspapersDateDesc(NewspaperDto newspaper);
        List<NewspaperDto> SearchNewspapersByPublicationOffice(string publishingOfficeName);
        List<NewspaperDto> SearchNewspapersByPublicationOfficeDateAsc(string publishingOfficeName);
        List<NewspaperDto> SearchNewspapersByPublicationOfficeDateDesc(string publishingOfficeName);
        NewspaperDto GetNewspaper(NewspaperDto newspaper);
        NewspaperDto GetNewspaperByISSN(NewspaperDto newspaper);
    }
}
