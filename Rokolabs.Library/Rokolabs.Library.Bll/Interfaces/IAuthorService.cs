﻿using Rokolabs.Library.Data;
using System.Collections.Generic;

namespace Rokolabs.Library.Bll.Interfaces
{
    public interface IAuthorService
    {
        List<Author> GetAllAuthors();
        int InsertAuthor(Author author);
        void UpdateAuthor(int id, Author author);
        void DeleteAuthor(int id);
        List<Author> SearchAuthor(Author author);
        List<Author> SearchAuthors(string search);
        void AddAuthorsToBook(Book book);
        void AddAuthorsToPatent(Patent patent);
        Author GetAuthor(Author author);
    }
}
