﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace searcher
{

    class Program
    {
        delegate List<string> Search(string textExample, string[] text, ref int indexOfLine);
        delegate void Write(List<string> lines, string filename);

        static void Main(string[] args)
        {
            if (args.Length > 2)
            {
                string searchPath = args[0];
                string fileTemplate = args[1];
                Parameters consoleParameters = new Parameters();
                string[] texts;
                string partOfPath;
                Search searcher = SearchText;
                Write writer = ConsoleWrite;
                if (args.Length == 2)
                    PrintHelp();
                else
                {
                    AnalyzeConsole(args, ref consoleParameters.logName, ref consoleParameters.textTemplate, ref consoleParameters.isRegular);
                    
                    if (consoleParameters.isRegular)
                        searcher = SearchTextWithRegularExpression;
                    
                    if (consoleParameters.logName != string.Empty)
                        writer = LogWrite;
                    
                    List<string> directoriesNames;
                    try
                    {
                        string[] loadedFiles = SearchFiles(searchPath, fileTemplate, out directoriesNames, consoleParameters.isRegular, consoleParameters.logName);
                        if (loadedFiles.Length != 0)
                        {
                            if (File.Exists(Path.Combine(searchPath, consoleParameters.logName)))
                                File.Delete(Path.Combine(searchPath, consoleParameters.logName));
                            int indexOfLine = 0;
                            for (int i = 0; i < loadedFiles.Length; i++)
                            {
                                texts = GetText(Path.Combine(directoriesNames[i], loadedFiles[i]));
                                List<string> searchResults = searcher(consoleParameters.textTemplate, texts,  ref indexOfLine);
                                if (searchResults.Capacity != 0)
                                {
                                    partOfPath = Path.Combine(directoriesNames[i], loadedFiles[i]).Replace(searchPath + "\\", "");
                                    writer(new List<string> { $"File: {partOfPath}" }, consoleParameters.logName);
                                    writer(searchResults, consoleParameters.logName);
                                }
                            }
                        }
                        else
                            writer(new List<string> { "File not found" }, consoleParameters.logName);
                    }
                    catch (FileNotFoundException)
                    { 
                        Console.WriteLine("Directory not found"); 
                    }
                    catch (UnauthorizedAccessException)
                    {
                        Console.WriteLine("Access to the path is denied");
                    }
                    catch (DirectoryNotFoundException)
                    {
                        Console.WriteLine("Directory not found");
                    }
                    catch (IOException)
                    {
                        Console.WriteLine("One of the files is locked");
                    }
                }
            }
            else
                PrintHelp();
        }

        public static string[] SearchFiles(string searchPath, string fileTemplate, out List<string> directoriesNames, bool isRegular, string logName)
        {
            directoriesNames = new List<string>();
            Write writer = ConsoleWrite;
            DirectoryInfo info = new DirectoryInfo(searchPath);
            info.Attributes = FileAttributes.Normal;
            var files = info.GetFiles(fileTemplate, SearchOption.AllDirectories);
            string[] fileNames = new string[files.Length];
            for (int i = 0; i < files.Length; i++)
            {
                fileNames[i] = files[i].Name;
                directoriesNames.Add(files[i].DirectoryName);
            }
            return fileNames;
        }

        public static string[] GetText(string fileName)
        {
            string[] text = File.ReadAllLines(fileName);
            return text;
        }

        public static List<string> SearchTextWithRegularExpression(string textExample, string[] text, ref int indexOfLine)
        {
            Regex re = new Regex(textExample);
            List<string> searchResults = new List<string>();
            for (int line = 0; line < text.Length; line++)
            {
                var matches = re.Matches(text[line]);
                foreach (Match item in matches)
                {
                    searchResults.Add($"{indexOfLine+1}. String: {line+1}, Position: {item.Index+1}, Result: {text[line]}");
                    indexOfLine++;
                }
            }
            return searchResults;
        }

        public static List<string> SearchText(string textExample, string[] text, ref int indexOfLine)
        {
            int indexOfEnter = 0;
            List<string> searchResults = new List<string>();
            for (int i = 0; i < text.Length; i++)
            {
                while (indexOfEnter < text[i].Length)
                {
                    indexOfEnter = text[i].IndexOf(textExample, indexOfEnter);
                    if (indexOfEnter==-1)
                    {
                        indexOfEnter = 0;
                        break;
                    }
                    searchResults.Add($"{indexOfLine+1}. String: {i+1}, Position: {indexOfEnter+1}, Result: {text[i]}");
                    indexOfEnter += indexOfEnter + textExample.Length;
                    indexOfLine++;
                }
            }
            return searchResults;
        }

        public static void AnalyzeConsole(string[] args, ref string logName, ref string textTemplate, ref bool isRegular)
        {
            string[] argsCopy = new string[args.Length];
            args.CopyTo(argsCopy, 0);
            for (int i = 2; i < argsCopy.Length; i++)
            {
                if (argsCopy[i] == "-l")
                {
                    logName = args[i + 1];
                    argsCopy[i] = String.Empty;
                    argsCopy[i + 1] = String.Empty;
                    continue;
                }
                if (argsCopy[i] == "-r")
                {
                    textTemplate = argsCopy[i + 1];
                    argsCopy[i] = String.Empty;
                    argsCopy[i + 1] = String.Empty;
                    isRegular = true;
                }
                if (argsCopy[i] == "-h" || argsCopy[i] == "-help")
                {
                    PrintHelp();
                    argsCopy[i] = String.Empty;
                }
                if (argsCopy[i] == String.Empty)
                    continue;
                else
                {
                    textTemplate = argsCopy[i];
                }
            }
        }

        public static void LogWrite(List<string> logs, string filename)
        {
            using (StreamWriter sw = new StreamWriter(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, filename), true))
            {
                foreach (var item in logs)
                {
                    sw.WriteLine(item);
                    Console.WriteLine(item);
                }
            }
        }

        public static void ConsoleWrite(List<string> records, string filename)
        {
            foreach (var item in records)
            {
                Console.WriteLine(item);
            }
        }

        public static void PrintHelp()
        {
            Console.WriteLine("[ HELP ]");
            Console.WriteLine("For searching files enter path, filename extension and what you want to find");
            Console.WriteLine("These commands can be in any order");
            Console.WriteLine("SOME TEXT                 text you want to find");
            Console.WriteLine("-r REGULAR EXPRESSION     enter your regular expression");
            Console.WriteLine("-l FILENAME               log filename");
        }
    }
    public class Parameters
    {
        public string textTemplate;
        public string logName;
        public bool isRegular;

        public Parameters()
        {
            textTemplate = string.Empty;
            logName = string.Empty;
            isRegular = false;
        }
    }
}
