﻿using NUnit.Framework;
using Rokolabs.Library.Data;
using System;

namespace LibraryTests
{
    public class NewspaperTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestNewspaperYearTest()
        {
            Newspaper newspaper = new Newspaper();
            newspaper.YearOfPublishing = 2042;
            short year = (short)DateTime.Today.Year;
            Assert.AreEqual(newspaper.YearOfPublishing, year);
        }
        [Test]
        public void TestNewspaperDateTest()
        {
            Newspaper newspaper = new Newspaper();
            newspaper.Date = new DateTime(2021, 8, 12);
            newspaper.YearOfPublishing = 2020;
            Assert.AreEqual((short)newspaper.Date.Year, newspaper.YearOfPublishing);
        }

        [Test]
        public void TestNewspaperPages()
        {
            Newspaper newspaper = new Newspaper();
            newspaper.PagesCount = -10;
            Assert.True(newspaper.PagesCount >= 0);
        }
    }
}
