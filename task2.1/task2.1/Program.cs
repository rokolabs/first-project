﻿using JournalMaker;
using ExternalFilesWorker;
using LogWorker;
using System;
using System.Collections.Generic;
using System.IO;


namespace task2._1
{
    class Program
    {
        public static string pathToBackup;
        public static string pathToSource;
        public static JournalLog jl;

        static void Main(string[] args)
        {
            Console.Write("Select work mode: (w) - watching, (r) - restore: ");
            string answer = Console.ReadLine();
            AnalyzeAnswer(answer);
            Console.WriteLine("Press enter to exit.");
            Console.ReadLine();
        }

        private static void AnalyzeAnswer(string answer)
        {
            switch (answer)
            {
                case "w":
                    Watch();
                    break;
                case "r":
                    Restore("HistoryLog.txt");
                    break;
                default:
                    Console.WriteLine("Incorrect input");
                    break;
            }
        }

        private static void Watch()
        {
            Console.Write("Enter directory you want to watch: ");
            pathToSource = Console.ReadLine();
            Console.Write("Enter directory for backups: ");
            pathToBackup = Console.ReadLine();
            
            if (!Directory.Exists(pathToSource))
            {
                Console.WriteLine("Directory doesn't exists");
                return;
            }
            if (!Directory.Exists(pathToBackup))
                Directory.CreateDirectory(pathToBackup);
            
            FileInfo[] files = Search.GetAllFiles(pathToSource);
            if (!File.Exists(Path.Combine(pathToBackup, "HistoryLog.txt")))
            {
                jl = new JournalLog(pathToBackup, pathToSource);
                jl.CreateFirstRecord(files);
                SaverToLogFile.SaveJournal(jl, pathToBackup);
                BackupCreator.CreateFilesBackup(jl.Records[^1], pathToBackup);
            }
            else
            {
                jl = LoaderFromLogFile.LoadJournal(pathToBackup, "HistoryLog.txt", pathToSource);
            }
            var watcher = new FileSystemWatcher(pathToSource);
            watcher.NotifyFilter = NotifyFilters.Attributes
                                    | NotifyFilters.CreationTime
                                    | NotifyFilters.DirectoryName
                                    | NotifyFilters.FileName
                                    | NotifyFilters.LastAccess
                                    | NotifyFilters.LastWrite
                                    | NotifyFilters.Security
                                    | NotifyFilters.Size;

            watcher.Changed += OnChanged;
            watcher.Created += OnCreated;
            watcher.Deleted += OnDeleted;
            watcher.Renamed += OnRenamed;
            watcher.Error += OnError;
            watcher.Filter = "*.txt";
            watcher.IncludeSubdirectories = true;
            watcher.EnableRaisingEvents = true;
        }

        private static void Restore(string journalName)
        {
            Console.Write("Enter directory you watched: ");
            pathToSource = Console.ReadLine();
            Console.Write("Enter directory with backups: ");
            pathToBackup = Console.ReadLine();

            if (!Directory.Exists(pathToSource) || !Directory.Exists(pathToBackup))
            {
                Console.WriteLine("Directory doesn't exists");
                return;
            }
            if (!File.Exists(Path.Combine(pathToBackup, journalName)))
            {
                Console.WriteLine("Journal log file doesn't exists");
                return;
            }
            jl = LoaderFromLogFile.LoadJournal(pathToBackup, journalName, pathToSource);
            DateTime dt = new DateTime();
            Console.WriteLine("Enter date and time in format dd-mm-yyyy hh:mm AM/PM or dd/mm/yyyy hh:mm AM/PM: ");
            string enteredDateTime = Console.ReadLine();
            try
            {
                dt = DateTime.Parse(enteredDateTime);
                JournalRecord jr = FileRestorer.FindFilesForRestore(dt, jl);
                FileRestorer.RestoreFiles(jr, jl.JournalPath);
                jl.Records.Add(jr);
                SaverToLogFile.AddRecordToLog(pathToBackup, jl);
            }
            catch (FormatException)
            {
                Console.WriteLine("Invalid date and time");
            }
        }

        private static void OnChanged(object sender, FileSystemEventArgs e)
        {
            if (e.ChangeType != WatcherChangeTypes.Changed)
            {
                return;
            }
            jl.CreateRecordForChangingFile(e.FullPath);
            SaverToLogFile.AddRecordToLog(pathToBackup, jl);
            BackupCreator.CreateFilesBackup(jl.Records[^1], pathToBackup);
        }

        private static void OnCreated(object sender, FileSystemEventArgs e)
        {
            jl.CreateRecordForCreatingFile(e.FullPath);
            SaverToLogFile.AddRecordToLog(pathToBackup, jl);
            BackupCreator.CreateFilesBackup(jl.Records[^1], pathToBackup);
        }

        private static void OnDeleted(object sender, FileSystemEventArgs e)
        {
            jl.CreateRecordForDeletingFile(e.FullPath);
            SaverToLogFile.AddRecordToLog(pathToBackup, jl);
            BackupCreator.CreateFilesBackup(jl.Records[^1], pathToBackup);
        }


        private static void OnRenamed(object sender, RenamedEventArgs e)
        {
            jl.CreateRecordForRenamingFile(e.OldFullPath, e.FullPath);
            SaverToLogFile.AddRecordToLog(pathToBackup, jl);
            BackupCreator.CreateFilesBackup(jl.Records[^1], pathToBackup);
        }

        private static void OnError(object sender, ErrorEventArgs e) =>
            PrintException(e.GetException());

        private static void PrintException(Exception? ex)
        {
            if (ex != null)
            {
                Console.WriteLine($"Message: {ex.Message}");
            }
        }
    }
}
