﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Rokolabs.Library.Bll.Interfaces;
using Rokolabs.Library.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Rokolabs.Library.UIConsole
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private readonly IAuthorService _authorService;
        private readonly IBookService _bookService;
        private readonly IPatentService _patentService;
        private readonly ICityService _cityService;
        private readonly ICountryService _countryService;
        private readonly IPublishingOfficeService _publishingOfficeService;
        private readonly INewspaperService _newspaperService;
        private readonly Drawer _drawer;
        private readonly Validator _validator;

        public Worker(ILogger<Worker> logger, IAuthorService authorService, IBookService bookService, ICityService cityService, 
            IPatentService patentService, IPublishingOfficeService publishingOfficeService, INewspaperService newspaperService, ICountryService countryService)
        {
            _logger = logger;
            _authorService = authorService;
            _bookService = bookService;
            _cityService = cityService;
            _countryService = countryService;
            _patentService = patentService;
            _publishingOfficeService = publishingOfficeService;
            _newspaperService = newspaperService;
            _drawer = new Drawer(_logger);
            _validator = new Validator(_authorService, _bookService, _patentService, _cityService, _countryService, _publishingOfficeService, _newspaperService, _drawer, _logger);
        }
        
        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            int key;
            do
            {
                _drawer.DrawMainMenu();
                key = _drawer.CheckSelectedItem(_drawer.MainMenuStartPosition, _drawer.MainMenuEndPosition);
                AnalyzeChoiseInMainMenu(key, _drawer);
            }
            while (key != 0);
            return Task.CompletedTask;
        }

        private void ShowAuthors(List<Author> authors, string indent="")
        {
            foreach (var author in authors)
            {
                Console.WriteLine($"{indent}{author.FirstName,10}. {author.LastName,10} | {author.Description}");
            }
        }

        private void ShowBooks(List<Book> books, string indent = "")
        {
            foreach (var book in books)
            {
                string authors = String.Empty;
                if (book.Authors is not null)
                {
                    for (int i = 0; i < book.Authors.Count; i++)
                    {
                        authors += book.Authors[i].FirstName[0] + ". " + book.Authors[i].LastName + "";
                        if (i != book.Authors.Count - 1)
                            authors += ", ";
                    }
                }
                Console.WriteLine($"{indent}{authors} - {book.Title} ({book.YearOfPublishing})");
            }
        }

        private void ShowBooksGroupedByYear(List<Book> books, string indent = "")
        {
            books.Sort();
            short year = 0;
            foreach (var book in books)
            {
                string authors = String.Empty;
                if (book.Authors is not null)
                {
                    for (int i = 0; i < book.Authors.Count; i++)
                    {
                        authors += book.Authors[i].FirstName[0] + ". " + book.Authors[i].LastName + "";
                        if (i != book.Authors.Count - 1)
                            authors += ", ";
                    }
                }
                if (year != book.YearOfPublishing)
                {
                    year = book.YearOfPublishing;
                    Console.WriteLine($"Books by {year}");
                }
                Console.WriteLine($"{indent}{authors} - {book.Title} ({book.YearOfPublishing})");
            }
        }

        private void ShowNewspapers(List<Newspaper> newspapers, string indent = "")
        {
            foreach (var newspaper in newspapers)
            {
                Console.WriteLine($"{indent}{newspaper.Title} #{newspaper.PaperNumber}/{newspaper.YearOfPublishing}");
            }
        }

        private void ShowNewspapersGroupedByYear(List<Newspaper> newspapers, string indent = "")
        {
            newspapers.Sort();
            short year = 0;
            foreach (var newspaper in newspapers)
            {
                if (year != newspaper.YearOfPublishing)
                {
                    year = newspaper.YearOfPublishing;
                    Console.WriteLine($"Newspapers by {year}");
                }
                Console.WriteLine($"{indent}{newspaper.Title} #{newspaper.PaperNumber}/{newspaper.YearOfPublishing}");
            }
        }

        private void ShowPatents(List<Patent> patents, string indent = "")
        {
            foreach (var patent in patents)
            {
                Console.WriteLine($"{indent}{patent.Title} - {patent.ApplicationDate.ToShortDateString()}");
            }
        }

        private void ShowPatentsGroupedByYear(List<Patent> patents, string indent = "")
        {
            patents.Sort();
            short year = 0;
            foreach (var patent in patents)
            {
                if (year != (short)patent.PublicationDate.Year)
                {
                    year = (short)patent.PublicationDate.Year;
                    Console.WriteLine($"Patents by {patent}");
                }
                Console.WriteLine($"{indent}{patent.Title} - {patent.ApplicationDate.ToShortDateString()}");
            }
        }

        public void AnalyzeChoiseInMainMenu(int position, Drawer drawer)
        {
            int key;
            switch (position)
            {
                case 1:
                    drawer.DrawAddRecord();
                    key = drawer.CheckSelectedItem(drawer.MainMenuStartPosition, 4);
                    Console.Clear();
                    ChooseAddingProfile(key, drawer);
                    break;
                case 2:
                    drawer.DrawDeleteRecord();
                    key = drawer.CheckSelectedItem(drawer.MainMenuStartPosition, 4);
                    Console.Clear();
                    ChooseDeletingProfile(key, drawer);
                    break;
                case 3:
                    drawer.DrawViewCatalog();
                    key = drawer.CheckSelectedItem(drawer.MainMenuStartPosition, 3);
                    Console.Clear();
                    ChooseView(key, drawer);
                    break;
                case 4:
                    drawer.DrawSearchByTitle();
                    SearchPublicationByTitle(drawer);
                    break;
                case 5:
                    drawer.DrawSearchByAuthor();
                    SearchPublicationByAuthor(drawer);
                    break;
                case 6:
                    drawer.DrawSearhByPublishingOffice();
                    SearchPublicationByPublishingOffice(drawer);
                    break;
                default:
                    break;
            }
        }

        private void SearchPublicationByPublishingOffice(Drawer drawer)
        {
            Console.WriteLine("Enter the name of the publisher");
            string publishingOfficesName = Console.ReadLine();
            List<PublishingOffice> publishingOffices = _publishingOfficeService.GetSuitablePublishingOffices(publishingOfficesName);
            var books = new List<Book>();
            var newspapers = new List<Newspaper>();
            drawer.DrawViewMode();
            var sortMode = drawer.CheckSelectedItem(1, 3);
            Console.SetCursorPosition(0, 4);
            drawer.DrawGroupMode();
            var groupMode = drawer.CheckSelectedItem(5, 2);
            Console.Clear();
            switch (sortMode)
            {
                case 1:
                    foreach (var office in publishingOffices)
                    {
                        Console.WriteLine($"Publications by {office.PublishingOfficeName}");
                        books = _bookService.SearchBooksByPublishingOfficeDateAsc(office.PublishingOfficeName);
                        newspapers = _newspaperService.SearchNewspaperByPublicationOfficeDateAsc(office.PublishingOfficeName);
                    }
                    break;
                case 2:
                    foreach (var office in publishingOffices)
                    {
                        Console.WriteLine($"Publications by {office.PublishingOfficeName}");
                        books = _bookService.SearchBooksByPublishingOfficeDateDesc(office.PublishingOfficeName);
                        newspapers = _newspaperService.SearchNewspaperByPublicationOfficeDateDesc(office.PublishingOfficeName);
                    }
                    break;
                default:
                    foreach (var office in publishingOffices)
                    {
                        Console.WriteLine($"Publications by {office.PublishingOfficeName}");
                        books = _bookService.SearchBooksByPublishingOfficeDateDesc(office.PublishingOfficeName);
                        newspapers = _newspaperService.SearchNewspaperByPublicationOfficeDateDesc(office.PublishingOfficeName);
                    }
                    break;
            }
            if (groupMode == 1)
            {
                ShowBooksGroupedByYear(books);
                ShowNewspapersGroupedByYear(newspapers);
            }
            else
            {
                Console.WriteLine("Books:");
                ShowBooks(books);
                Console.WriteLine("Newspapers:");
                ShowNewspapers(newspapers);
            }
            drawer.DrawPressAnyKey();
        }

        private void SearchPublicationByAuthor(Drawer drawer)
        {
            Console.WriteLine("Fill in the following fields");
            var author = drawer.GetInfoAboutAuthor();
            List<Book> books = new List<Book>();
            List<Patent> patents = new List<Patent>();
            drawer.DrawViewMode();
            var sortMode = drawer.CheckSelectedItem(1, 3);
            Console.SetCursorPosition(0,4);
            drawer.DrawAuthorsPublicationsVariants();
            var publicationsType = drawer.CheckSelectedItem(5, 3);
            Console.SetCursorPosition(0, 8);
            drawer.DrawGroupMode();
            var groupMode = drawer.CheckSelectedItem(9, 2);
            switch (sortMode, publicationsType)
            {
                case (1, 1):
                    books = _bookService.SearchBookByAuthorDateAsc(author);
                    break;
                case (1, 2):
                    patents = _patentService.SearchPatentByAuthorDateAsc(author);
                    break;
                case (1, 3):
                    books = _bookService.SearchBookByAuthorDateAsc(author);
                    patents = _patentService.SearchPatentByAuthorDateAsc(author);
                    break;
                case (2, 1):
                    books = _bookService.SearchBookByAuthorDateDesc(author);
                    break;
                case (2, 2):
                    patents = _patentService.SearchPatentByAuthorDateDesc(author);
                    break;
                case (2, 3):
                    books = _bookService.SearchBookByAuthorDateDesc(author);
                    patents = _patentService.SearchPatentByAuthorDateDesc(author);
                    break;
                case (3, 1):
                    books = _bookService.SearchBookByAuthor(author);
                    break;
                case (3, 2):
                    patents = _patentService.SearchPatentByAuthor(author);
                    break;
                default:
                    books = _bookService.SearchBookByAuthor(author);
                    patents = _patentService.SearchPatentByAuthor(author);
                    break;
            }
            Console.Clear();
            Console.WriteLine($"Publications by {author.FirstName} {author.LastName}");
            if (groupMode == 1)
            {
                ShowBooksGroupedByYear(books);
                ShowPatentsGroupedByYear(patents);
            }
            else
            {
                ShowBooks(books);
                ShowPatents(patents);
            }
            drawer.DrawPressAnyKey();
        }

        private void SearchPublicationByTitle(Drawer drawer)
        {
            Console.WriteLine("Fill in the following fields");
            Console.Write("Title: ");
            string title = Console.ReadLine();
            List<Book> books;
            List<Patent> patents;
            drawer.DrawViewMode();
            var sortMode = drawer.CheckSelectedItem(1, 3);
            Console.SetCursorPosition(0, 4);
            drawer.DrawGroupMode();
            var groupMode = drawer.CheckSelectedItem(5, 2);
            switch (sortMode)
            {
                case 1:
                    books = _bookService.SearchBookDateAsc(new Book() { Title = title });
                    patents = _patentService.SearchPatentDateAsc(new Patent() { Title = title });
                    break;
                case 2:
                    books = _bookService.SearchBookDateDesc(new Book() { Title = title });
                    patents = _patentService.SearchPatentDateDesc(new Patent() { Title = title }); break;
                default:
                    books = _bookService.SearchBook(new Book() { Title = title });
                    patents = _patentService.SearchPatent(new Patent() { Title = title });
                    break;
            }
            Console.Clear();
            if (groupMode == 1)
            {
                ShowBooksGroupedByYear(books);
                ShowPatentsGroupedByYear(patents);
            }
            else
            {
                Console.WriteLine("Books in library: ");
                ShowBooks(books);
                Console.WriteLine("Patents in library: ");
                ShowPatents(patents);
            }
            drawer.DrawPressAnyKey();
        }

        private void ChooseDeletingProfile(int key, Drawer drawer)
        {
            string title;
            switch (key)
            {
                case 1:
                    var author = drawer.GetInfoForDeletingAuthor();
                    List<Author> authors = _authorService.SearchAuthor(author);
                    ShowAuthors(authors, "  ");
                    drawer.DrawGoBackFooter();
                    key = drawer.CheckSelectedItem(4, authors.Count+1);
                    if (key <= authors.Count)
                    {
                        _authorService.DeleteAuthor(authors[key - 1].AuthorID);
                        drawer.DrawDeleteMessage(authors.Count + 5, authors[key - 1].FirstName + " " + authors[key - 1].LastName);
                        Console.ReadKey();
                    }
                    Console.Clear();
                    break;
                case 2:
                    title = drawer.DrawDeletePublication("book");
                    List<Book> books = _bookService.SearchBook(new Book() { Title = title});
                    ShowBooks(books, "  ");
                    drawer.DrawGoBackFooter();
                    key = drawer.CheckSelectedItem(3, books.Count + 1);
                    if (key <= books.Count)
                    {
                        _bookService.DeleteBook(books[key - 1].BookId);
                        drawer.DrawDeleteMessage(books.Count + 5, books[key - 1].Title);
                    }
                    Console.Clear();
                    break;
                case 3:
                    title = drawer.DrawDeletePublication("patent");
                    List<Patent> patents = _patentService.SearchPatent(new Patent() { Title = title });
                    ShowPatents(patents, "  ");
                    drawer.DrawGoBackFooter();
                    key = drawer.CheckSelectedItem(3, patents.Count + 1);
                    if (key <= patents.Count)
                    {
                        _patentService.DeletePatent(patents[key - 1].PatentID);
                        drawer.DrawDeleteMessage(patents.Count+5, patents[key - 1].Title);
                    }
                    Console.Clear();
                    break;
                case 4:
                    title = drawer.DrawDeletePublication("newspaper");
                    List<Newspaper> newspapers = _newspaperService.SearchNewspaper(new Newspaper() { Title = title });
                    ShowNewspapers(newspapers, "  ");
                    drawer.DrawGoBackFooter();
                    key = drawer.CheckSelectedItem(3, newspapers.Count + 1);
                    if (key <= newspapers.Count)
                    {
                        _newspaperService.DeleteNewspaper(newspapers[key - 1].PaperID);
                        drawer.DrawDeleteMessage(newspapers.Count + 5, newspapers[key - 1].Title);
                    }
                    Console.Clear();
                    break;
                default:
                    break;
            }
        }

        private void ChooseAddingProfile(int key, Drawer drawer)
        {
            switch (key)
            {
                case 1:
                    var author = drawer.GetInfoForAddingAuthor();
                    _validator.ValidateAuthor(author);
                    drawer.DrawPressAnyKey();
                    break;
                case 2:
                    var book = drawer.GetInfoForAddingBook();
                    AddAuthorsToBook(drawer, book);
                    if (_validator.ValidateBook(book))
                        _authorService.AddAuthorsToBook(book);
                    drawer.DrawPressAnyKey();
                    break;
                case 3:
                    var patent = drawer.GetInfoForAddingPatent();
                    AddAuthorsToPatent(drawer, patent);
                    if (_validator.ValidatePatent(patent))
                        _authorService.AddAuthorsToPatent(patent);
                    drawer.DrawPressAnyKey();
                    break;
                case 4:
                    var newspaper = drawer.AddNewspaper();
                    _validator.ValidateNewspaper(newspaper);
                    drawer.DrawPressAnyKey();
                    break;
                default:
                    break;
            }
        }

        private void AddAuthorsToBook(Drawer drawer, Book book)
        {
            ConsoleKeyInfo key;
            do
            {
                Console.Clear();
                Console.WriteLine($"Adding authors to book {book.Title}");
                var author = drawer.GetInfoAboutAuthor();
                var authors = _authorService.SearchAuthor(author);
                if (authors.Count == 0)
                {
                    Console.WriteLine("There is no such author in the database");
                    Console.WriteLine("Please, add descriprion for adding the author to database:");
                    Console.Write("Description: ");
                    author.Description = Console.ReadLine();
                    author.AuthorID = _authorService.InsertAuthor(author);
                    book.Authors.Add(author);
                }
                else
                {
                    ShowAuthors(authors);
                    var selectedAuthor = drawer.CheckSelectedItem(3, authors.Count);
                    book.Authors.Add(authors[selectedAuthor - 1]);
                }
                Console.WriteLine("Press ESC to stop");
                key = Console.ReadKey();
            }
            while (key.Key != ConsoleKey.Escape);
            Console.WriteLine();
        }

        private void AddAuthorsToPatent(Drawer drawer, Patent patent)
        {
            ConsoleKeyInfo key;
            do
            {
                Console.Clear();
                Console.WriteLine($"Adding authors to patent {patent.Title}");
                var author = drawer.GetInfoAboutAuthor();
                var authors = _authorService.SearchAuthor(author);
                if (authors.Count == 0)
                {
                    Console.WriteLine("There is no such author in the database");
                    Console.WriteLine("Please, add descriprion for adding the author to database:");
                    Console.Write("Description: ");
                    author.Description = Console.ReadLine();
                    author.AuthorID = _authorService.InsertAuthor(author);
                    patent.Authors.Add(author);
                }
                else
                {
                    ShowAuthors(authors);
                    var selectedAuthor = drawer.CheckSelectedItem(3, authors.Count);
                    patent.Authors.Add(authors[selectedAuthor - 1]);
                    Console.SetCursorPosition(0, 3 + authors.Count);
                }
                Console.WriteLine("\nPress ESC to stop");
                key = Console.ReadKey();
            }
            while (key.Key != ConsoleKey.Escape);
            Console.WriteLine();
        }

        private void ChooseView(int key, Drawer drawer)
        {
            drawer.DrawViewMode();
            var sortMode = drawer.CheckSelectedItem(1,3);
            Console.SetCursorPosition(0, 4);
            drawer.DrawGroupMode();
            var groupMode = drawer.CheckSelectedItem(5, 2);
            switch (key)
            {
                case 1:
                    List<Patent> patents = LoadSortedPatents(sortMode);
                    Console.WriteLine("Patents in library:\n");
                    if (groupMode == 1)
                        ShowPatentsGroupedByYear(patents);
                    else
                        ShowPatents(patents);
                    drawer.DrawPressAnyKey();
                    break;
                case 2:

                    List<Book> books = LoadSortedBooks(sortMode);
                    Console.WriteLine("Books in library:\n");
                    if (groupMode == 1)
                        ShowBooksGroupedByYear(books);
                    else
                        ShowBooks(books);
                    drawer.DrawPressAnyKey();
                    break;
                case 3:
                    List<Newspaper> newspapers = LoadSortedNewspapers(sortMode);
                    Console.WriteLine("Newspapers in library:\n");
                    if (groupMode == 1)
                        ShowNewspapersGroupedByYear(newspapers);
                    else
                        ShowNewspapers(newspapers);
                    drawer.DrawPressAnyKey();
                    break;
                default:
                    break;
            }
        }

        private List<Newspaper> LoadSortedNewspapers(int sortMode)
        {
            List<Newspaper> newspapers;
            switch (sortMode)
            {
                case 1:
                    newspapers = _newspaperService.GetAllNewspapersDateAsc();
                    break;
                case 2:
                    newspapers = _newspaperService.GetAllNewspapersDateDesc();
                    break;
                default:
                    newspapers = _newspaperService.GetAllNewspapers();
                    break;
            }
            Console.Clear();
            return newspapers;
        }

        private List<Book> LoadSortedBooks(int sortMode)
        {
            List<Book> books;
            switch (sortMode)
            {
                case 1:
                    books = _bookService.GetAllBooksDateAsc();
                    break;
                case 2:
                    books = _bookService.GetAllBooksDateDesc();
                    break;
                default:
                    books = _bookService.GetAllBooks();
                    break;
            }
            Console.Clear();
            return books;
        }

        private List<Patent> LoadSortedPatents(int sortMode)
        {
            List<Patent> patents;
            switch (sortMode)
            {
                case 1:
                    patents = _patentService.GetAllPatentsDateAsc();
                    break;
                case 2:
                    patents = _patentService.GetAllPatentsDateDesc();
                    break;
                default:
                    patents = _patentService.GetAllPatents();
                    break;
            }
            Console.Clear();
            return patents;
        }
    }
}
