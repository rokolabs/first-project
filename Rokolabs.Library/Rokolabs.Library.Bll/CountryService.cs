﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using Rokolabs.Library.Bll.Interfaces;
using Rokolabs.Library.Dal.Interfaces;

namespace Rokolabs.Library.Bll
{
    public class CountryService : ICountryService
    {
        private readonly ILogger<CountryService> _logger;
        private readonly ICountryRepository _countryRepository;
        private readonly IMapper _mapper;

        public CountryService(ILogger<CountryService> logger, IMapper mapper, ICountryRepository countryRepository)
        {
            _logger = logger;
            _countryRepository = countryRepository;
            _mapper = mapper;
        }

        public int FindThisEntity(string name)
        {
            return _countryRepository.FindCountry(name);
        }

        public int InsertThisEntity(string name)
        {
            return _countryRepository.InsertCountry(name);
        }
    }
}
