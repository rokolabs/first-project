﻿using System;
using System.IO;

namespace Lib
{
    public class LevelGenerator
    {
        private static readonly Random _rnd = new Random();

        public enum GameLevels
        {
            Level1,
            Level2,
            Level3,
            Level4,
            Level5
        }

        public LevelGenerator()
        {}

        public string CreateLevelFile(int height, int width, int level)
        {
            int foxIndex = (int)BaseObject.ObjectTypes.Fox;
            string levelname = "Level" + level + ".txt";
            using (StreamWriter sw = new StreamWriter(Path.Combine(Environment.CurrentDirectory, levelname)))
            {
                sw.WriteLine("map");
                sw.WriteLine($"{height},{width}");
                sw.WriteLine("player");
                sw.WriteLine("1,1");
                int objectIndex;
                int RandomDifference = (int)(100 * (1 - level / 10.0));
                for (int i = 2; i < width - 1; i++)
                {
                    for (int j = 2; j < height - 1; j++)
                    {
                        objectIndex = _rnd.Next(RandomDifference);
                        if ((objectIndex >= 0 && objectIndex < foxIndex) || (objectIndex > foxIndex && objectIndex <= 11))
                        {
                            sw.WriteLine($"{(BaseObject.ObjectTypes)objectIndex}");
                            sw.WriteLine($"{i},{j}");
                        }
                    }
                }
                sw.WriteLine("fox");
                sw.WriteLine($"{width},{height}");
            }
            return levelname;
        }
    }
}
