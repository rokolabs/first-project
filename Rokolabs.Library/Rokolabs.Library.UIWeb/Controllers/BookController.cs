﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Rokolabs.Library.Bll.Interfaces;
using Rokolabs.Library.Data;
using Sentry;
using System;
using System.Collections.Generic;

namespace Rokolabs.Library.UIWeb.Controllers
{
    public class BookController : Controller
    {
        private readonly ILogger<BookController> _logger;
        private readonly IBookService _bookService;
        private readonly ICityService _cityService;
        private readonly IPublishingOfficeService _publishingOfficeService;
        private readonly IHub _hub;
        
        public BookController(ILogger<BookController> logger, IBookService bookService, ICityService cityService, IPublishingOfficeService publishingOfficeService, IHub hub)
        {
            _logger = logger;
            _bookService = bookService;
            _cityService = cityService;
            _publishingOfficeService = publishingOfficeService;
            _hub = hub;
        }

        // GET: BookController
        public ActionResult Index()
        {
            //var books = _bookService.GetAllBooks();
            List<Book> books = default(List<Book>);
            Wrapper.Watch(() => { books = _bookService.GetAllBooks(); }, _hub, "load-all-books", "transaction");
            return View(books);
        }

        [HttpPost]
        public ActionResult Index(Book book, int[] authors)
        {
            List<Book> books = default(List<Book>);
            if (book.Title == null)
                books = _bookService.GetAllBooks();
            else
            {
                //books = _bookService.SearchBook(book);
                Wrapper wrapper = new Wrapper();
                books = wrapper.Watch(() => _bookService.SearchBook(book), _hub, "search-all-books", "transaction");
                //Wrapper.Watch(() => { books = _bookService.SearchBook(book); }, _hub, "search-all-books", "transaction");
            }
            return View(books);
        }

        // GET: BookController/Details/5
        public ActionResult Details(Book book)
        {
            return View(book);
        }

        // GET: BookController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BookController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Book book)
        {

            Wrapper.Watch(() => { 
                book.CityID = CheckEntityInDatabase(book.CityName, _cityService);
                book.PublishingOfficeID = CheckEntityInDatabase(book.PublishingOfficeName, _publishingOfficeService);
                _bookService.InsertBook(book); 
            }, 
            _hub, "creating-a-book", "transaction");
            return RedirectToAction(nameof(Index));
            //var transaction = _hub.GetSpan();
            //var child1 = transaction.StartChild("creating-a-book");
            //try
            //{
            //    book.CityID = CheckEntityInDatabase(book.CityName, _cityService);
            //    book.PublishingOfficeID = CheckEntityInDatabase(book.PublishingOfficeName, _publishingOfficeService);
            //    _bookService.InsertBook(book);
            //    child1.Finish(SpanStatus.Ok);
            //    return RedirectToAction(nameof(Index));
            //}
            //catch
            //{
            //    child1.Finish(SpanStatus.Aborted);
            //    return View();
            //}
        }

        // GET: BookController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: BookController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: BookController/Delete/5
        public ActionResult Delete(Book book)
        {
            return View(book);
        }

        // POST: BookController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int bookId, IFormCollection collection)
        {
            try
            {
                _bookService.DeleteBook(bookId);
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                return View();
            }
        }

        public int CheckEntityInDatabase(string name, IDefiningEntity entityRepostitory)
        {
            int id = entityRepostitory.FindThisEntity(name);
            if (id == 0)
            {
                id = entityRepostitory.InsertThisEntity(name);
            }
            return id;
        }
    }
}
