﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Rokolabs.Library.Data
{
    public class Newspaper: IComparable
    {
        public string Title { get; set; }
        public int CityID { get; set; }

        [DisplayName("City of publishing")]
        public string CityName { get; set; }
        public int PublishingOfficeID { get; set; }

        [DisplayName("Publishing office")]
        public string PublishingOfficeName { get; set; }
        public string Description { get; set; }
        public string ISSN { get; set; }

        [DisplayName("Newspaper number")]
        public int PaperNumber { get; set; }

        [DisplayName("Title")]
        public string FullName
        {
            get
            {
                string name;
                if (YearOfPublishing == 0)
                    name = $"{Title} №{PaperNumber}";
                else
                    name = $"{Title} №{PaperNumber}/{YearOfPublishing}";
                return name;
            }
        }

        public int PaperID
        {
            get
            {
                return paperID;
            }
            set
            {
                if (value < 0)
                    paperID = 0;
                else
                    paperID = value;
            }
        }

        [DisplayName("Year")]
        [Range(1, 32767)]
        public short YearOfPublishing
        { 
            get
            {
                return yearOfPublishing;
            }
            set
            {
                if (value > (short)DateTime.Today.Year)
                    yearOfPublishing = (short)DateTime.Today.Year;
                else if(value != (short)Date.Year && Date != DateTime.MinValue)
                    yearOfPublishing = (short)Date.Year;
                else
                    yearOfPublishing = value;
            }
            
        }

        [DisplayName("Pages")]
        public int PagesCount { 
            get
            {
                return pagesCount;
            }
            set
            {
                if (value <= 0)
                    pagesCount = 1;
                else
                    pagesCount = value;
            }
        }

        [DisplayName("Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Date { 
            get
            {
                return date;
            }
            set
            {
                if (value > DateTime.Today)
                    date = DateTime.Today;
                else
                    date = value;
                if ((short)value.Year != yearOfPublishing)
                    yearOfPublishing = (short)value.Year;
            }
        }

        private DateTime date;
        private short yearOfPublishing;
        private int paperID;
        private int pagesCount;

        public int CompareTo(object obj)
        {
            Newspaper newspaper = obj as Newspaper;
            if (newspaper != null)
                return this.YearOfPublishing.CompareTo(newspaper.YearOfPublishing);
            else
                throw new Exception("Unable to compare two objects");
        }
    }
}
