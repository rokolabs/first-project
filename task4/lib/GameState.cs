﻿using System.Collections.Generic;

namespace Lib
{
    public class GameState
    {
        public Map MapExample { get; set; }
        public Player PlayerExample { get; set; }
        public List<BaseObject> DrawableObjects { get; }
        public List<IHitter> HittersList { get; }
        public List<IActor> ActorsList { get; }
        public List<Bonus> BonusesList { get; }
        public List<Monster> MonstersList { get; }
        public List<Obstacle> ObstaclesList { get; }

        public GameState()
        {
            DrawableObjects = new List<BaseObject>();
            HittersList = new List<IHitter>();
            ActorsList = new List<IActor>();
            BonusesList = new List<Bonus>();
            MonstersList = new List<Monster>();
            ObstaclesList = new List<Obstacle>();
        }
    }
}
