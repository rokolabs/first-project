﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using Rokolabs.Library.Bll.Interfaces;
using Rokolabs.Library.Dal.DTOs;
using Rokolabs.Library.Dal.Interfaces;
using Rokolabs.Library.Data;
using System.Collections.Generic;

namespace Rokolabs.Library.Bll
{
    public class CityService : ICityService
    {
        private readonly ILogger<CityService> _logger;
        private readonly ICityRepository _cityRepository;
        private readonly IMapper _mapper;

        public CityService(ILogger<CityService> logger, IMapper mapper, ICityRepository cityRepository)
        {
            _logger = logger;
            _cityRepository = cityRepository;
            _mapper = mapper;
        }
        public List<City> GetCitiesNames(string name)
        {
            // Get all cities that start with a given characters
            return new List<City>();
        }

        public int FindThisEntity(string name)
        {
            return _cityRepository.FindCity(name);
        }

        public int InsertThisEntity(string name)
        {
            return _cityRepository.InsertCity(name);
        }
    }
}
