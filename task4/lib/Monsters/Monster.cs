﻿using System;

namespace Lib
{
    public abstract class Monster : BaseObject, IActor, IHitter
    {
        public int HitRange { get; set; }
        public int Previous_X { get; set; }
        public int Previous_Y { get; set; }
        public int IgnoreObstaclesCounter { get; set; }
        public bool DoneStep { get; set; }
        public int PassNextSteps { get; set; }

        public delegate bool IsMovingAllowed(int x, int y, bool allObstaclesImpassable = true);

        private static readonly Random rnd = new Random();

        public Monster(int x, int y)
            : base(x, y)
        {
            IgnoreObstaclesCounter = 0;
        }

        public Monster()
        {
            IgnoreObstaclesCounter = 0;
        }

        public abstract void DoNextStep(Map map);

        public virtual void Hit(Player player)
        {
            player.Hurt(HitRange);
        }

        public void Clean()
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.SetCursorPosition(Previous_X, Previous_Y);
            Console.Write(" ");
            Console.SetCursorPosition(X, Y);
        }
        public virtual void GetBonus(Bonus bonus)
        {
            if (bonus is Blueberry)
            {
                IgnoreObstaclesCounter = 7;
                bonus.Availiable = false;
            }
        }

        public virtual void MoveRandom(IsMovingAllowed isMovingAllowed, Map map)
        {
            int dx = rnd.Next(3) - 1;
            int dy = rnd.Next(3) - 1;
            Previous_X = X;
            Previous_Y = Y;

            if (isMovingAllowed(X + dx, Y))
            {
                X += dx;
            }
            if (isMovingAllowed(X, Y + dy))
            {
                Y += dy;
            }
            DoneStep = true;
        }
    }
}
