﻿using System;
namespace Lib
{
    public class Bush : Obstacle, IHitter
    {
        public Bush(int x, int y)
            : base(x, y)
        {
            HitRange = 5;
            IsPassable = true;
        }

        public Bush()
        {
            HitRange = 5;
            IsPassable = true;
        }

        public override void Draw()
        {
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.SetCursorPosition(X, Y);
            Console.Write("#");
        }

        public void Hit(Player player)
        {
            player.Hurt(HitRange);
        }
    }
}
