﻿using System;
using System.Net;
using System.Runtime.Serialization;

namespace Rokolabs.Library.Data.Exceptions
{
    public class ExtraAppException : AppException
    {
		public ExtraAppException(string message)
			: base(message)
		{
			statusCode = HttpStatusCode.NotFound;
		}


		public ExtraAppException(string message, Exception innerException)
			: base(message, innerException)
		{
			statusCode = HttpStatusCode.NotFound;
		}


		protected ExtraAppException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
			statusCode = HttpStatusCode.NotFound;
		}
	}
}
