﻿namespace Lib
{
    public interface IActor
    {
        public abstract void DoNextStep(Map map);
        public abstract void Clean();
        public int Previous_X { get; set; }
        public int Previous_Y { get; set; }
        public bool DoneStep { get; set; }
        public int IgnoreObstaclesCounter { get; set; }
        public abstract void GetBonus(Bonus bonus);
    }
}
