﻿using Rokolabs.Library.Dal.DTOs;
using System.Collections.Generic;

namespace Rokolabs.Library.Dal.Interfaces
{
    public interface IBookRepository
    {
        List<BookDto> GetAllBooks();
        List<BookDto> GetAllBooksDateAsc();
        List<BookDto> GetAllBooksDateDesc();
        int InsertBook(BookDto book);
        int GetBook(BookDto book);
        int GetBookByISBN(BookDto book);
        List<BookDto> SearchBooks(BookDto book);
        List<BookDto> SearchBooksDateAsc(BookDto book);
        List<BookDto> SearchBooksDateDesc(BookDto book);
        void DeleteBook(int bookId);
        List<BookDto> SearchBooksByAuthor(AuthorDto author);
        List<BookDto> SearchBooksByAuthorDateAsc(AuthorDto author);
        List<BookDto> SearchBooksByAuthorDateDesc(AuthorDto author);
        List<BookDto> SearchBooksByPublishingOffice(string publishingOfficeName);
        List<BookDto> SearchBooksByPublishingOfficeDateAsc(string publishingOfficeName);
        List<BookDto> SearchBooksByPublishingOfficeDateDesc(string publishingOfficeName);


    }
}
