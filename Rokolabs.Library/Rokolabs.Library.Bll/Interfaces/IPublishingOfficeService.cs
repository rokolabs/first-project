﻿using Rokolabs.Library.Data;
using System.Collections.Generic;

namespace Rokolabs.Library.Bll.Interfaces
{
    public interface IPublishingOfficeService: IDefiningEntity
    {
        List<PublishingOffice> GetSuitablePublishingOffices(string name);

    }
}
