﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using Rokolabs.Library.Bll.Interfaces;
using Rokolabs.Library.Dal.DTOs;
using Rokolabs.Library.Dal.Interfaces;
using Rokolabs.Library.Data;
using Sentry;
using System.Collections.Generic;
using System.Linq;

namespace Rokolabs.Library.Bll
{
    public class BookService : IBookService
    {
		private readonly ILogger<BookService> _logger;
		private readonly IBookRepository _bookRepository;
		private readonly IAuthorRepository _authorRepository;
		private readonly IPublishngOfficeRepository _publishngOfficeRepository;
		private readonly IMapper _mapper;
        private readonly IHub _hub;
        public BookService(ILogger<BookService> logger, IMapper mapper, IBookRepository bookRepository, IAuthorRepository authorRepository, IPublishngOfficeRepository publishngOfficeRepository, IHub hub)
		{
			_logger = logger;
			_bookRepository = bookRepository;
			_authorRepository = authorRepository;
			_publishngOfficeRepository = publishngOfficeRepository;
			_mapper = mapper;
            _hub = hub;
		}

        public void DeleteBook(int bookId)
        {
            _logger.LogInformation($"Book with id [{bookId}] was deleted");
            _bookRepository.DeleteBook(bookId);
		}

        public List<Book> GetAllBooks()
        {
            var books = _mapper.Map<List<Book>>(_bookRepository.GetAllBooks());
            AddAuthors(books);
            return books;
        }

        private void AddAuthors(List<Book> books)
        {
            var authors = _authorRepository.GetAuthorsByBooksIds(books.Select(p => p.BookId));
            foreach (var book in books)
            {
                book.Authors = _mapper.Map<List<Author>>(authors.Where(p => book.BookId == p.BookId));
            }
        }

        public int InsertBook(Book book)
        {
            _logger.LogInformation($"Book {book.Title} ({book.YearOfPublishing}) ISBN: {book.ISBN} added");
            var child = _hub.GetSpan();
            var childInserting = child.StartChild("Inserting-in-database");
            var bookID = _bookRepository.InsertBook(_mapper.Map<BookDto>(book));
            childInserting.Finish();
            return bookID;
		}

        public List<Book> SearchBook(Book book)
        {
            var transaction = _hub.GetSpan();
            //var transaction = _hub.StartTransaction(
            //"searching-transaction",
            //"searching-book-operation"
            //);
            var span = transaction.StartChild("searching-book-in-library-operation");
            var books = _mapper.Map<List<Book>>(_bookRepository.SearchBooks(_mapper.Map<BookDto>(book)));
            span.Finish();
            //throw new System.Exception("smth went wrong");
            var span2 = transaction.StartChild("adding-authors-operation");
            AddAuthors(books);
            span2.Finish(SpanStatus.NotFound);
            //transaction.Finish();
            return books;
		}

        public List<Book> SearchBookByAuthor(Author author)
        {
			var fullAuthor = _mapper.Map<Author>(_authorRepository.GetAuthor(_mapper.Map<AuthorDto>(author)));
			List<Book> books = new List<Book>();
			if (fullAuthor != null)
				books = _mapper.Map<List<Book>>(_bookRepository.SearchBooksByAuthor(_mapper.Map<AuthorDto>(fullAuthor)));
			AddAuthors(books);
			return books;
        }

        public List<Book> SearchBookByAuthorsID(int[] authors)
        {
            // get authors' data by ids -> list of authors
            // get books by authors' ids -> list of books
            // add authors to books

            //var fullAuthor = _mapper.Map<Author>(_authorRepository.GetAuthor(_mapper.Map<AuthorDto>(author)));
            List<Book> books = new List<Book>();
            //if (fullAuthor != null)
            //    books = _mapper.Map<List<Book>>(_bookRepository.SearchBooksByAuthor(_mapper.Map<AuthorDto>(fullAuthor)));
            //AddAuthors(books);
            return books;
        }

        public List<Book> SearchBooksByPublishingOffice(string publishingOfficeName)
        {
			var books = _mapper.Map<List<Book>>(_bookRepository.SearchBooksByPublishingOffice(publishingOfficeName));
			AddAuthors(books);
			return books;
		}

        public List<Book> GetAllBooksDateAsc()
        {
            var books = _mapper.Map<List<Book>>(_bookRepository.GetAllBooksDateAsc());
            AddAuthors(books);
            return books;
        }

        public List<Book> GetAllBooksDateDesc()
        {
            var books = _mapper.Map<List<Book>>(_bookRepository.GetAllBooksDateDesc());
            AddAuthors(books);
            return books;
        }

        public List<Book> SearchBookDateAsc(Book book)
        {
            var books = _mapper.Map<List<Book>>(_bookRepository.SearchBooksDateAsc(_mapper.Map<BookDto>(book)));
            AddAuthors(books);
            return books;
        }

        public List<Book> SearchBookDateDesc(Book book)
        {
            var books = _mapper.Map<List<Book>>(_bookRepository.SearchBooksDateDesc(_mapper.Map<BookDto>(book)));
            AddAuthors(books);
            return books;
        }

        public List<Book> SearchBookByAuthorDateAsc(Author author)
        {
            var fullAuthor = _mapper.Map<Author>(_authorRepository.GetAuthor(_mapper.Map<AuthorDto>(author)));
            List<Book> books = new List<Book>();
            if (fullAuthor != null)
                books = _mapper.Map<List<Book>>(_bookRepository.SearchBooksByAuthorDateAsc(_mapper.Map<AuthorDto>(fullAuthor)));
            AddAuthors(books);
            return books;
        }

        public List<Book> SearchBookByAuthorDateDesc(Author author)
        {
            var fullAuthor = _mapper.Map<Author>(_authorRepository.GetAuthor(_mapper.Map<AuthorDto>(author)));
            List<Book> books = new List<Book>();
            if (fullAuthor != null)
                books = _mapper.Map<List<Book>>(_bookRepository.SearchBooksByAuthorDateDesc(_mapper.Map<AuthorDto>(fullAuthor)));
            AddAuthors(books);
            return books;
        }

        public List<Book> SearchBooksByPublishingOfficeDateAsc(string publishingOfficeName)
        {
            var books = _mapper.Map<List<Book>>(_bookRepository.SearchBooksByPublishingOfficeDateAsc(publishingOfficeName));
            AddAuthors(books);
            return books;
        }

        public List<Book> SearchBooksByPublishingOfficeDateDesc(string publishingOfficeName)
        {
            var books = _mapper.Map<List<Book>>(_bookRepository.SearchBooksByPublishingOfficeDateDesc(publishingOfficeName));
            AddAuthors(books);
            return books;
        }

        public Book GetBook(Book book)
        {
            Book dbBook = new Book();
            dbBook.BookId = _bookRepository.GetBook(_mapper.Map<BookDto>(book));
            AddAuthors(new List<Book>() { dbBook });
            return dbBook;
        }

        public Book GetBookByISBN(Book book)
        {
            Book dbBook = new Book();
            dbBook.BookId = _bookRepository.GetBookByISBN(_mapper.Map<BookDto>(book));
            AddAuthors(new List<Book>() { dbBook });
            return dbBook;
        }
    }
}
