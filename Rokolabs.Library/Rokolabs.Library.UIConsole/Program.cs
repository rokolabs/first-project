﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Rokolabs.Library.Bll;
using Rokolabs.Library.Bll.Interfaces;
using Rokolabs.Library.Dal;
using Rokolabs.Library.Dal.Interfaces;
using System;
using System.IO;

namespace Rokolabs.Library.UIConsole
{
    class Program
    {
		static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

		private static IHostBuilder CreateHostBuilder(string[] args)
		{
			return Host.CreateDefaultBuilder(args)
				.ConfigureServices((hostingContext, services) =>
				{
					IConfiguration configuration = hostingContext.Configuration;
					var connectionString = configuration.GetConnectionString("DefaultConnection");

					var mappingConfig = new MapperConfiguration(mc =>
					{
						mc.AddProfile(new MappingProfile());
					});

					IMapper mapper = mappingConfig.CreateMapper();
					services.AddSingleton(mapper);

					services.AddSingleton(new DBOptions() { ConnectionString = connectionString });
					services.AddTransient<IAuthorService, AuthorService>();
					services.AddTransient<IBookService, BookService>();
					services.AddTransient<ICityService, CityService>();
					services.AddTransient<ICountryService, CountryService>();
					services.AddTransient<IPatentService, PatentService>();
					services.AddTransient<IPublishingOfficeService, PublishingOfficeService>();
					services.AddTransient<INewspaperService, NewspaperService>();
					services.AddTransient<IAuthorRepository, AuthorRepository>();
					services.AddTransient<IBookRepository, BookRepository>();
					services.AddTransient<ICityRepository, CityRepository>();
					services.AddTransient<ICountryRepository, CountryRepository>();
					services.AddTransient<INewspaperRepository, NewspaperRepository>();
					services.AddTransient<IPatentRepository, PatentRepository>();
					services.AddTransient<IPublishngOfficeRepository, PublishingOfficeRepository>();

					services.AddHostedService<Worker>();
				})
				.ConfigureLogging(logging =>
				{
					logging.ClearProviders();
					logging.AddConsole();
					//logging.AddFile(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "log.txt"));
				});
		}
	}
}
