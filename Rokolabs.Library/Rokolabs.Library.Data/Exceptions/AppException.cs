﻿using System;
using System.Net;
using System.Runtime.Serialization;
namespace Rokolabs.Library.Data.Exceptions
{
    [Serializable]
    public class AppException : ApplicationException
    {
        public HttpStatusCode statusCode { get; set; }

		public AppException(string message)
			: base(message)
		{
			statusCode = HttpStatusCode.InternalServerError;
		}

		public AppException(string message, Exception innerException)
			: base(message, innerException)
		{
			statusCode = HttpStatusCode.InternalServerError;
		}


		protected AppException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
			statusCode = HttpStatusCode.InternalServerError;
		}
	}
}
