﻿using System;

namespace Lib
{
    public class Pineapple: Bonus
    {
        public override void Draw()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.SetCursorPosition(X, Y);
            Console.Write("O");
        }

        public Pineapple(int x, int y)
            : base(x, y, 45)
        {
        }

        public Pineapple()
        {
            Energy = 45;
        }
    }
}
