﻿using System;
using System.Collections.Generic;
using Sentry;


namespace Rokolabs.Library.UIWeb
{
    public class Wrapper
    {

        public static void Watch(Action action, IHub hub, string operationName, string transactionName)
        {
            var transaction = hub.GetSpan();
            bool isNewTransaction = false;
            if (transaction == null)
            {
                transaction = hub.StartTransaction(transactionName, operationName);
                isNewTransaction = true;
            }
            var child = transaction.StartChild(operationName);
            action();
            child.Finish();
            if (isNewTransaction)
            { 
                transaction.Finish(); 
            }
        }

        public T Watch<T>(Func<T> func, IHub hub, string operationName, string transactionName)
        {
            var transaction = hub.GetSpan();
            bool isNewTransaction = false;
            if (transaction == null)
            {
                transaction = hub.StartTransaction(transactionName, operationName);
                isNewTransaction = true;
            }
            var child = transaction.StartChild(operationName);
            try
            {
                var result = func();
                child.Finish(SpanStatus.Ok);
                return result;
            }
            catch (Exception e)
            {
                child.Finish(e);
                throw;
            }
            finally
            {
                if (isNewTransaction)
                {
                    transaction.Finish();
                }
            }
        }
    }
}
