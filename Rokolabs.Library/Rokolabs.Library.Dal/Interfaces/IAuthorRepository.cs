﻿using Rokolabs.Library.Dal.DTOs;
using Rokolabs.Library.Data;
using System.Collections.Generic;

namespace Rokolabs.Library.Dal.Interfaces
{
    public interface IAuthorRepository
    {
        List<AuthorDto> GetAllAuthors();
        int InsertAuthor(AuthorDto author);
        List<AuthorDto> GetAuthorsByBooksIds(IEnumerable<int> ids);
        List<AuthorDto> GetAuthorsByPatentsIds(IEnumerable<int> ids);
        void UpdateAuthor(int id, AuthorDto authorDto);
        void DeleteAuthor(int id);
        List<AuthorDto> SearchAuthors(AuthorDto author);
        List<AuthorDto> SearchAuthors(string search);
        AuthorDto GetAuthor(AuthorDto author);
        void AddAuthorToBook(BookDto book, AuthorDto author);
        void AddAuthorToPatent(PatentDto patent, AuthorDto author);
    }
}
