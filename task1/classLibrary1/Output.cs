﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace lib
{
    public static class Output
    {
        public static void PrintArray<T>(T[] arr)
        {
            foreach (var item in arr)
            {
                Console.Write($"{item}, ");
            }
            Console.WriteLine();
        }
    }
}
