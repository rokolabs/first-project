﻿using System;

namespace Lib
{
    public class Apple : Bonus
    {
        public override void Draw()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.SetCursorPosition(X, Y);
            Console.Write("o");
        }

        public Apple(int x, int y)
            : base(x, y, 30)
        {
        }

        public Apple()
        {
            Energy = 30;
        }
    }
}
