﻿using System;

namespace Rokolabs.Library.Dal.DTOs
{
    public class NewspaperDto
    {
        public int PaperID { get; set; }
        public string Title { get; set; }
        public int CityID { get; set; }
        public int PublishingOfficeID { get; set; }
        public string CityName { get; set; }
        public string PublishingOfficeName { get; set; }
        public short YearOfPublishing { get; set; }
        public int PagesCount { get; set; }
        public string Description { get; set; }
        public int PaperNumber { get; set; }
        public DateTime Date { get; set; }
        public string ISSN { get; set; }
    }
}
