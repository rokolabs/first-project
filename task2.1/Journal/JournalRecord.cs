﻿using System;
using System.Collections.Generic;

namespace JournalMaker
{
    public class JournalRecord
    {
        public DateTime DateTimeProperty { get; set; }
        public List<FileInRecord> Files { get; set; }

        public JournalRecord(DateTime DateTimeProperty, string[] files, string separator)
        {
            Files = new List<FileInRecord>();
            this.DateTimeProperty = DateTimeProperty;
            for (int i = 0; i < files.Length; i++)
            {
                string[] fileInfo = files[i].Split(separator);
                FileInRecord file = new FileInRecord(fileInfo);
                Files.Add(file);
            }
        }

        public JournalRecord()
        {
            Files = new List<FileInRecord>();
        }

        public void PrintJournalRecord()
        {
            Console.WriteLine(DateTimeProperty.ToString());
            for (int i = 0; i < Files.Count; i++)
            {
                Files[i].PrintFileInRecord();
            }
        }
     }
}

