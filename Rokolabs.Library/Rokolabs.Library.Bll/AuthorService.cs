﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using Rokolabs.Library.Bll.Interfaces;
using Rokolabs.Library.Dal.DTOs;
using Rokolabs.Library.Dal.Interfaces;
using Rokolabs.Library.Data;
using System.Collections.Generic;

namespace Rokolabs.Library.Bll
{
    public class AuthorService : IAuthorService
    {
        private readonly ILogger<AuthorService> _logger;
        private readonly IAuthorRepository _authorRepository;
        private readonly IMapper _mapper;


        public AuthorService(ILogger<AuthorService> logger, IMapper mapper, IAuthorRepository authorRepository)
        {
            _logger = logger;
            _authorRepository = authorRepository;
            _mapper = mapper;
        }

        public void DeleteAuthor(int id)
        {
            _authorRepository.DeleteAuthor(id);
            _logger.LogInformation($"Deleted author with id = {id}");
        }

        public int InsertAuthor(Author author)
        {
            if (author.FirstName != string.Empty && author.LastName != string.Empty)
            {
                _logger.LogInformation($"Insert author {author.FirstName} {author.LastName}");
                return _authorRepository.InsertAuthor(_mapper.Map<AuthorDto>(author));
            }
            else
            {
                _logger.LogError($"Try to insert author with unfilled data: FirstName = [{author.FirstName}], LastName = [{author.LastName}]");
                return 0;
            }
        }

        public void UpdateAuthor(int id, Author author)
        {
            _authorRepository.UpdateAuthor(id, _mapper.Map<AuthorDto>(author));
        }

        public List<Author> GetAllAuthors()
        {
            return _mapper.Map<List<Author>>(_authorRepository.GetAllAuthors());
        }

        public List<Author> SearchAuthor(Author author)
        {
           return _mapper.Map<List<Author>>(_authorRepository.SearchAuthors(_mapper.Map<AuthorDto>(author)));
        }

        public void AddAuthorsToBook(Book book)
        {
            _logger.LogInformation($"Add authors to book {book.Title}");
            for (int i = 0; i < book.Authors.Count; i++)
            {
                _authorRepository.AddAuthorToBook(_mapper.Map<BookDto>(book), _mapper.Map<AuthorDto>(book.Authors[i]));
                _logger.LogInformation($"{i} - {book.Authors[i].FirstName} {book.Authors[i].LastName}");
            }
           
        }
        public void AddAuthorsToPatent(Patent patent)
        {
            _logger.LogInformation($"Add authors to pantent {patent.Title}");
            for (int i = 0; i < patent.Authors.Count; i++)
            {
                _authorRepository.AddAuthorToPatent(_mapper.Map<PatentDto>(patent), _mapper.Map<AuthorDto>(patent.Authors[i]));
                _logger.LogInformation($"{i} - {patent.Authors[i].FirstName} {patent.Authors[i].LastName}");
            }
        }

        public Author GetAuthor(Author author)
        {
            return _mapper.Map<Author>(_authorRepository.GetAuthor(_mapper.Map<AuthorDto>(author)));
        }

        public List<Author> SearchAuthors(string search)
        {
            return _mapper.Map<List<Author>>(_authorRepository.SearchAuthors(search));
        }
    }
}
