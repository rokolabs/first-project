﻿using System;

namespace JournalMaker
{
    public class FileInRecord
    {
        public int Id { get; set; }

        public string LocalFilename { get; set; }

        public string Fullname { get; set; }

        public string Change { get; set; }

        public FileInRecord(string[] fileInfo)
        {
            Id = int.Parse(fileInfo[0]);
            LocalFilename = fileInfo[1];
            Fullname = fileInfo[2];
            Change = fileInfo[3];
        }

        public FileInRecord(int newId, string newLocalFilename, string newFullname, string newChange)
        {
            Id = newId;
            LocalFilename = newLocalFilename;
            Fullname = newFullname;
            Change = newChange;
        }
        public FileInRecord()
        {}

        public FileInRecord(string newLocalFilename, string newFullname)
        {
            Id = 1;
            LocalFilename = newLocalFilename;
            Fullname = newFullname;
            Change = "created";
        }

        public void PrintFileInRecord()
        {
            System.Console.WriteLine($"ID: {Id} | LocalName: {LocalFilename} | Fullname: {Fullname} | Change: {Change}");
        }

        public static FileInRecord CreateChangedFile(FileInRecord fileInRecord)
        {
            FileInRecord newFileInRecord = new FileInRecord
            {
                Id = fileInRecord.Id + 1,
                LocalFilename = CreateNewLocalname(fileInRecord.LocalFilename),
                Fullname = fileInRecord.Fullname,
                Change = "changed"
            };
            return newFileInRecord;
        }

        public static FileInRecord CreateNotChangedFile(FileInRecord fileInRecord)
        {
            FileInRecord newFileInRecord = new FileInRecord
            {
                Id = fileInRecord.Id,
                LocalFilename = fileInRecord.LocalFilename,
                Fullname = fileInRecord.Fullname,
                Change = "not changed"
            };
            return newFileInRecord;
        }

        public static FileInRecord CreateDeletedFile(FileInRecord fileInRecord)
        {
            FileInRecord newFileInRecord = new FileInRecord
            {
                Id = -1,
                LocalFilename = fileInRecord.LocalFilename,
                Fullname = fileInRecord.Fullname,
                Change = "deleted"
            };
            return newFileInRecord;
        }

        public static string CreateNewLocalname(string previousLocalname)
        {
            string[] partsOfFilename = previousLocalname.Split("_");
            int newIndex = int.Parse(partsOfFilename[0]) + 1;
            partsOfFilename[0] = newIndex.ToString();
            return String.Join("_", partsOfFilename);
        }
    }
}
