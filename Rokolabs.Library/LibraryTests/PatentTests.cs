using NUnit.Framework;
using Rokolabs.Library.Data;
using System;

namespace LibraryTests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestPatentApplicationDate()
        {
            Patent patent = new Patent();
            patent.ApplicationDate = new DateTime(2051, 2, 2);
            Assert.AreEqual(patent.ApplicationDate, DateTime.Today);
        }

        [Test]
        public void TestPatentPublicationDate()
        {
            Patent patent = new Patent();
            patent.PublicationDate = new DateTime(2051, 2, 2);
            Assert.AreEqual(patent.PublicationDate, DateTime.Today);
        }

        [Test]
        public void TestPatentDateDiifs()
        {
            Patent patent = new Patent();
            patent.ApplicationDate = new DateTime(2021, 4, 12);
            patent.PublicationDate = new DateTime(2021, 2, 2);
            Assert.AreEqual(patent.ApplicationDate, patent.PublicationDate);
        }

        [Test]
        public void TestPatentPages()
        {
            Patent patent = new Patent();
            patent.PagesCount = -23;
            Assert.True(patent.PagesCount >= 0);
        }

        [Test]
        public void TestPatentAuthors()
        {
            Patent patent = new Patent();
            Assert.IsNotNull(patent.Authors);
        }
    }
}