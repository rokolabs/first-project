﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Rokolabs.Library.Bll.Interfaces;
using Rokolabs.Library.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rokolabs.Library.UIWeb.Controllers
{
    public class NewspaperController : Controller
    {
        private readonly ILogger<BookController> _logger;
        private readonly INewspaperService _newspaperService;
        private readonly ICityService _cityService;
        private readonly IPublishingOfficeService _publishingOfficeService;

        public NewspaperController(ILogger<BookController> logger, INewspaperService newspaperService, ICityService cityService, IPublishingOfficeService publishingOfficeService)
        {
            _logger = logger;
            _newspaperService = newspaperService;
            _cityService = cityService;
            _publishingOfficeService = publishingOfficeService;
        }


        // GET: NewspaperController
        public ActionResult Index()
        {
            var newspapers = _newspaperService.GetAllNewspapers();
            return View(newspapers);
        }

        [HttpPost]
        public ActionResult Index(Newspaper newspaper)
        {
            List<Newspaper> newspapers;
            if (newspaper.Title == null)
                newspapers = _newspaperService.GetAllNewspapers();
            else
                newspapers = _newspaperService.SearchNewspaper(newspaper);
            return View(newspapers);
        }

        // GET: NewspaperController/Details/5
        public ActionResult Details(Newspaper newspaper)
        {
            return View(newspaper);
        }

        // GET: NewspaperController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: NewspaperController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Newspaper newspaper)
        {
            try
            {
                newspaper.CityID = CheckEntityInDatabase(newspaper.CityName, _cityService);
                newspaper.PublishingOfficeID = CheckEntityInDatabase(newspaper.PublishingOfficeName, _publishingOfficeService);
                _newspaperService.InsertNewspaper(newspaper);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: NewspaperController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: NewspaperController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: NewspaperController/Delete/5
        public ActionResult Delete(Newspaper newspaper)
        {
            return View(newspaper);
        }

        // POST: NewspaperController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int paperID, IFormCollection collection)
        {
            try
            {
                _newspaperService.DeleteNewspaper(paperID);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        public int CheckEntityInDatabase(string name, IDefiningEntity entityRepostitory)
        {
            int id = entityRepostitory.FindThisEntity(name);
            if (id == 0)
            {
                id = entityRepostitory.InsertThisEntity(name);
            }
            return id;
        }
    }
}
