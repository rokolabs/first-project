﻿using System.Collections.Generic;

namespace Rokolabs.Library.Data
{
    public class Author
    {
        
        public int AuthorID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Description { get; set; }
    }
}
