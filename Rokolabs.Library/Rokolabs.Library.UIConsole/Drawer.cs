﻿using Microsoft.Extensions.Logging;
using Rokolabs.Library.Data;
using System;
using System.Collections.Generic;

namespace Rokolabs.Library.UIConsole
{
    public class Drawer
    {
        public int MainMenuStartPosition { get; set; } = 2;
        public int MainMenuEndPosition { get; set; } = 6;
        public int ExtraMenuStartPosition { get; set; } = 8;

        private readonly ILogger<Worker> _logger;

        public Drawer(ILogger<Worker> logger)
        {
            _logger = logger;
        }
        public Drawer()
        {}

        public void DrawMainMenu()
        {
            Console.SetCursorPosition(0, 0);
            Console.WriteLine("Welcome to Library catalog");
            Console.WriteLine("Please select:");
            Console.WriteLine("   - Add record to catalog");
            Console.WriteLine("   - Delete record from catalog");
            Console.WriteLine("   - View catalog");
            Console.WriteLine("   - Search publication by title");
            Console.WriteLine("   - Search publication by author");
            Console.WriteLine("   - Search publication by publishng office");
            Console.SetCursorPosition(0, ExtraMenuStartPosition);
        }
        
        public int CheckSelectedItem(int startPositon, int endPosition, string arrow = "->")
        {
            string empty = new(' ', arrow.Length);
            int i = startPositon;
            Console.SetCursorPosition(0, startPositon);
            Console.Write(arrow);
            ConsoleKeyInfo key;
            for (; ; )
            {
                key = Console.ReadKey(true);
                switch (key.Key)
                {
                    case ConsoleKey.DownArrow:
                        if (i == endPosition + startPositon - 1)
                            continue;
                        Console.SetCursorPosition(0, i);
                        Console.Write(empty);
                        Console.SetCursorPosition(0, ++i);
                        Console.Write(arrow);
                        break;
                    case ConsoleKey.UpArrow:
                        if (i == startPositon)
                            continue;
                        Console.SetCursorPosition(0, i);
                        Console.Write(empty);
                        Console.SetCursorPosition(0, --i);
                        Console.Write(arrow);
                        break;
                    case ConsoleKey.Enter:
                        return i + 1 - startPositon;
                    case ConsoleKey.Escape:
                        return 0;
                }
            }
        }

        public void DrawSearhByPublishingOffice()
        {
            GoToExtraMenu();
            Console.WriteLine("You selected search by publishing office");
        }

        public void DrawSearchByAuthor()
        {
            GoToExtraMenu();
            Console.WriteLine("You selected search by author");
        }

        public void DrawSearchByTitle()
        {
            GoToExtraMenu();
            Console.WriteLine("You selected search by title");
        }

        public void DrawViewCatalog()
        {
            GoToExtraMenu();
            Console.WriteLine("You selected view catalog");
            Console.WriteLine("Please select:");
            Console.WriteLine("   - View patents");
            Console.WriteLine("   - View books");
            Console.WriteLine("   - View newspapers");
        }

        public bool DrawResolveAuthorProblem(Author dbAuthor, Author author)
        {
            GoToExtraMenu();
            Console.WriteLine("There is author with same first and last name in database");
            Console.WriteLine("Full information for the author");
            Console.WriteLine($"{dbAuthor.FirstName} {dbAuthor.LastName} - {dbAuthor.Description}");
            if (dbAuthor.Description == author.Description)
            {
                Console.WriteLine("The description you entered matches the description of the author in the database");
                Console.WriteLine("Do you want to add another author with new description?");
                Console.WriteLine("  - Yes");
                Console.WriteLine("  - No");
                return false;
            }
            else
            {
                Console.WriteLine("The description you entered differs from the description of the author in the database");
                Console.WriteLine("Are you sure you want to add this author?");
                Console.WriteLine("  - Yes");
                Console.WriteLine("  - No");
                return true;
            }
        }

        public void DrawViewMode()
        {
            GoToExtraMenu();
            Console.WriteLine("Please choose sorting mode:");
            Console.WriteLine("   - Asceding sort by year of publication");
            Console.WriteLine("   - Desceding sort by year of publication");
            Console.WriteLine("   - No sort");
        }

        public void DrawGroupMode()
        {
            Console.WriteLine("Do you want group publications by year?");
            Console.WriteLine("   - Yes");
            Console.WriteLine("   - No");
        }

        public void DrawAuthorsPublicationsVariants()
        {
            Console.WriteLine("Please choose publications type:");
            Console.WriteLine("   - Books");
            Console.WriteLine("   - Patents");
            Console.WriteLine("   - Both");
        }

        public void DrawDeleteRecord()
        {
            GoToExtraMenu();
            Console.WriteLine("You selected delete record");
            Console.WriteLine("Please select:");
            Console.WriteLine("   - Delete author");
            Console.WriteLine("   - Delete book");
            Console.WriteLine("   - Delete patent");
            Console.WriteLine("   - Delete newspaper");
        }

        private static void GoToExtraMenu()
        {
            Console.Clear();
            Console.SetCursorPosition(0, 0);
            Console.WriteLine("                                                              ");
            Console.SetCursorPosition(0, 0);
        }

        public void DrawAddRecord()
        {
            GoToExtraMenu();
            Console.WriteLine("You selected add record");
            Console.WriteLine("Please select:");
            Console.WriteLine("   - Add author");
            Console.WriteLine("   - Add book");
            Console.WriteLine("   - Add patent");
            Console.WriteLine("   - Add newspaper");
        }

        public Author GetInfoForDeletingAuthor()
        {
            var author = new Author();
            Console.WriteLine("You selected delete author");
            Console.WriteLine("Fill in the following fields");
            Console.Write("Author's first name: ");
            author.FirstName = Console.ReadLine();
            Console.Write("Author's last name: ");
            author.LastName = Console.ReadLine();
            return author;
        }

        public Author GetInfoAboutAuthor()
        {
            var author = new Author();
            Console.Write("Author's first name: ");
            author.FirstName = Console.ReadLine();
            Console.Write("Author's last name: ");
            author.LastName = Console.ReadLine();
            return author;
        }


        public void DrawGoBackFooter()
        {
            Console.WriteLine("  Go back...");
            Console.WriteLine();
        }

        public string DrawDeletePublication(string type)
        {
            Console.WriteLine($"You selected delete {type}");
            Console.WriteLine("Fill in the following fields");
            Console.Write("Title: ");
            string title = Console.ReadLine();
            return title;
        }

        public void DrawDeleteMessage(int indent, string title)
        {
            Console.SetCursorPosition(0, indent);
            Console.WriteLine($"{title} was deleted");
            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }

        public void DrawPressAnyKey()
        {
            Console.WriteLine();
            Console.WriteLine("Press any key...");
            Console.ReadKey();
            Console.Clear();
        }

        public Author GetInfoForAddingAuthor()
        {
            var author = new Author();
            Console.WriteLine("You selected add author");
            Console.WriteLine("Fill in the following fields");
            Console.Write("Author's first name: ");
            author.FirstName = Console.ReadLine();
            Console.Write("Author's last name: ");
            author.LastName = Console.ReadLine();
            Console.Write("Some extra description: ");
            author.Description = Console.ReadLine();
            return author;
        }

        public Book GetInfoForAddingBook()
        {
            var book = new Book();
            short year;
            short pagesCount;
            Console.WriteLine("You selected add book");
            Console.WriteLine("Fill in the following fields");
            Console.Write("Title: ");
            book.Title = Console.ReadLine();
            Console.Write("Pages count: ");
            short.TryParse(Console.ReadLine(), out pagesCount);
            book.PagesCount = pagesCount;
            Console.Write("Year of publishing: ");
            short.TryParse(Console.ReadLine(), out year);
            book.YearOfPublishing = year;
            Console.Write("Some description: ");
            book.Description = Console.ReadLine();
            Console.Write("ISBN: ");
            book.ISBN = Console.ReadLine();
            Console.Write("City of publishing: ");
            book.CityName = Console.ReadLine();
            Console.Write("Publishing Office name: ");
            book.PublishingOfficeName = Console.ReadLine();
            return book;
        }

        public Patent GetInfoForAddingPatent()
        {
            Patent patent = new Patent();
            Console.WriteLine("You selected add patent");
            Console.WriteLine("Fill in the following fields");
            int registrationNumber;
            short pagesCount;
            DateTime applicationDate;
            DateTime publicationDate;
            Console.Write("Title: ");
            patent.Title = Console.ReadLine();
            Console.Write("Patent registration number: ");
            int.TryParse(Console.ReadLine(), out registrationNumber);
            patent.PatentRegistrationNumber = registrationNumber;
            Console.Write("Country: ");
            patent.CountryName = Console.ReadLine();
            Console.Write("Applicaion date: ");
            DateTime.TryParse(Console.ReadLine(), out applicationDate);
            Console.Write("Publication date: ");
            DateTime.TryParse(Console.ReadLine(), out publicationDate);
            patent.ApplicationDate = applicationDate;
            patent.PublicationDate = publicationDate;
            Console.Write("Pages count: ");
            short.TryParse(Console.ReadLine(), out pagesCount);
            patent.PagesCount = pagesCount;
            Console.Write("Some description: ");
            patent.Description = Console.ReadLine();
            return patent;
        }

        public Newspaper AddNewspaper()
        {
            var newspaper = new Newspaper();
            int pagesCount;
            int paperNumber;
            DateTime date;
            Console.WriteLine("You selected add newspaper");
            Console.WriteLine("Fill in the following fields");
            Console.Write("Title: ");
            newspaper.Title = Console.ReadLine();
            Console.Write("City of publishing: ");
            newspaper.CityName = Console.ReadLine();
            Console.Write("Publishing office name: ");
            newspaper.PublishingOfficeName = Console.ReadLine();
            Console.Write("Pages count: ");
            int.TryParse(Console.ReadLine(), out pagesCount);
            newspaper.PagesCount = pagesCount;
            Console.Write("Description: ");
            newspaper.Description = Console.ReadLine();
            Console.Write("Paper number: ");
            int.TryParse(Console.ReadLine(), out paperNumber);
            newspaper.PaperNumber = paperNumber;
            Console.Write("Date of publication: ");
            DateTime.TryParse(Console.ReadLine(), out date);
            newspaper.YearOfPublishing = (short)date.Year;
            newspaper.Date = date;
            Console.Write("ISSN: ");
            newspaper.ISSN = Console.ReadLine();
            return newspaper;
        }
    }
}
