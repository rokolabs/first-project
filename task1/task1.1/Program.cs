﻿using System;
using lib;

namespace task1._1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Input array lenght: ");
            int length = int.Parse(Console.ReadLine());
            if (length <= 0)
                Console.WriteLine("Incorrect value");
            else
            {
                int[] arr = ArrayFuncs.CreateArray(length);
                Console.WriteLine("Array:");
                Output.PrintArray(arr);
                int minItem = ArrayFuncs.FindMin(arr);
                Console.WriteLine($"Min value in array: {minItem}");
                int maxItem = ArrayFuncs.FindMax(arr);
                Console.WriteLine($"Max value in array: {maxItem}");
                double avg = ArrayFuncs.FindAverage(arr);
                Console.WriteLine($"Average value in array: {avg}");
                ArrayFuncs.SortArray(arr);
                Console.WriteLine("Sorted array: ");
                Output.PrintArray(arr);
                double median = ArrayFuncs.FindMedian(arr);
                Console.WriteLine($"Median value in array: {median}");
            }
        }
    }
}
