﻿using System;

namespace Lib
{
    public class Player : BaseObject, IActor
    {
        private int _health;
        public ConsoleKey Key { get; set; }
        public delegate bool IsMovingAllowed(int x, int y, bool allObstaclesImpassable = true);

        public int Health 
        { 
            get
            {
                return _health;
            }
            set
            {
                if (value < 0)
                    _health = 0;
                else
                if (value > 100)
                    _health = 100;
                else
                    _health = value;  
            }
        }

        public int Previous_X { get; set; }
        public int Previous_Y { get; set; }
        public int IgnoreObstaclesCounter { get; set; }
        public bool DoneStep { get; set; }

        public string Nickname { get; set; }

        public Player(int x, int y, string nickname)
            : base(x, y)
        {
            Health = 100;
            IgnoreObstaclesCounter = 0;
            Previous_X = 1;
            Previous_Y = 1;
            Nickname = nickname;
        }

        public Player()
        {
            Previous_X = 1;
            Previous_Y = 1;
        }

        public override void Draw()
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.SetCursorPosition(X, Y);
            Console.Write("@");
        }

        public void Clean()
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.SetCursorPosition(Previous_X, Previous_Y);
            Console.Write(" ");
            Console.SetCursorPosition(X, Y);
        }

        public void DoNextStep(Map map)
        {
            IsMovingAllowed isMovingAllowed = map.IsThereFreePlace;
            if (IgnoreObstaclesCounter > 0)
            {
                isMovingAllowed = map.IsThereNoBorder;
                IgnoreObstaclesCounter--;
            }
            switch (Key)
            {
                case ConsoleKey.LeftArrow:
                    if (isMovingAllowed(X - 1, Y, false))
                    {
                        Previous_X = X;
                        X--;
                        Previous_Y = Y;
                    }
                    break;
                case ConsoleKey.RightArrow:
                    if (isMovingAllowed(X + 1, Y, false))
                    {
                        Previous_X = X;
                        X++;
                        Previous_Y = Y;
                    }
                    break;
                case ConsoleKey.UpArrow:
                    if (isMovingAllowed(X, Y - 1, false))
                    {
                        Previous_Y = Y;
                        Y--;
                        Previous_X = X;
                    } 
                    break;
                case ConsoleKey.DownArrow:
                    if (isMovingAllowed(X, Y + 1, false))
                    {
                        Previous_Y = Y;
                        Y++;
                        Previous_X = X;
                    }
                    break;
            }
            Key = ConsoleKey.Tab;
        }

        public void Hurt(int hitRange)
        {
            Health -= hitRange;
        }

        public bool IsAlive()
        {
            return Health > 0;
        }

        public void GetBonus(Bonus bonus)
        {
            Health += bonus.Energy;
            if (bonus is Blueberry)
                IgnoreObstaclesCounter = 7;
            bonus.Availiable = false;
        }
    }
}
