﻿using System;

namespace Lib
{
    public class Cherry: Bonus
    {
        public Cherry(int x, int y)
            : base(x, y, 22)
        {
        }

        public Cherry()
        {
            Energy = 22;
        }
        public override void Draw()
        {
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.SetCursorPosition(X, Y);
            Console.Write("*");
        }
    }
}
