﻿using System;

namespace Lib
{
    public class Fox : Monster
    {
        public Fox(int x, int y) : base(x, y)
        {
            HitRange = 15;
            PassNextSteps = 0;
        }

        public Fox()
        {
            HitRange = 15;
            PassNextSteps = 0;
        }

        public override void DoNextStep(Map map)
        {
            DoneStep = false;
            IsMovingAllowed isMovingAllowed = map.IsThereFreePlace;
            if (IgnoreObstaclesCounter > 0)
            {
                isMovingAllowed = map.IsThereNoBorder;
                IgnoreObstaclesCounter--;
            }
            if (PassNextSteps > 0)
            { 
                MoveRandom(isMovingAllowed, map);
                PassNextSteps -= 1;
            } 
            else
                CatchPlayer(isMovingAllowed, map);
        }

        private void CatchPlayer(IsMovingAllowed isMovingAllowed, Map map)
        {
            Previous_X = X;
            Previous_Y = Y;
            if (map.player.X < X && isMovingAllowed(X - 1, Y))
                X--;
            else
            if (map.player.X > X && isMovingAllowed(X + 1, Y))
                X++;
            if (map.player.Y < Y && isMovingAllowed(X, Y - 1))
                Y--;
            else
            if (map.player.Y > Y && isMovingAllowed(X, Y + 1))
                Y++;
            if (Previous_X != X || Previous_Y != Y)
                DoneStep = true;
        }

        public override void Draw()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.SetCursorPosition(X, Y);
            Console.Write("F");
        }

        public override void Hit(Player player)
        {
            player.Hurt(HitRange);
            PassNextSteps = 6;
        }
    }
}
