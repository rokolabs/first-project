﻿using System;
using System.IO;
using lib;

namespace task1._4
{
    class Program
    {
        static void Main(string[] args)
        {
            string inputFile = args[0];
            Random rnd = new Random();
            int value = rnd.Next(0, 50);
            string[] data = File.ReadAllLines(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, inputFile));
            Console.WriteLine("Content of file: ");
            int[] intData = ArrayFuncs.ToIntArray(data);
            Output.PrintArray(intData);
            int sumOfPos = ArrayFuncs.SumOfPos(intData);
            Console.WriteLine($"Summa of positive values: {sumOfPos}");
            int sumOfNeg = ArrayFuncs.SumOfNeg(intData);
            Console.WriteLine($"Summa of negative values: {sumOfNeg}");
            int sumBiggerthan = ArrayFuncs.SumBiggerThan(intData, value);
            Console.WriteLine($"Summa of values bigger than {value}: {sumBiggerthan}");
        }
    }
}
