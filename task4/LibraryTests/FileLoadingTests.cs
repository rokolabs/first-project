using Lib;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;

namespace LibraryTests
{
    public class FileLoadingTests
    {
        public string pathToData { get; set; }
        public string pathToConfig { get; set; }

        [SetUp]
        public void Setup()
        {
            pathToData = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "data");
            pathToConfig = Path.Combine(AppDomain.CurrentDomain.BaseDirectory);
        }

        [Test]
        public void LoadingLocalGameTest()
        {
            Loader loader = new Loader(pathToConfig);
            GameState gameState = new GameState();
            loader.LoadPreviousGame(gameState);

            Map map = gameState.MapExample;
            Player player = gameState.PlayerExample;
            List<BaseObject> drawable = gameState.DrawableObjects;
            List<IActor> actors = gameState.ActorsList;
            List<IHitter> hitters = gameState.HittersList;
            List<Bonus> bonuses = gameState.BonusesList;

            Assert.IsNotNull(map);
            Assert.IsNotNull(player);
            Assert.IsNotNull(drawable);
            Assert.IsNotNull(actors);
            Assert.IsNotNull(hitters);
            Assert.IsNotNull(bonuses);
        }

        [Test]
        public void NewLocalGameTest()
        {
            Loader loader = new Loader(pathToConfig);
            GameState gameState = new GameState();
            int level = 1;
            string levelName = "Level1.txt";
            string playerNickname = "Player1";

            loader.LoadLevelFromFile(level, levelName, playerNickname, gameState);

            Map map = gameState.MapExample;
            Player player = gameState.PlayerExample;
            List<BaseObject> drawable = gameState.DrawableObjects;
            List<IActor> actors = gameState.ActorsList;
            List<IHitter> hitters = gameState.HittersList;
            List<Bonus> bonuses = gameState.BonusesList;

            Assert.IsNotNull(map);
            Assert.IsNotNull(player);
            Assert.IsNotNull(drawable);
            Assert.IsNotNull(actors);
            Assert.IsNotNull(hitters);
            Assert.IsNotNull(bonuses);
        }

        [Test]
        public void NewServerGameTest()
        {
            Loader loader = new Loader(pathToConfig);
            string playerNickname = "Player1";

            for (int i = 1; i <= 5; i++)
            {
                GameState gameState = new GameState();
                loader.LoadLevelFromServer(i, playerNickname, gameState);

                Map map = gameState.MapExample;
                Player player = gameState.PlayerExample;
                List<BaseObject> drawable = gameState.DrawableObjects;
                List<IActor> actors = gameState.ActorsList;
                List<IHitter> hitters = gameState.HittersList;
                List<Bonus> bonuses = gameState.BonusesList;

                Assert.IsNotNull(map);
                Assert.IsNotNull(player);
                Assert.IsNotNull(drawable);
                Assert.IsNotNull(actors);
                Assert.IsNotNull(hitters);
                Assert.IsNotNull(bonuses);
            }
        }

        [Test]
        public void SaveAndLoadingServerGameTest()
        {
            Loader loader = new Loader(pathToConfig);
            string playerNickname = "Player1";

            GameState gameState = new GameState();
            loader.LoadLevelFromServer(1, playerNickname, gameState);

            Map map = gameState.MapExample;
            Player player = gameState.PlayerExample;
            List<BaseObject> drawable = gameState.DrawableObjects;
            List<IActor> actors = gameState.ActorsList;
            List<IHitter> hitters = gameState.HittersList;
            List<Bonus> bonuses = gameState.BonusesList;

            Saver saver = new Saver(pathToConfig);
            saver.SaveGameToServer(map, drawable);

            GameState newGameState = new GameState();

            loader.LoadPreviousGame(newGameState);

            map = newGameState.MapExample;
            player = newGameState.PlayerExample;
            drawable = newGameState.DrawableObjects;
            actors = newGameState.ActorsList;
            hitters = newGameState.HittersList;
            bonuses = newGameState.BonusesList;

            Assert.IsNotNull(map);
            Assert.IsNotNull(player);
            Assert.IsNotNull(drawable);
            Assert.IsNotNull(actors);
            Assert.IsNotNull(hitters);
            Assert.IsNotNull(bonuses);
        }


        [Test]
        public void LoadStatisticsTest()
        {
            Loader loader = new Loader(pathToConfig);
            var listOfData = loader.LoadStatistics();
            Assert.IsNotEmpty(listOfData);
        }
        [Test]
        public void CheckRecordsInStatisticsTest()
        {
            List<Obstacle> obstacles = new List<Obstacle>();
            List<Bonus> bonuses = new List<Bonus>();
            List<Monster> monsters = new List<Monster>();

            Map map = new(30, 30, obstacles, bonuses, monsters);
            Player player = new Player(1, 1, "Player10");
            map.Counter = 165;
            map.LevelId = 1;
            map.player = player;
            Saver saver = new Saver(pathToConfig);
            saver.UpdateStatistics(map);
            Assert.Pass();
        }
    }
}