﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Text.Json;

namespace Lib
{
    public class Saver
    {
        public string ConfigPath { get; set; }
        public string DataPath { get; set; }
        public string ConfigFullName { get; set; }
        public string SavingsName { get;}
        private string connectionString;

        public Saver(string path)
        {
            ConfigPath = path;
            DataPath = Path.Combine(ConfigPath, "data");
            ConfigFullName = Path.Combine(ConfigPath, "Config.txt");
            SavingsName = GetSavingsName();
            connectionString = GetConnectionData();

            if (SavingsName == String.Empty)
            {
                SetSavingName();
                SavingsName = GetSavingsName();
            }
        }

        private string GetConnectionData()
        {
            string[] data = File.ReadAllLines(ConfigFullName);
            for (int i = 0; i < data.Length; i++)
            {
                if (data[i] == "connection_data")
                    return data[i + 1];
            }
            return string.Empty;

        }

        private string GetSavingsName()
        {
            string[] data = File.ReadAllLines(ConfigFullName);
            for (int i = 0; i < data.Length; i++)
            {
                if (data[i] == "saves_name")
                    return data[i + 1];
            }
            return string.Empty;
        }

        public void SetSavingName()
        { 
            using (StreamWriter sw = new StreamWriter(ConfigFullName))
            {
                sw.WriteLine("saves_name");
                sw.WriteLine("saved_game.txt");
            }
        }

        public void Save(List<string> serializedObjects)
        {
            using (StreamWriter sw = new StreamWriter(Path.Combine(DataPath, SavingsName)))
            {
                for (int i = 0; i < serializedObjects.Count; i++)
                {
                    sw.WriteLine(serializedObjects[i]);
                }
            }
        }

        public List<string> GetDataFromGame(Map map, List<BaseObject> drawableObjects)
        {
            List<string> serializedObjects = new List<string>
            {
                "map",
                $"{map.Height},{map.Width}"
            };
            for (int i = 0; i < drawableObjects.Count; i++)
            {
                switch (drawableObjects[i])
                {
                    case Apple apple:
                        serializedObjects.Add("apple");
                        serializedObjects.Add(JsonSerializer.Serialize(apple));
                        break;
                    case Blueberry blueberry:
                        serializedObjects.Add("blueberry");
                        serializedObjects.Add(JsonSerializer.Serialize(blueberry));
                        break;
                    case Cherry cherry:
                        serializedObjects.Add("cherry");
                        serializedObjects.Add(JsonSerializer.Serialize(cherry));
                        break;
                    case Pineapple pineapple:
                        serializedObjects.Add("pineapple");
                        serializedObjects.Add(JsonSerializer.Serialize(pineapple));
                        break;
                    case Raspberry raspberry:
                        serializedObjects.Add("raspberry");
                        serializedObjects.Add(JsonSerializer.Serialize((Raspberry)drawableObjects[i]));
                        break;
                    case Bear bear:
                        serializedObjects.Add("bear");
                        serializedObjects.Add(JsonSerializer.Serialize(bear));
                        break;
                    case Fox fox:
                        serializedObjects.Add("fox");
                        serializedObjects.Add(JsonSerializer.Serialize(fox));
                        break;
                    case Wolf wolf:
                        serializedObjects.Add("wolf");
                        serializedObjects.Add(JsonSerializer.Serialize(wolf));
                        break;
                    case Bush bush:
                        serializedObjects.Add("bush");
                        serializedObjects.Add(JsonSerializer.Serialize(bush));
                        break;
                    case Stone stone:
                        serializedObjects.Add("stone");
                        serializedObjects.Add(JsonSerializer.Serialize(stone));
                        break;
                    case Tree tree:
                        serializedObjects.Add("tree");
                        serializedObjects.Add(JsonSerializer.Serialize(tree));
                        break;
                    case Wall wall:
                        serializedObjects.Add("wall");
                        serializedObjects.Add(JsonSerializer.Serialize(wall));
                        break;
                    case Player player:
                        serializedObjects.Add("player");
                        serializedObjects.Add(JsonSerializer.Serialize(player));
                        break;
                    default:
                        break;
                }
            }
            return serializedObjects;
        }

        internal void SaveResult(Map map)
        {
            using SqlConnection connection = new SqlConnection(connectionString);
            string savingStatistics = "INSERT INTO [Statistics] ([PlayerID], [DateTime], [Result], [LevelID]) VALUES (@PlayerID, @DateTime, @Result, @LevelID)";
            SqlCommand savingStatisticsCommand = new SqlCommand(savingStatistics, connection);
            decimal PlayerID = SavePlayer(map);
            savingStatisticsCommand.Parameters.AddWithValue(@"PlayerID", PlayerID);
            savingStatisticsCommand.Parameters.AddWithValue(@"DateTime", DateTime.Now);
            savingStatisticsCommand.Parameters.AddWithValue("@Result", map.Counter);
            savingStatisticsCommand.Parameters.AddWithValue("@LevelID", map.LevelId);
            connection.Open();
            savingStatisticsCommand.ExecuteNonQuery();
        }

        public void SaveGameToServer(Map map, List<BaseObject> drawableObjects)
        {
            DeleteSimularSaveExist(map.player.Nickname);
            decimal playerID = SavePlayer(map);
            List<SqlCommand> commands = new List<SqlCommand>();
            using SqlConnection connection = new SqlConnection(connectionString);

            SqlCommand mapsInsertCommand = CreateInsertIntoMapCommand(map, connection);
            for (int i = 0; i < map.Monsters.Count; i++)
            {
                commands.Add(CreateInsertIntoMonstersCommand(map.Monsters[i], connection));
            }
            for (int i = 0; i < map.Bonuses.Count; i++)
            {
                commands.Add(CreateInsertIntoBonusesCommand(map.Bonuses[i], connection));
            }
            for (int i = 0; i < map.Obstacles.Count; i++)
            {
                commands.Add(CreateInsertIntoObstaclesCommand(map.Obstacles[i], connection));
            }
            connection.Open();
            mapsInsertCommand.Parameters.AddWithValue("@PlayerID", playerID);
            decimal mapId = (decimal)mapsInsertCommand.ExecuteScalar();
            for (int i = 0; i < commands.Count; i++)
            {                
                commands[i].Parameters.AddWithValue("@MapID", mapId);
                commands[i].ExecuteNonQuery();
            }
        }

        public decimal SavePlayer(Map map)
        {
            using SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand playerInsertCommand = CreateInsertIntoPlayersCommand(map, connection);
            connection.Open();
            decimal playerID = (decimal)playerInsertCommand.ExecuteScalar();
            return playerID;
        }

        public void DeleteSimularSaveExist(string nickname)
        {
            using SqlConnection connection = new SqlConnection(connectionString);
            int playerID = GetPlayerId(nickname);
            string deletePlayer = $"DELETE FROM [Players] WHERE PlayerID = @PlayerID";
            SqlCommand deletePlayerCommand = new SqlCommand(deletePlayer, connection);
            deletePlayerCommand.Parameters.AddWithValue("@PlayerID", playerID);
            connection.Open();
            deletePlayerCommand.ExecuteNonQuery();
        }

        public int GetPlayerId(string nickname)
        {
            using SqlConnection connection = new SqlConnection(connectionString);
            int playerID;
            string selectPlayer = $"SELECT [PlayerID], [Nickname] FROM [Players] p WHERE p.Nickname = '{nickname}'";
            SqlCommand selectPlayerCommand = new SqlCommand(selectPlayer, connection);
            connection.Open();
            using (SqlDataReader reader = selectPlayerCommand.ExecuteReader())
            {
                while (reader.Read())
                {
                    playerID = (int)reader["PlayerID"];
                    return playerID;
                }
            }
            return 0;
        }

        public int GetMapId(int playerId)
        {
            string connectionString = "Server=DESKTOP-HMS6KRQ;Database=GameDb;Trusted_connection=True;";
            int MapId = 0;
            using SqlConnection connection = new SqlConnection(connectionString);
            string findMapId = $"Select MapID FROM maps m WHERE m.PlayerID = '{playerId}'";
            SqlCommand selectPlayerCommand = new SqlCommand(findMapId, connection);
            connection.Open();
            using (SqlDataReader reader = selectPlayerCommand.ExecuteReader())
            {
                while (reader.Read())
                {
                    MapId = (int)reader["MapID"];
                }
            }
            return MapId;
        }

        private static SqlCommand CreateInsertIntoMapCommand(Map map, SqlConnection connection)
        {
            string insertIntoMaps = "INSERT INTO [Maps] ([Height], [Width], [LevelID], [PlayerID], [StepsDone]) VALUES (@Height, @Width, @LevelID, @PlayerID, @StepsDone); SELECT SCOPE_IDENTITY()";
            SqlCommand mapsInsertCommand = new SqlCommand(insertIntoMaps, connection);
            mapsInsertCommand.Parameters.AddWithValue("@Height", map.Height);
            mapsInsertCommand.Parameters.AddWithValue("@Width", map.Width);
            mapsInsertCommand.Parameters.AddWithValue("@LevelID", map.LevelId);
            mapsInsertCommand.Parameters.AddWithValue("@StepsDone", map.Counter);
            return mapsInsertCommand;
        }

        private static SqlCommand CreateInsertIntoPlayersCommand(Map map, SqlConnection connection)
        {
            string insertIntoPlayers = "INSERT INTO [Players] ([Nickname], [Previous_X], [Previous_Y], [X], [Y], [Health], [IgnoreObstaclesCounter], [DoneStep], [PressedKey])" +
                " VALUES (@Nickname, @Previous_X, @Previous_Y, @X, @Y, @Health, @IgnoreObstaclesCounter, @DoneStep, @PressedKey) SELECT SCOPE_IDENTITY();";
            SqlCommand playerInsertCommand = new SqlCommand(insertIntoPlayers, connection);
            playerInsertCommand.Parameters.AddWithValue("@Nickname", map.player.Nickname);
            playerInsertCommand.Parameters.AddWithValue("@Previous_X", map.player.Previous_X);
            playerInsertCommand.Parameters.AddWithValue("@Previous_Y", map.player.Previous_Y);
            playerInsertCommand.Parameters.AddWithValue("@X", map.player.X);
            playerInsertCommand.Parameters.AddWithValue("@Y", map.player.Y);
            playerInsertCommand.Parameters.AddWithValue("@Health", map.player.Health);
            playerInsertCommand.Parameters.AddWithValue("@IgnoreObstaclesCounter", map.player.IgnoreObstaclesCounter);
            playerInsertCommand.Parameters.AddWithValue("@DoneStep", map.player.DoneStep);
            playerInsertCommand.Parameters.AddWithValue("@PressedKey", map.player.Key);
            return playerInsertCommand;
        }

        private static SqlCommand CreateInsertIntoMonstersCommand(Monster monster, SqlConnection connection)
        {
            string insertIntoMonsters = "INSERT INTO [Monsters] ([PassNextSteps], [HitRange], [Previous_X], [Previous_Y], [IgnoreObstaclesCounter], [DoneStep], [X], [Y], [Type], [MapID]) " +
                "VALUES (@PassNextSteps, @HitRange, @Previous_X, @Previous_Y, @IgnoreObstaclesCounter, @DoneStep, @X, @Y, @Type, @MapID);";
            SqlCommand monstersInsertCommand = new SqlCommand(insertIntoMonsters, connection);
            monstersInsertCommand.Parameters.AddWithValue("@PassNextSteps", monster.PassNextSteps);
            monstersInsertCommand.Parameters.AddWithValue("@HitRange", monster.HitRange);
            monstersInsertCommand.Parameters.AddWithValue("@Previous_X", monster.Previous_X);
            monstersInsertCommand.Parameters.AddWithValue("@Previous_Y", monster.Previous_Y);
            monstersInsertCommand.Parameters.AddWithValue("@IgnoreObstaclesCounter", monster.IgnoreObstaclesCounter);
            monstersInsertCommand.Parameters.AddWithValue("@DoneStep", monster.DoneStep);
            AddBaseParameters(monster, monstersInsertCommand);
            return monstersInsertCommand;
        }

        private static SqlCommand CreateInsertIntoBonusesCommand(Bonus bonus, SqlConnection connection)
        {
            string insertIntoBonuses = "INSERT INTO [Bonuses] ([Energy], [Availiable], [X], [Y], [Type], [MapID]) " +
                "VALUES (@Energy, @Availiable, @X, @Y, @Type, @MapID);";
            SqlCommand bonusesInsertCommand = new SqlCommand(insertIntoBonuses, connection);
            bonusesInsertCommand.Parameters.AddWithValue("@Energy", bonus.Energy);
            bonusesInsertCommand.Parameters.AddWithValue("@Availiable", bonus.Availiable);
            AddBaseParameters(bonus, bonusesInsertCommand);
            return bonusesInsertCommand;
        }

        private static SqlCommand CreateInsertIntoObstaclesCommand(Obstacle obstacle, SqlConnection connection)
        {
            string insertIntoObstacles = "INSERT INTO [Obstacles] ([HitRange], [IsPassable], [X], [Y], [Type], [MapID]) " +
                "VALUES (@HitRange, @IsPassable, @X, @Y, @Type, @MapID);";
            SqlCommand obstaclesInsertCommand = new SqlCommand(insertIntoObstacles, connection);
            obstaclesInsertCommand.Parameters.AddWithValue("@HitRange", obstacle.HitRange);
            obstaclesInsertCommand.Parameters.AddWithValue("@IsPassable", obstacle.IsPassable);
            AddBaseParameters(obstacle, obstaclesInsertCommand);
            return obstaclesInsertCommand;
        }

        private static void AddBaseParameters(BaseObject baseObject, SqlCommand InsertCommand)
        {
            InsertCommand.Parameters.AddWithValue("@X", baseObject.X);
            InsertCommand.Parameters.AddWithValue("@Y", baseObject.Y);
            var myType = Enum.Parse(typeof(BaseObject.ObjectTypes), baseObject.GetType().Name);
            InsertCommand.Parameters.AddWithValue("@Type", myType);
        }

        public void UpdateStatistics(Map map)
        {
            using SqlConnection connection = new SqlConnection(connectionString);
            
            string getLevelStats = $"SELECT [Statistics].[PlayerID], [Players].[Nickname], [Statistics].[DateTime], [Statistics].[Result], [Statistics].[LevelID] FROM[GameDb].[dbo].[Statistics] INNER JOIN[GameDb].[dbo].[Players] ON[Statistics].[PlayerID] = [Players].[PlayerID] WHERE [Statistics].[LevelID] = @LevelID";
            string getLevelStatsCount = $"SELECT COUNT ([Statistics].[PlayerID]) FROM[GameDb].[dbo].[Statistics] INNER JOIN[GameDb].[dbo].[Players] ON[Statistics].[PlayerID] = [Players].[PlayerID] WHERE [Statistics].[LevelID] = @LevelID";
            string updateStats = "UPDATE [Statistics] SET [PlayerID] = @PlayerID, [DateTime] = @DateTime, [Result] = @Result, [LevelID] = @LevelID Where [PlayerId] = @PreviousPlayerID";
            SqlCommand getLevelStatsCommand = new SqlCommand(getLevelStats, connection);
            getLevelStatsCommand.Parameters.AddWithValue("@LevelID", map.LevelId);
            SqlCommand getLevelStatsCountCommand = new SqlCommand(getLevelStatsCount, connection);
            SqlCommand updateStatsCommand = new SqlCommand(updateStats, connection);
            getLevelStatsCountCommand.Parameters.AddWithValue("@LevelID", map.LevelId);
            connection.Open();
            int count = (int)getLevelStatsCountCommand.ExecuteScalar();
            int previousPlayerId = 0;
            if (count < 10)
            {
                SqlCommand savingStatisticsCommand = GetSavingStatsCommand(map, connection);
                savingStatisticsCommand.ExecuteNonQuery();
            }
            else
            {
                using SqlDataReader reader = getLevelStatsCommand.ExecuteReader();
                while (reader.Read())
                {
                    if ((int)reader["Result"] > map.Counter)
                    {
                        previousPlayerId = (int)reader["PlayerID"];
                        break;
                    }
                }
                if (previousPlayerId > 0)
                {
                    SaveResult(map);
                    //decimal PlayerID = SavePlayer(map);
                    //updateStatsCommand.Parameters.AddWithValue("@PlayerID", PlayerID);
                    //updateStatsCommand.Parameters.AddWithValue("@DateTime", DateTime.Now);
                    //updateStatsCommand.Parameters.AddWithValue("@Result", map.Counter);
                    //updateStatsCommand.Parameters.AddWithValue("@LevelID", map.LevelId);
                    //updateStatsCommand.ExecuteNonQuery();
                }

            }
        }

        private SqlCommand GetSavingStatsCommand(Map map, SqlConnection connection)
        {
            string savingStatistics = "INSERT INTO [Statistics] ([PlayerID], [DateTime], [Result], [LevelID]) VALUES (@PlayerID, @DateTime, @Result, @LevelID)";
            SqlCommand savingStatisticsCommand = new SqlCommand(savingStatistics, connection);
            decimal PlayerID = SavePlayer(map);
            savingStatisticsCommand.Parameters.AddWithValue(@"PlayerID", PlayerID);
            savingStatisticsCommand.Parameters.AddWithValue(@"DateTime", DateTime.Now);
            savingStatisticsCommand.Parameters.AddWithValue("@Result", map.Counter);
            savingStatisticsCommand.Parameters.AddWithValue("@LevelID", map.LevelId);
            return savingStatisticsCommand;
        }
    }
}
