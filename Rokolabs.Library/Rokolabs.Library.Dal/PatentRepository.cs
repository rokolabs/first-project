﻿using Dapper;
using Microsoft.Extensions.Logging;
using Rokolabs.Library.Dal.DTOs;
using Rokolabs.Library.Dal.Interfaces;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Rokolabs.Library.Dal
{
    public class PatentRepository : IPatentRepository
    {
        private readonly ILogger<PatentRepository> _logger;
        private readonly DBOptions _options;

        public PatentRepository(ILogger<PatentRepository> logger, DBOptions options)
        {
            _logger = logger;
            _options = options;
        }

        public void DeletePatent(int patentId)
        {
            using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
                var query = "DELETE FROM [Patents] WHERE [PatentID] = @patentId";
                db.Execute(query, new { patentId });
            }
        }

        public List<PatentDto> GetAllPatents()
        {
            using IDbConnection db = new SqlConnection(_options.ConnectionString);
            return db.Query<PatentDto>("GetAllPatents",
                commandType: CommandType.StoredProcedure
                ).ToList();
        }
        public List<PatentDto> GetAllPatentsDateAsc()
        {
            using IDbConnection db = new SqlConnection(_options.ConnectionString);
            return db.Query<PatentDto>("GetAllPatentsDateASC",
                commandType: CommandType.StoredProcedure
                ).ToList();
        }

        public List<PatentDto> GetAllPatentsDateDesc()
        {
            using IDbConnection db = new SqlConnection(_options.ConnectionString);
            return db.Query<PatentDto>("GetAllPatentsDateDESC",
                commandType: CommandType.StoredProcedure
                ).ToList();
        }

        public int InsertPatent(PatentDto patent)
        {
            using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
                var query = "INSERT INTO [Patents] ([Title], [PatentRegistrationNumber], [CountryID], [ApplicationDate], [PublicationDate], [PagesCount], [Description]) " +
                    "VALUES (@title, @patentRegistrationNumber, @countryID, @applicationDate, @publicationDate, @pagesCount, @description); SELECT CAST(SCOPE_IDENTITY() as int);";
                return db.Query<int>(query, patent).FirstOrDefault();
            }
        }

        public List<PatentDto> SearchPatents(PatentDto patent)
        {
            using IDbConnection db = new SqlConnection(_options.ConnectionString);
            return db.Query<PatentDto>("GetPatentsByTitle",
                new { patent.Title },
                commandType: CommandType.StoredProcedure
                ).ToList();
        }

        public List<PatentDto> SearchPatentsByAuthor(AuthorDto author)
        {
            using IDbConnection db = new SqlConnection(_options.ConnectionString);
            return db.Query<PatentDto>("GetPatentsByAuthor",
                new { author.AuthorId },
                commandType: CommandType.StoredProcedure
                ).ToList();
        }

        public List<PatentDto> SearchPatentsByAuthorDateAsc(AuthorDto author)
        {
            using IDbConnection db = new SqlConnection(_options.ConnectionString);
            return db.Query<PatentDto>("GetPatentsByAuthorDateASC",
                new { author.AuthorId },
                commandType: CommandType.StoredProcedure
                ).ToList();
        }

        public List<PatentDto> SearchPatentsByAuthorDateDesc(AuthorDto author)
        {
            using IDbConnection db = new SqlConnection(_options.ConnectionString);
            return db.Query<PatentDto>("GetPatentsByAuthorDateDESC",
                new { author.AuthorId },
                commandType: CommandType.StoredProcedure
                ).ToList();
        }

        public List<PatentDto> SearchPatentsDateAsc(PatentDto patent)
        {
            using IDbConnection db = new SqlConnection(_options.ConnectionString);
            return db.Query<PatentDto>("GetPatentsByTitleDateASC",
                new { patent.Title },
                commandType: CommandType.StoredProcedure
                ).ToList();
        }

        public List<PatentDto> SearchPatentsDateDesc(PatentDto patent)
        {
            using IDbConnection db = new SqlConnection(_options.ConnectionString);
            return db.Query<PatentDto>("GetPatentsByTitleDateDESC",
                new { patent.Title },
                commandType: CommandType.StoredProcedure
                ).ToList();
        }

        public PatentDto GetPatent(PatentDto patent)
        {
            using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
                var query = "SELECT [Patents].[PatentID], [Patents].[Title], [Patents].[PatentRegistrationNumber], [Patents].[CountryID] FROM [dbo].[Patents] WHERE [Patents].[PatentRegistrationNumber] = @PatentRegistrationNumber AND [Patents].[CountryID] = @CountryID";
                return db.Query<PatentDto>(query, patent).FirstOrDefault();
            }
        }
    }
}
