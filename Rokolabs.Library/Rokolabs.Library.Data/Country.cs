﻿namespace Rokolabs.Library.Data
{
    public class Country
    {
        public int CountryID { get; set; }
        public string CountryName { get; set; }
    }
}
