﻿namespace Lib
{
    public abstract class BaseObject
    {
        public int X { get; set; }
        public int Y { get; set; }
        public enum ObjectTypes
        {
            Apple,
            Blueberry,
            Cherry,
            Pineapple,
            Raspberry,
            Bear,
            Fox,
            Wolf,
            Bush,
            Stone,
            Tree,
            Wall
        }

        public BaseObject(int x, int y)
        {
            X = x;
            Y = y;
        }
        public BaseObject()
        {}

        public abstract void Draw();
    }
}
