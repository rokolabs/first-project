﻿namespace Lib
{
    public abstract class Obstacle : BaseObject
    {
        public int HitRange { get; set; }
        public bool IsPassable { get; set; }
        public Obstacle(int x, int y)
            : base(x, y)
        { }
        public Obstacle()
        { }
    }
}
