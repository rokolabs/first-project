﻿using System;

namespace Lib
{
    public class Wolf: Monster
    {
        public Wolf(int x, int y) : base(x, y)
        {
            HitRange = 25;
        }

        public Wolf()
        {
            HitRange = 25;
        }

        public override void Draw()
        {
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.SetCursorPosition(X, Y);
            Console.Write("W");
        }

        public override void DoNextStep(Map map)
        {
            IsMovingAllowed isMovingAllowed = map.IsThereFreePlace;
            if (IgnoreObstaclesCounter > 0)
            {
                isMovingAllowed = map.IsThereNoBorder;
                IgnoreObstaclesCounter--;
            }
            MoveRandom(isMovingAllowed, map);
        }
    }
}
