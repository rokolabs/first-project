﻿using System;
using System.Collections.Generic;
using System.IO;
using JournalMaker;
using ExternalFilesWorker;

namespace LogWorker
{
    public class LoaderFromLogFile
    {
        public static JournalLog LoadJournal(string pathToJournal, string journalName, string pathToSource)
        {
            JournalLog journal = new JournalLog(pathToJournal, pathToSource);
            string logData = File.ReadAllText(Path.Combine(pathToJournal, journalName));
            string[] stringRecords = logData.Replace("\r", String.Empty).Split(journal.finalSeparator, StringSplitOptions.RemoveEmptyEntries);
            if (stringRecords[0] == "program started\n")
            {
                List<JournalRecord> journalRecords = new List<JournalRecord>();
                for (int i = 1; i < stringRecords.Length; i++)
                {
                    string[] recordInfo = stringRecords[i].Split("\n", StringSplitOptions.RemoveEmptyEntries);
                    if (recordInfo.Length > 1)
                        journalRecords.Add(new JournalRecord(
                            DateTime.Parse(recordInfo[0]), recordInfo[1..recordInfo.Length], journal.separator));
                }
                journal.Records = journalRecords;
            }
            else
            {
                Console.WriteLine("The backup folder contains incorrect log file");
                Console.WriteLine("The backup folder was cleaned. Watching starts now");
                DirectoryInfo info = new DirectoryInfo(pathToSource)
                {
                    Attributes = FileAttributes.Normal
                };
                FileInfo[] files = info.GetFiles("*.txt", SearchOption.AllDirectories);
                info = new DirectoryInfo(pathToJournal)
                {
                    Attributes = FileAttributes.Normal
                };
                FileInfo[] wrongFiles = info.GetFiles("*.txt", SearchOption.AllDirectories);
                for (int i = 0; i < wrongFiles.Length; i++)
                {
                    File.Delete(wrongFiles[i].FullName);
                }
                journal.CreateFirstRecord(files);
                SaverToLogFile.SaveJournal(journal, pathToJournal);
                BackupCreator.CreateFilesBackup(journal.Records[^1], pathToJournal);
            }
            return journal;
        }
    }
}
