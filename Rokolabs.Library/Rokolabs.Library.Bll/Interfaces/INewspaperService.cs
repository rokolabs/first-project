﻿using Rokolabs.Library.Data;
using System.Collections.Generic;

namespace Rokolabs.Library.Bll.Interfaces
{
    public interface INewspaperService
    {
        List<Newspaper> GetAllNewspapers();
        List<Newspaper> GetAllNewspapersDateAsc();
        List<Newspaper> GetAllNewspapersDateDesc();
        int InsertNewspaper(Newspaper newspaper);
        List<Newspaper> SearchNewspaper(Newspaper newspaper);
        List<Newspaper> SearchNewspaperDateAsc(Newspaper newspaper);
        List<Newspaper> SearchNewspaperDateDesc(Newspaper newspaper);
        void DeleteNewspaper(int newspaperId);
        List<Newspaper> SearchNewspaperByPublicationOffice(string publishingOfficeName);
        List<Newspaper> SearchNewspaperByPublicationOfficeDateAsc(string publishingOfficeName);
        List<Newspaper> SearchNewspaperByPublicationOfficeDateDesc(string publishingOfficeName);
        Newspaper GetNewspaper(Newspaper newspaper);
        Newspaper GetNewspaperByISSN(Newspaper newspaper);
    }
}
