﻿using Lib;
using System;

namespace task4
{
    class Program
    {
        static void Main(string[] args)
        {
            Engine engine = new Engine();
            engine.Start();
        }
    }
}
