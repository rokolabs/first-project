﻿using System;

namespace lib
{
    public static class ArrayFuncs
    {
        static Random rnd = new Random();
        public static int[] CreateArray(int length)
        {
            int[] arr = new int[length];
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = rnd.Next();
            }
            return arr;
        }

        public static double[] CreateDoubleArray(int len)
        {
            double[] arr = new double[len];
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = 20 * rnd.NextDouble() - 10;
            }
            return arr;
        }

        public static int FindMax(int[] arr)
        {
            int maxItem = arr[0];
            for (int i = 1; i < arr.Length; i++)
            {
                if (maxItem < arr[i])
                    maxItem = arr[i];
            }
            return maxItem;
        }

        public static int FindMin(int[] arr)
        {
            int minItem = arr[0];
            for (int i = 0; i < arr.Length; i++)
            {
                if (minItem > arr[i])
                    minItem = arr[i];
            }
            return minItem;
        }

        public static double FindAverage(int[] arr)
        {
            int sum = 0;
            int count = arr.Length;
            for (int i = 0; i < arr.Length; i++)
            {
                sum += arr[i];
            }
            return sum / count;
        }

        public static void SortArray(int[] arr)
        {
            // Use bubble sort
            int temp;
            for (int i = 0; i < arr.Length; i++)
            {
                for (int j = i + 1; j < arr.Length; j++)
                {
                    if (arr[i] > arr[j])
                    {
                        temp = arr[i];
                        arr[i] = arr[j];
                        arr[j] = temp;
                    }
                }
            }
        }

        public static double FindMedian(int[] arr)
        {
            int[] tempArr = new int[arr.Length];
            int midIndex;
            double median;
            Array.Copy(arr, tempArr, arr.Length);
            SortArray(tempArr);
            midIndex = arr.Length / 2;
            if (arr.Length % 2 == 1)
                median = tempArr[midIndex-1];
            else
                median = (tempArr[midIndex-1] + tempArr[midIndex]) / 2.0;
            return median;
        }

        public static int CountPositiveNums(double[] arr)
        {
            int count = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] > 0)
                    count++;
            }
            return count;
        }

        public static void TurnNegToZero(int[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] < 0)
                    arr[i] = 0;
            }
        }

        public static int[] ToIntArray(string[] arr)
        {
            int[] newArr = new int[arr.Length];
            for (int i = 0; i < arr.Length; i++)
            {
                int.TryParse(arr[i], out newArr[i]);
            }
            return newArr;
        }

        public static string[] ToStringArray(int[] arr)
        {
            string[] data = new string[arr.Length];
            for (int i = 0; i < arr.Length; i++)
            {
                data[i] = arr[i].ToString();
            }
            return data;
        }

        public static int SumOfPos(int[] arr)
        {
            int summ = 0;   
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] > 0)
                    summ += arr[i];
            }
            return summ;
        }

        public static int SumOfNeg(int[] arr)
        {
            int summ = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] < 0)
                    summ += arr[i];
            }
            return summ;
        }

        public static int SumBiggerThan(int[] arr, int value)
        {
            int summ = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] > value)
                    summ += arr[i];
            }
            return summ;
        }
    }
}

