﻿using System;
using System.IO;
using lib;

namespace task1._3
{
    class Program
    {
        static void Main(string[] args)
        {
            string inputFile = args[0];
            string outputFile = args[1];
            string[] data = File.ReadAllLines(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, inputFile));
            Console.WriteLine("File content:");
            int[] intData = ArrayFuncs.ToIntArray(data);
            Output.PrintArray(intData);
            Console.WriteLine("Turn all negative values to zero: ");
            ArrayFuncs.TurnNegToZero(intData);
            Output.PrintArray(intData);
            Console.WriteLine("Sorted array: ");
            ArrayFuncs.SortArray(intData);
            Output.PrintArray(intData);
            string[] newData = ArrayFuncs.ToStringArray(intData);
            File.WriteAllLines(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, outputFile), newData);
        }
    }
}
