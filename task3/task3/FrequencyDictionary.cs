﻿using System;
using System.Collections.Generic;
using System.Text;

namespace task3
{
    class FrequencyDictionary
    {
        public Dictionary<char, int> SymbolsDictionary = new Dictionary<char, int>();
        public Dictionary<string, int> WordsDictionary = new Dictionary<string, int>();
        private char[] delimiterChars = { ' ', ',', '.', ':', ';', '!', '?', '\t', '\n', '\r'};
        public void FindAllWords(string[] text)
        {
            for (int i = 0; i < text.Length; i++)
            {
                string[] wordsInLine = text[i].Split(delimiterChars, StringSplitOptions.RemoveEmptyEntries);
                for (int j = 0; j < wordsInLine.Length; j++)
                {
                    string word = wordsInLine[j].ToLower();
                    if (WordsDictionary.ContainsKey(word))
                        WordsDictionary[word] += 1;
                    else
                        WordsDictionary.Add(word, 1);
                }
            }
        }

        public void FindAllSymbols(string[] text)
        {
            for (int i = 0; i < text.Length; i++)
            {
                char[] chars = text[i].ToCharArray();
                for (int j = 0; j < chars.Length; j++)
                {
                    char symbol = Char.ToLower(chars[j]);
                    //if (Char.IsPunctuation(symbol))
                    //    continue;
                    //else
                    //{
                        if (SymbolsDictionary.ContainsKey(symbol))
                            SymbolsDictionary[symbol] += 1;
                        else
                            SymbolsDictionary.Add(symbol, 1);
                    //}
                }
            }
        }
    }
}
