﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace task3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter path to files:");
            string pathToFile = Console.ReadLine();

            string[] files = GetAllFiles(pathToFile);
            if (files.Length != 0)
            {
                for (int j = 0; j < 10; j++)
                {
                    FrequencyDictionary fw = new FrequencyDictionary();
                    Stopwatch sw = new Stopwatch();
                    sw.Start();
                    for (int i = 0; i < files.Length; i++)
                    {
                        string[] text = File.ReadAllLines(files[i]);
                        //long ticks = Wrapper(() => fw.FindAllWords(text));
                        //Console.WriteLine($"Time: {ticks}");
                        //ticks = Wrapper(() => fw.FindAllSymbols(text));
                        //Console.WriteLine($"Time: {ticks}");
                        fw.FindAllWords(text);
                        fw.FindAllSymbols(text);
                    }
                    sw.Stop();
                    Console.WriteLine($"Time: {sw.ElapsedTicks}");
                    CreateOutputFile(fw.WordsDictionary, "words.txt");
                    CreateOutputFile(fw.SymbolsDictionary, "Symbols.txt");
                }
            }
        }

        public static long Wrapper(Action action)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            action();
            sw.Stop();
            return sw.ElapsedTicks;
        }

        public static void PrintDictionary<T>(Dictionary<T, int> dict)
        {
            foreach (var item in dict)
            {
                Console.WriteLine($"{item.Key} - {item.Value}");
            }
        }

        public static string[] GetAllFiles(string path)
        {
            DirectoryInfo info = new DirectoryInfo(path);
            try
            {
                var files = info.GetFiles("*.txt", SearchOption.AllDirectories);
                string[] filenames = new string[files.Length];
                for (int i = 0; i < files.Length; i++)
                {
                    filenames[i] = files[i].FullName;
                }
                return filenames;
            }
            catch (IOException)
            {
                Console.WriteLine("Wrong directory");
                return new string[0];
            }
        }

        public static void CreateOutputFile<T>(Dictionary<T, int> dict, string filename)
        {
            using (StreamWriter sw = new StreamWriter(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, filename)))
            {
                foreach (var item in dict)
                {
                    sw.WriteLine($"{item.Key} - {item.Value}");
                }
            }
        }
    }
}
