﻿using System;

namespace Lib
{
    class Wall: Obstacle
    {
        public Wall(int x, int y)
            : base(x, y)
        {
            IsPassable = false;
            HitRange = 0;
        }

        public Wall()
        {
            IsPassable = false;
            HitRange = 0;
        }

        public override void Draw()
        {
            Console.ForegroundColor = ConsoleColor.DarkGray;
            Console.SetCursorPosition(X, Y);
            Console.Write("=");
        }
    }
}
