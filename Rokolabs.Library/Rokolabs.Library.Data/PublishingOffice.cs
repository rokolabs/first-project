﻿namespace Rokolabs.Library.Data
{
    public class PublishingOffice
    {
        public int PublishingOfficeID { get; set; }
        public string PublishingOfficeName { get; set; }
    }
}
