﻿  using System;

namespace Rokolabs.Library.Dal.DTOs
{
    public class PatentDto
    {
        public int PatentID { get; set; }
        public string Title { get; set; }
        public int PatentRegistrationNumber { get; set; }
        public int CountryID { get; set; }
        public string CountryName { get; set; }
        public DateTime ApplicationDate { get; set; }
        public DateTime PublicationDate { get; set; }
        public short PagesCount { get; set; }
        public string Description { get; set; }
    }
}
