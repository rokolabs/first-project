﻿namespace Rokolabs.Library.Dal.Interfaces
{
    public interface ICountryRepository
    {
        int FindCountry(string countryName);
        int InsertCountry(string countryName);
    }
}
