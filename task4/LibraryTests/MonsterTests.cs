﻿using Lib;
using NUnit.Framework;
using System.Collections.Generic;

namespace LibraryTests
{
    public class MonsterTests
    {

        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void EatBonusTest()
        {
            List<Obstacle> obstacles = new List<Obstacle>();
            List<Bonus> bonuses = new List<Bonus>();
            List<Monster> monsters = new List<Monster>();

            Map map = new Map(10, 10, obstacles, bonuses, monsters);
            int previousHealth = 100;

            Player player = new Player(6, 5, "player1");
            player.Health = previousHealth;
            map.player = player;

            Bear bear = new Bear(6, 6);
            Wolf wolf = new Wolf(6, 6);
            Fox fox = new Fox(6, 6);

            Blueberry blueberry = new Blueberry(6, 7);
            Raspberry raspberry = new Raspberry(6, 8);

            monsters.Add(bear);
            monsters.Add(wolf);
            monsters.Add(fox);

            bonuses.Add(blueberry);
            bonuses.Add(raspberry);

            foreach (var monster in monsters)
            {
                monster.Y++;
                Bonus bonus = map.TryFindBonus(monster.X, monster.Y);
                if (bonus != null)
                {
                    monster.GetBonus(bonus);
                    Assert.IsTrue(monster.IgnoreObstaclesCounter == 7);
                }
            }
            bear.Y++; 
            Bonus anotherBonus = map.TryFindBonus(bear.X, bear.Y);
            if (anotherBonus != null)
            {
                int prevHitRange = bear.HitRange;
                bear.GetBonus(anotherBonus);
                Assert.IsTrue(bear.HitRange == prevHitRange * 2);
            }   
        }

        [Test]
        public void MeetObstacleTest()
        {
            List<Obstacle> obstacles = new List<Obstacle>();
            List<Bonus> bonuses = new List<Bonus>();
            List<Monster> monsters = new List<Monster>();

            Map map = new Map(30, 30, obstacles, bonuses, monsters);
            Bear bear = new Bear(11, 10);
            Tree tree = new Tree(11, 10);
            map.Monsters.Add(bear);
            map.Obstacles.Add(tree);
            Assert.IsFalse(map.IsThereFreePlace(bear.X, bear.Y));
        }

    }
}
