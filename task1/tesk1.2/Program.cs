﻿using System;
using lib;

namespace task1._2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Input array lengh: ");
            int length = int.Parse(Console.ReadLine());
            if (length <= 0)
                Console.WriteLine("Incorrect value");
            else 
            {
                double[] arr = ArrayFuncs.CreateDoubleArray(length);
                Output.PrintArray(arr);
                int countPositive = ArrayFuncs.CountPositiveNums(arr);
                Console.WriteLine($"Count of positive numbers in array: {countPositive}");
            }
        }
    }
}
