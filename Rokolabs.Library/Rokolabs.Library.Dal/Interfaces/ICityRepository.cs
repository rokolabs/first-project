﻿using Rokolabs.Library.Dal.DTOs;

namespace Rokolabs.Library.Dal.Interfaces
{
    public interface ICityRepository
    {
        int FindCity(string cityName);
        int InsertCity(string cityName);
    }
}
