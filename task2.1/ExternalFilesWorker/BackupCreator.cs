﻿using System.IO;
using JournalMaker;

namespace ExternalFilesWorker
{
    public class BackupCreator
    {
        public static void CreateFileBackup(string pathToBackup, string localFileName, string sourceFileName)
        {
            File.Copy(sourceFileName, Path.Combine(pathToBackup, localFileName));
        }

        public static void CreateFilesBackup(JournalRecord jr, string pathToBackup)
        {
            for (int i = 0; i < jr.Files.Count; i++)
            {
                if (jr.Files[i].Change != "not changed" && jr.Files[i].Change != "deleted")
                    File.Copy(jr.Files[i].Fullname, Path.Combine(pathToBackup, jr.Files[i].LocalFilename));
            }
        }

        public static void CleanDirectory(string pathToJournal, string journalName)
        {
            File.Delete(Path.Combine(pathToJournal, journalName));
            DirectoryInfo info = new DirectoryInfo(pathToJournal)
            {
                Attributes = FileAttributes.Normal
            };
            var files = info.GetFiles("*.txt");
            for (int i = 0; i < files.Length; i++)
            {
                File.Delete(files[i].FullName);
            }
        }
    }
}
