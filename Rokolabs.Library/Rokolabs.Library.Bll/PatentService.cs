﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using Rokolabs.Library.Bll.Interfaces;
using Rokolabs.Library.Dal.DTOs;
using Rokolabs.Library.Dal.Interfaces;
using Rokolabs.Library.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Rokolabs.Library.Bll
{
    public class PatentService : IPatentService
    {
        private readonly ILogger<PatentService> _logger;
        private readonly IPatentRepository _patentRepository;
        private readonly IAuthorRepository _authorRepository;
        private readonly IMapper _mapper;
        public PatentService(ILogger<PatentService> logger, IMapper mapper, IPatentRepository patentRepository, IAuthorRepository authorRepository)
        {
            _logger = logger;
            _patentRepository = patentRepository;
            _authorRepository = authorRepository;
            _mapper = mapper;
        }

        public void DeletePatent(int patentId)
        {
            _patentRepository.DeletePatent(patentId);
        }

        public List<Patent> GetAllPatents()
        {
            var patents = _mapper.Map<List<Patent>>(_patentRepository.GetAllPatents());
            AddAuthors(patents);
            return patents;
        }
        private void AddAuthors(List<Patent> patents)
        {
            var authors = _authorRepository.GetAuthorsByPatentsIds(patents.Select(p => p.PatentID));
            foreach (var patent in patents)
            {
                patent.Authors = _mapper.Map<List<Author>>(authors.Where(p => patent.PatentID == p.PatentId));
            }
        }

        public List<Patent> GetAllPatentsDateAsc()
        {
            var patents = _mapper.Map<List<Patent>>(_patentRepository.GetAllPatentsDateAsc());
            AddAuthors(patents);
            return patents;
        }

        public List<Patent> GetAllPatentsDateDesc()
        {
            var patents = _mapper.Map<List<Patent>>(_patentRepository.GetAllPatentsDateDesc());
            AddAuthors(patents);
            return patents;
        }

        public int InsertPatent(Patent patent)
        {
            return _patentRepository.InsertPatent(_mapper.Map<PatentDto>(patent));
        }

        public List<Patent> SearchPatent(Patent patent)
        {
            var patents = _mapper.Map<List<Patent>>(_patentRepository.SearchPatents(_mapper.Map<PatentDto>(patent)));
            AddAuthors(patents);
            return patents;
        }

        public List<Patent> SearchPatentByAuthor(Author author)
        {
            var fullAuthor = _mapper.Map<Author>(_authorRepository.GetAuthor(_mapper.Map<AuthorDto>(author)));
            List<Patent> patents = new List<Patent>();
            if (fullAuthor != null)
                patents = _mapper.Map<List<Patent>>(_patentRepository.SearchPatentsByAuthor(_mapper.Map<AuthorDto>(fullAuthor)));
            return patents;
        }

        public List<Patent> SearchPatentByAuthorDateAsc(Author author)
        {
            var fullAuthor = _mapper.Map<Author>(_authorRepository.GetAuthor(_mapper.Map<AuthorDto>(author)));
            List<Patent> patents = new List<Patent>();
            if (fullAuthor != null)
                patents = _mapper.Map<List<Patent>>(_patentRepository.SearchPatentsByAuthorDateAsc(_mapper.Map<AuthorDto>(fullAuthor)));
            return patents;
        }

        public List<Patent> SearchPatentByAuthorDateDesc(Author author)
        {
            var fullAuthor = _mapper.Map<Author>(_authorRepository.GetAuthor(_mapper.Map<AuthorDto>(author)));
            List<Patent> patents = new List<Patent>();
            if (fullAuthor != null)
                patents = _mapper.Map<List<Patent>>(_patentRepository.SearchPatentsByAuthorDateDesc(_mapper.Map<AuthorDto>(fullAuthor)));
            return patents;
        }

        public List<Patent> SearchPatentDateAsc(Patent patent)
        {
            var patents = _mapper.Map<List<Patent>>(_patentRepository.SearchPatentsDateAsc(_mapper.Map<PatentDto>(patent)));
            AddAuthors(patents);
            return patents;
        }

        public List<Patent> SearchPatentDateDesc(Patent patent)
        {
            var patents = _mapper.Map<List<Patent>>(_patentRepository.SearchPatentsDateDesc(_mapper.Map<PatentDto>(patent)));
            AddAuthors(patents);
            return patents;
        }

        public Patent GetPatent(Patent patent)
        {
            return _mapper.Map<Patent>(_patentRepository.GetPatent(_mapper.Map<PatentDto>(patent)));
        }
    }
}
