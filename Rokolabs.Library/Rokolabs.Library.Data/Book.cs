﻿ using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Rokolabs.Library.Data
{
    public class Book : IComparable
    {
        public Book()
        {
            Authors = new List<Author>();
        }

        [DisplayName("Id")]
        public int BookId { get; set; }

        [Required]
        [MaxLength(300)]
        public string Title { get; set; }
        public List<Author> Authors { get; set; }
        public int CityID { get; set; }

        [DisplayName("City of publishing")]
        public string CityName { get; set; }

        [DisplayName("Publishing office")]
        public string PublishingOfficeName { get; set; }
        public int PublishingOfficeID { get; set; }
        public string Description { get; set; }

        [DisplayName("Title")]
        public string FullName {
            get
            {
                string name = $"{AuthorsInfo} - {Title} ({YearOfPublishing})";
                return name;
            }
        }

        [DisplayName("Authors")]
        public string AuthorsInfo
        {
            get
            {
                if (authorsInfo == null)
                {
                    StringBuilder data = new StringBuilder();
                    for (int i = 0; i < Authors.Count; i++)
                    {
                        data.Append(Authors[0].FirstName[0] + ". " + Authors[i].LastName);
                        if (i != Authors.Count - 1)
                            data.Append(", ");
                    }
                    authorsInfo = data.ToString();
                }
                return authorsInfo;
            }
            set
            {
                authorsInfo = value;
            }
        }

        [DisplayName("Year")]
        [Required]
        public short YearOfPublishing
        {
            get
            {
                return yearOfPublishing;
            }
            set
            {
                if (value > (short)DateTime.Today.Year)
                    yearOfPublishing = (short)DateTime.Today.Year;
                else
                    yearOfPublishing = value;
            }
        }
        [DisplayName("Pages count")]
        [Range(1, 32767)]
        public int PagesCount
        {
            get
            {
                return pagesCount;
            }
            set
            {
                if (value <= 0)
                    pagesCount = 1;
                else
                    pagesCount = value;
            }
        }

        [MaxLength(18)]
        public string ISBN
        {
            get
            {
                return isbn;
            }
            set
            {
                if (value == String.Empty || value  == null ||  value.Length < 18 || value.Length > 18)
                    isbn = String.Empty;
                else
                {
                    if (value.Substring(0, 5) != "ISBN ")
                        isbn = "ISBN " + value.Substring(5);
                    else
                        isbn = value;
                }
            }
        }

        private string isbn;
        private short yearOfPublishing;
        private int pagesCount;
        private string authorsInfo;

        public int CompareTo(Object o)
        {
            Book book = o as Book;
            if (book != null)
                return this.YearOfPublishing.CompareTo(book.YearOfPublishing);
            else
                throw new Exception("Unable to compare two objects");
        }
    }
}
