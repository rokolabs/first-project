﻿using Rokolabs.Library.Data;
using System.Collections.Generic;

namespace Rokolabs.Library.Bll.Interfaces
{
    public interface IPatentService
    {
        List<Patent> GetAllPatents();
        List<Patent> GetAllPatentsDateAsc();
        List<Patent> GetAllPatentsDateDesc();
        int InsertPatent(Patent patent);
        List<Patent> SearchPatent(Patent patent);
        List<Patent> SearchPatentDateAsc(Patent patent);
        List<Patent> SearchPatentDateDesc(Patent patent);
        void DeletePatent(int patentId);
        List<Patent> SearchPatentByAuthor(Author author);
        List<Patent> SearchPatentByAuthorDateAsc(Author author);
        List<Patent> SearchPatentByAuthorDateDesc(Author author);
        Patent GetPatent(Patent patent);
    }
}
