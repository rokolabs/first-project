﻿namespace Rokolabs.Library.Dal.DTOs
{
    public class CityDto
    {
        public int CityID { get; set; }
        public string CityName { get; set; }
    }
}
