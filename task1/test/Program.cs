﻿using System;
using System.IO;

namespace test
{
    class Program
    {
        static void Main(string[] args)
        {
            DriveInfo drive = new DriveInfo("C");
            Console.WriteLine(drive.AvailableFreeSpace >> 30);
        }
    }
}
