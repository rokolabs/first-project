﻿using System;
using System.Collections.Generic;
using System.IO;
using JournalMaker;

namespace ExternalFilesWorker
{
    public class FileRestorer
    {
        public static void RestoreFiles(JournalRecord jr, string pathToBackup)
        {
            for (int i = 0; i < jr.Files.Count; i++)
            {
                if (jr.Files[i].Id != -1)
                {
                    if (File.Exists(jr.Files[i].Fullname))
                    {
                        File.Delete(jr.Files[i].Fullname);
                        File.Copy(Path.Combine(pathToBackup, jr.Files[i].LocalFilename), jr.Files[i].Fullname);
                    }
                    else
                        File.Copy(Path.Combine(pathToBackup, jr.Files[i].LocalFilename), jr.Files[i].Fullname);
                }
            }
        }

        public static JournalRecord FindFilesForRestore(DateTime dateTime, JournalLog jl)
        {
            for (int i = jl.Records.Count-1; i >= 0 ; i--)
            {
                if (jl.Records[i].DateTimeProperty <= dateTime)
                    return jl.Records[i];
            }
            return new JournalRecord();
        }
    }

}
