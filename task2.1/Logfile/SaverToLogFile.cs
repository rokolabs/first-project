﻿using JournalMaker;
using System.IO;

namespace LogWorker
{
    public class SaverToLogFile
    {
        public static void SaveJournal(JournalLog journal, string pathToJournal)
        {
            using (StreamWriter sw = new StreamWriter(Path.Combine(pathToJournal, journal.journalName)))
            {
                sw.WriteLine(journal.firstLine);
                sw.WriteLine(journal.finalSeparator);
            }
            AddRecordToLog(pathToJournal, journal);
        }

        public static void AddRecordToLog(string pathToJournal, JournalLog journalLog)
        {
            using (StreamWriter sw = new StreamWriter(Path.Combine(pathToJournal, journalLog.journalName), true))
            {
                string[] stringRecord = SerializeRecord(journalLog);
                for (int i = 0; i < stringRecord.Length; i++)
                {
                    sw.WriteLine(stringRecord[i]);
                }
            }
        }

        public static string[] SerializeRecord(JournalLog journalLog)
        {
            JournalRecord lastRecord = journalLog.Records[^1];
            string[] stringRecord = new string[lastRecord.Files.Count + 2];
            stringRecord[0] = lastRecord.DateTimeProperty.ToString();
            for (int i = 0; i < stringRecord.Length - 2; i++)
            {
                stringRecord[i + 1] = lastRecord.Files[i].Id.ToString() + journalLog.separator + lastRecord.Files[i].LocalFilename +
                    journalLog.separator + lastRecord.Files[i].Fullname + journalLog.separator + lastRecord.Files[i].Change;
            }
            stringRecord[^1] = journalLog.finalSeparator;
            return stringRecord;
        }
    }
}
