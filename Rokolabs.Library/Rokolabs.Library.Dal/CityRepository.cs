﻿using Dapper;
using Microsoft.Extensions.Logging;
using Rokolabs.Library.Dal.DTOs;
using Rokolabs.Library.Dal.Interfaces;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Rokolabs.Library.Dal
{
    public class CityRepository : ICityRepository
    {
        private readonly ILogger<CityRepository> _logger;
        private readonly DBOptions _options;

        public CityRepository(ILogger<CityRepository> logger, DBOptions options)
        {
            _logger = logger;
            _options = options;
        }

        public int FindCity(string cityName)
        {
            using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
                var query = "SELECT ([CityID]) FROM [Cities] WHERE [Cities].[CityName] = @cityName";
                return db.Query<int>(query, new { cityName }).FirstOrDefault();
            }
        }


        public int InsertCity(string cityName)
        {
            using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
                var query = "INSERT INTO [Cities] ([CityName]) VALUES (@cityName); SELECT CAST(SCOPE_IDENTITY() as int);";
                return db.Query<int>(query, new { cityName }).FirstOrDefault();
            }
        }
    }
}
