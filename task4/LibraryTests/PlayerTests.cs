﻿using Lib;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace LibraryTests
{
    public class PlayerTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void EatBonusTest()
        {
            List<Obstacle> obstacles = new List<Obstacle>();
            List<Bonus> bonuses = new List<Bonus>();
            List<Monster> monster = new List<Monster>();

            Map map = new Map(10, 10, obstacles, bonuses, monster);
            int previousHealth = 10;

            Player player = new Player(6,5, "player1");
            player.Health = previousHealth;
            map.player = player;

            Apple apple = new Apple(6, 6);
            Blueberry blueberry = new Blueberry(6, 7);
            Cherry cherry = new Cherry(6, 8);
            Pineapple pineapple = new Pineapple(6, 9);
            Raspberry raspberry = new Raspberry(6, 10);

            map.Bonuses.Add(apple);
            map.Bonuses.Add(blueberry);
            map.Bonuses.Add(cherry);
            map.Bonuses.Add(pineapple);
            map.Bonuses.Add(raspberry);


            for (int i = 0; i < map.Bonuses.Count; i++)
            {
                player.Y++;
                Bonus bonus = map.TryFindBonus(player.X, (player.Y));
                if (bonus != null)
                {
                    player.GetBonus(bonus);
                    Assert.IsTrue(player.Health == previousHealth + bonus.Energy || player.Health == 100);
                    previousHealth = player.Health;
                }
            }
            
        }

        [Test]
        public void MeetMonsterTest()
        {
            List<Obstacle> obstacles = new List<Obstacle>();
            List<Bonus> bonuses = new List<Bonus>();
            List<Monster> monsters = new List<Monster>();

            Map map = new Map(10, 10, obstacles, bonuses, monsters);
            int previousHealth = 100;

            Player player = new Player(6, 5, "player1");
            player.Health = previousHealth;
            map.player = player;

            Bear bear = new Bear(6, 6);
            Wolf wolf = new Wolf(6, 7);
            Fox fox = new Fox(6, 8);

            monsters.Add(bear);
            monsters.Add(wolf);
            monsters.Add(fox);

            foreach (var monster in monsters)
            {
                if (monster.X == player.X && monster.Y == player.Y)
                {
                    monster.Hit(player);
                    Assert.IsTrue(player.Health == previousHealth - monster.HitRange || player.Health == 0);
                    previousHealth = player.Health;
                }
            }
        }

        [Test]
        public void MeetPassableHitterObstacle()
        {
            List<Obstacle> obstacles = new List<Obstacle>();
            List<Bonus> bonuses = new List<Bonus>();
            List<Monster> monsters = new List<Monster>();

            Map map = new Map(10, 10, obstacles, bonuses, monsters);
            int previousHealth = 100;

            Player player = new Player(6, 5, "player1");
            player.Health = previousHealth;
            map.player = player;

            Bush bush = new Bush(6, 6);
            obstacles.Add(bush);

            foreach (var monster in monsters)
            {
                if (monster.X == player.X && monster.Y == player.Y)
                {
                    monster.Hit(player);
                    Assert.IsTrue(player.Health == previousHealth - monster.HitRange || player.Health == 0);
                    previousHealth = player.Health;
                }
            }
        }

        [Test]
        public void MeetPassableObstacleTest()
        {
            List<Obstacle> obstacles = new List<Obstacle>();
            List<Bonus> bonuses = new List<Bonus>();
            List<Monster> monsters = new List<Monster>();

            Map map = new Map(30, 30, obstacles, bonuses, monsters);
            Player player = new Player(11, 10, "player1");
            map.player = player;

            Bush bush = new Bush(11, 10);
            map.Obstacles.Add(bush);

            Assert.IsTrue(map.IsThereFreePlace(player.X, player.Y, false));
        }


    }
}
