USE [master]
GO
/****** Object:  Database [LibraryDB]    Script Date: 10/25/2021 2:31:25 AM ******/
CREATE DATABASE [LibraryDB]
 CONTAINMENT = NONE
ALTER DATABASE [LibraryDB] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [LibraryDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [LibraryDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [LibraryDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [LibraryDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [LibraryDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [LibraryDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [LibraryDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [LibraryDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [LibraryDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [LibraryDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [LibraryDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [LibraryDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [LibraryDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [LibraryDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [LibraryDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [LibraryDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [LibraryDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [LibraryDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [LibraryDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [LibraryDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [LibraryDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [LibraryDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [LibraryDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [LibraryDB] SET RECOVERY FULL 
GO
ALTER DATABASE [LibraryDB] SET  MULTI_USER 
GO
ALTER DATABASE [LibraryDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [LibraryDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [LibraryDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [LibraryDB] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [LibraryDB] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [LibraryDB] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'LibraryDB', N'ON'
GO
ALTER DATABASE [LibraryDB] SET QUERY_STORE = OFF
GO
USE [LibraryDB]
GO
/****** Object:  UserDefinedTableType [dbo].[dtIntEntity]    Script Date: 10/25/2021 2:31:25 AM ******/
CREATE TYPE [dbo].[dtIntEntity] AS TABLE(
	[EntityID] [int] NOT NULL,
	PRIMARY KEY CLUSTERED 
(
	[EntityID] ASC
)WITH (IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[AuthorBooks]    Script Date: 10/25/2021 2:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AuthorBooks](
	[AuthorID] [int] NOT NULL,
	[BookID] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Authors]    Script Date: 10/25/2021 2:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Authors](
	[AuthorID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](200) NOT NULL,
	[Description] [nvarchar](500) NULL,
 CONSTRAINT [PK_Authors] PRIMARY KEY CLUSTERED 
(
	[AuthorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Books]    Script Date: 10/25/2021 2:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Books](
	[BookID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](300) NOT NULL,
	[CityID] [int] NULL,
	[PublishingOfficeID] [int] NULL,
	[YearOfPublishing] [smallint] NOT NULL,
	[PagesCount] [smallint] NULL,
	[Description] [nvarchar](2000) NULL,
	[ISBN] [nchar](18) NULL,
 CONSTRAINT [PK_Books] PRIMARY KEY CLUSTERED 
(
	[BookID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cities]    Script Date: 10/25/2021 2:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cities](
	[CityID] [int] IDENTITY(1,1) NOT NULL,
	[CityName] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_Table_1] PRIMARY KEY CLUSTERED 
(
	[CityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Countries]    Script Date: 10/25/2021 2:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Countries](
	[CountryID] [int] IDENTITY(1,1) NOT NULL,
	[CountryName] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_Countries] PRIMARY KEY CLUSTERED 
(
	[CountryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[InventorPatents]    Script Date: 10/25/2021 2:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InventorPatents](
	[AuthorID] [int] NOT NULL,
	[PatentID] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Newspapers]    Script Date: 10/25/2021 2:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Newspapers](
	[PaperID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](300) NOT NULL,
	[CityID] [int] NULL,
	[YearOfPublishing] [smallint] NOT NULL,
	[PublishingOfficeID] [int] NOT NULL,
	[PagesCount] [smallint] NULL,
	[Description] [nvarchar](2000) NULL,
	[PaperNumber] [int] NULL,
	[Date] [date] NOT NULL,
	[ISSN] [nchar](14) NULL,
 CONSTRAINT [PK_Newspapers] PRIMARY KEY CLUSTERED 
(
	[PaperID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Patents]    Script Date: 10/25/2021 2:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Patents](
	[PatentID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](300) NOT NULL,
	[PatentRegistrationNumber] [int] NOT NULL,
	[CountryID] [int] NOT NULL,
	[ApplicationDate] [date] NULL,
	[PublicationDate] [date] NULL,
	[PagesCount] [smallint] NULL,
	[Description] [nvarchar](2000) NULL,
 CONSTRAINT [PK_Patents] PRIMARY KEY CLUSTERED 
(
	[PatentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PublishingOffices]    Script Date: 10/25/2021 2:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PublishingOffices](
	[PublishingOfficeID] [int] IDENTITY(1,1) NOT NULL,
	[PublishingOfficeName] [nvarchar](300) NOT NULL,
 CONSTRAINT [PK_PublishingOffices] PRIMARY KEY CLUSTERED 
(
	[PublishingOfficeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[AuthorBooks] ([AuthorID], [BookID]) VALUES (2004, 3002)
INSERT [dbo].[AuthorBooks] ([AuthorID], [BookID]) VALUES (3, 3003)
INSERT [dbo].[AuthorBooks] ([AuthorID], [BookID]) VALUES (4, 6002)
INSERT [dbo].[AuthorBooks] ([AuthorID], [BookID]) VALUES (5, 6004)
INSERT [dbo].[AuthorBooks] ([AuthorID], [BookID]) VALUES (7, 6006)
INSERT [dbo].[AuthorBooks] ([AuthorID], [BookID]) VALUES (6, 6007)
INSERT [dbo].[AuthorBooks] ([AuthorID], [BookID]) VALUES (8, 6008)
INSERT [dbo].[AuthorBooks] ([AuthorID], [BookID]) VALUES (9, 6009)
INSERT [dbo].[AuthorBooks] ([AuthorID], [BookID]) VALUES (10, 6011)
INSERT [dbo].[AuthorBooks] ([AuthorID], [BookID]) VALUES (5015, 6012)
INSERT [dbo].[AuthorBooks] ([AuthorID], [BookID]) VALUES (5017, 6014)
INSERT [dbo].[AuthorBooks] ([AuthorID], [BookID]) VALUES (5018, 6015)
INSERT [dbo].[AuthorBooks] ([AuthorID], [BookID]) VALUES (5019, 6015)
INSERT [dbo].[AuthorBooks] ([AuthorID], [BookID]) VALUES (5020, 6016)
INSERT [dbo].[AuthorBooks] ([AuthorID], [BookID]) VALUES (5021, 6016)
GO
SET IDENTITY_INSERT [dbo].[Authors] ON 

INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName], [Description]) VALUES (3, N'Sarah', N'Penner', NULL)
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName], [Description]) VALUES (4, N'Ashley', N'Audrain', NULL)
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName], [Description]) VALUES (5, N'Kazuo', N'Ishiguro', NULL)
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName], [Description]) VALUES (6, N'Taylor', N'Reid', N'Taylor Jenkins Reid')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName], [Description]) VALUES (7, N'Janet', N'Charles', N'Janet Skeslien Charles')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName], [Description]) VALUES (8, N'Kate', N'Quinn', NULL)
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName], [Description]) VALUES (9, N'Emily', N'Henry', NULL)
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName], [Description]) VALUES (10, N'Angie', N'Thomas', NULL)
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName], [Description]) VALUES (2004, N'Kristin', N'Hannah', NULL)
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName], [Description]) VALUES (4002, N'Anastasia', N'Greenberg', NULL)
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName], [Description]) VALUES (4003, N'Alexis', N'Cohen', NULL)
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName], [Description]) VALUES (4004, N'Monica', N'Grewal', NULL)
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName], [Description]) VALUES (4005, N'Martin', N'Wells', NULL)
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName], [Description]) VALUES (4006, N'Mike', N'McCracken', NULL)
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName], [Description]) VALUES (5015, N'Casey', N'McQuiston', NULL)
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName], [Description]) VALUES (5017, N'Anna', N'North', NULL)
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName], [Description]) VALUES (5018, N'Ibram', N'Kendi', N'Ibram X. Kendi')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName], [Description]) VALUES (5019, N'Keisha', N'Blain', N'Keisha N. Blain')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName], [Description]) VALUES (5020, N'Amber', N'Ruffin', NULL)
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName], [Description]) VALUES (5021, N'Lacey', N'Lamar', NULL)
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName], [Description]) VALUES (9007, N'Test', N'Author', N'')
SET IDENTITY_INSERT [dbo].[Authors] OFF
GO
SET IDENTITY_INSERT [dbo].[Books] ON 

INSERT [dbo].[Books] ([BookID], [Title], [CityID], [PublishingOfficeID], [YearOfPublishing], [PagesCount], [Description], [ISBN]) VALUES (3002, N'The Four Winds', 1, 1, 2021, 465, N'‘Powerful and compelling’ - Delia Owens, number one international bestselling author of Where The Crawdads', N'ISBN 978-5-86471-5')
INSERT [dbo].[Books] ([BookID], [Title], [CityID], [PublishingOfficeID], [YearOfPublishing], [PagesCount], [Description], [ISBN]) VALUES (3003, N'The Lost Apothecary', 1, 2, 2021, 320, N'Sarah Penner''s debut novel, The Lost Apothecary, is an enthralling work of mystery, murder, trust, and betrayal. Set in an atmospheric London, Penner''s immersive story flows skillfully from past to present, revealing the heartaches and lost dreams of three captivating main characters in a page-turningly tense drama that surprises right up until the final paragraph.', N'ISBN 978-04-1594-4')
INSERT [dbo].[Books] ([BookID], [Title], [CityID], [PublishingOfficeID], [YearOfPublishing], [PagesCount], [Description], [ISBN]) VALUES (6002, N'The Push', 1, 1003, 2021, 320, N'"The Push" is a thriller that demands to be read in a single sitting. Blythe was determined to be the mother she never had — but struggles when her daughter starts to behave differently, possessing a vaguely sinister quality that no one else notices except Blythe. When Blythe''s son is born, she has the blissful motherly connection for which she always hoped, until the life she imagined changes in an instant.', N'ISBN 978-14-0592-4')
INSERT [dbo].[Books] ([BookID], [Title], [CityID], [PublishingOfficeID], [YearOfPublishing], [PagesCount], [Description], [ISBN]) VALUES (6004, N'Klara and the Sun', 1, 1004, 2021, 320, N'In 2017, Kazuo Ishiguro won the Nobel Prize in Literature — this is his first novel since the award. Set in the near future, "Klara and the Sun" explores the human condition through Klara, an Artificial Friend. Klara is AI, keenly observational and eerily understanding the depth of human emotion as she watches out the store window and waits for a customer to one day choose her. This book is sweet, gripping, and subtly beautiful, exploring connection, loss, and love in this speculative science fiction read.', N'ISBN 978-05-9334-9')
INSERT [dbo].[Books] ([BookID], [Title], [CityID], [PublishingOfficeID], [YearOfPublishing], [PagesCount], [Description], [ISBN]) VALUES (6006, N'The Paris Library', 1, 1005, 2021, 368, N'This historical fiction novel is based on the little-known but true story of World War II librarians at the American Library in Paris. It begins in 1939, where young librarian Odile faces the fear of losing her library as the Nazis invade her city. In 1983, Lily is a teenager in Montana whose school project leads her to interview her French neighbor, uncovering her mysterious past and the secret that may connect them. This is not a war novel, but a descriptive and deeply intriguing piece of historical fiction that will pull on the heartstrings of all book lovers.', N'ISBN 978-1-43287-9')
INSERT [dbo].[Books] ([BookID], [Title], [CityID], [PublishingOfficeID], [YearOfPublishing], [PagesCount], [Description], [ISBN]) VALUES (6007, N'Malibu Rising', 1, 1006, 2021, 384, N'Taylor Jenkins Reid novels are known for being absolute page-turners, and "Malibu Rising" is no different. This book bounces between an epic, life-changing party over 24 hours and the family history of four famous siblings. Together, they''re a fascination to the world, children of the legendary rockstar Mick Riva. They''re all looking forward to their annual party for different reasons except Nina, recently abandoned by her husband and resentful of the spotlight. By morning, the house will be up in flames, but before that the party will become completely out of control and the secrets of the family will rise to the surface.', N'ISBN 978-03-8569-2')
INSERT [dbo].[Books] ([BookID], [Title], [CityID], [PublishingOfficeID], [YearOfPublishing], [PagesCount], [Description], [ISBN]) VALUES (6008, N'The Rose Code', 1, 1007, 2021, 645, N'In 1940, three very different women come together during the war to help break German military codes, creating deep bonds that are broken by the pressure of secrecy and the pain of loss through the war. Seven years later, the women are reunited at a royal wedding by a mysterious letter and must revisit a past of betrayal and heartbreak in order to crack one final code and stop an elusive enemy. This is a brilliant and riveting read, a bestseller with perfectly plotted narratives that has quickly become an undeniable 2021 favorite.', N'ISBN 978-008-455-8')
INSERT [dbo].[Books] ([BookID], [Title], [CityID], [PublishingOfficeID], [YearOfPublishing], [PagesCount], [Description], [ISBN]) VALUES (6009, N'People We Meet on Vacation', 1, 1008, 2021, 384, N'From the author of the 2020 hit "Beach Read" comes another summer favorite of two unlikely friends that vacation together every summer. Alex and Poppy couldn''t be more opposite: Alex, a quiet boy with hometown charm, and Poppy, a wanderlust-fueled wild child. After sharing a ride home in college, the two form a friendship, sharing a vacation together every summer for a decade, until two years ago when they ruined everything. Now, Poppy and Alex come together for one more trip to see if they can mend their friendship or if there''s really something more between them.', N'ISBN 978-1-98480-6')
INSERT [dbo].[Books] ([BookID], [Title], [CityID], [PublishingOfficeID], [YearOfPublishing], [PagesCount], [Description], [ISBN]) VALUES (6011, N'Concrete Rose', 1002, 1009, 2021, 336, N'"Concrete Rose" is the prequel to the super popular YA novel "The Hate U Give". In this book, readers meet Maverick Carter: a 17-year-old high school student who deals with the gang his father once ruled. When Maverick learns he''s a father, he decides to "go straight" by no longer dealing drugs, working a part-time job, finishing high school, and being there for his son. Torn between loyalty and responsibility, this book is Maverick''s coming-of-age journey as he decides what it means to him to be a father and a man.', N'ISBN 978-35-7016-6')
INSERT [dbo].[Books] ([BookID], [Title], [CityID], [PublishingOfficeID], [YearOfPublishing], [PagesCount], [Description], [ISBN]) VALUES (6012, N'One Last Stop', 1, 1, 2021, 432, N'August believes the best way to move through life is alone. That is, until she meets Jane on the subway who offers her a simple solution to her bad day. August can''t stop thinking about Jane and luckily sees her every day — strangely on the exact same train, in the exact same car. Jane is from the 1970s, caught in a magical timeslip with little memory of her past. Determined to help, August sets out to rescue Jane from the subway to which she appears to be tethered. This book is a magical young adult queer romance featuring diverse characters, tons of romantic scenes, and a charming plotline that keeps readers yearning for more.', N'ISBN 978-5-25024-4')
INSERT [dbo].[Books] ([BookID], [Title], [CityID], [PublishingOfficeID], [YearOfPublishing], [PagesCount], [Description], [ISBN]) VALUES (6014, N'Outlawed', 1002, 1010, 2021, 272, N'This book is a fast-paced and dangerous story that''s piqued readers'' interest by breaking the mold of traditional Westerns with a queer, feminist Western girl gang. In 1984, Ana is a respected midwife who hasn''t been able to get pregnant after a year of marriage. With the fear of being hanged as a witch, Ana joins the Hole in the Wall Gang, a group determined to create a safe haven for outlawed women. When the gang devises a risky plan, Ana must decide if she''s willing to risk her life for the chance of a new future for them all.', N'ISBN 978-1-63557-5')
INSERT [dbo].[Books] ([BookID], [Title], [CityID], [PublishingOfficeID], [YearOfPublishing], [PagesCount], [Description], [ISBN]) VALUES (6015, N'Four Hundred Souls: A Community History of African America, 1619-2019', 1002, 1011, 2021, 528, N'This is a chronological account of 400 years of previously silenced Black history in America. Curated by two historians, this book begins with the arrival of 20 enslaved Ndongo people in 1619 and continues to tell stories of slavery, segregation, and oppression over 80 chapters. There are also celebrations of African art and music, a life-changing collection that concludes with an essay from Alicia Garza on the Black Lives Matter movement.', N'ISBN 978-05-9313-4')
INSERT [dbo].[Books] ([BookID], [Title], [CityID], [PublishingOfficeID], [YearOfPublishing], [PagesCount], [Description], [ISBN]) VALUES (6016, N'You''ll Never Believe What Happened to Lacey: Crazy Stories About Racism', 1, 1012, 2021, 240, N'Amber Ruffin and Lacey Lamar are sisters who collaborated to create a compilation of what seem like absurdly unreal stories of racism, yet are all true and sometimes regular experiences for Black people. Told with hilarious sibling banter, the sisters swap stories of people mistaking them for Harriet Tubman, putting their whole hand in their hair, and their interaction with a racist donut store owner. Amber and Lacey shed light on these ridiculous moments of racism with which Black people can commiserate and others can learn from.', N'ISBN 978-1-53871-9')
SET IDENTITY_INSERT [dbo].[Books] OFF
GO
SET IDENTITY_INSERT [dbo].[Cities] ON 

INSERT [dbo].[Cities] ([CityID], [CityName]) VALUES (1, N'New York')
INSERT [dbo].[Cities] ([CityID], [CityName]) VALUES (5, N'Atlanta')
INSERT [dbo].[Cities] ([CityID], [CityName]) VALUES (1002, N'London')
INSERT [dbo].[Cities] ([CityID], [CityName]) VALUES (2002, N'Saratov')
INSERT [dbo].[Cities] ([CityID], [CityName]) VALUES (4002, N'Saratoc')
SET IDENTITY_INSERT [dbo].[Cities] OFF
GO
SET IDENTITY_INSERT [dbo].[Countries] ON 

INSERT [dbo].[Countries] ([CountryID], [CountryName]) VALUES (1, N'USA')
INSERT [dbo].[Countries] ([CountryID], [CountryName]) VALUES (2, N'Russia')
SET IDENTITY_INSERT [dbo].[Countries] OFF
GO
INSERT [dbo].[InventorPatents] ([AuthorID], [PatentID]) VALUES (4002, 3)
INSERT [dbo].[InventorPatents] ([AuthorID], [PatentID]) VALUES (4003, 3)
INSERT [dbo].[InventorPatents] ([AuthorID], [PatentID]) VALUES (4004, 3)
INSERT [dbo].[InventorPatents] ([AuthorID], [PatentID]) VALUES (4005, 1002)
INSERT [dbo].[InventorPatents] ([AuthorID], [PatentID]) VALUES (4006, 1002)
GO
SET IDENTITY_INSERT [dbo].[Newspapers] ON 

INSERT [dbo].[Newspapers] ([PaperID], [Title], [CityID], [YearOfPublishing], [PublishingOfficeID], [PagesCount], [Description], [PaperNumber], [Date], [ISSN]) VALUES (2, N'Ex-intel official who created controversial Trump Russia dossier speaks out', 5, 2021, 1002, 2, N'(CNN)Former British intelligence officer Christopher Steele, the man behind the "Steele Dossier" that claimed Russian officials held compromising information on former President Donald Trump, defended the claims made in the dossier in his first on-camera interview since it was revealed in 2017.', 10, CAST(N'2021-10-17' AS Date), N'ISSN6369-1735 ')
INSERT [dbo].[Newspapers] ([PaperID], [Title], [CityID], [YearOfPublishing], [PublishingOfficeID], [PagesCount], [Description], [PaperNumber], [Date], [ISSN]) VALUES (6, N'China''s economic growth slows to 4.9% in the third quarter', 5, 2021, 1002, 2, N'Hong Kong (CNN Business)China''s economy grew just 4.9% in the third quarter of 2021, the weakest rate of expansion in a year as the country contends with a major energy crunch, supply chain disruptions, and deepening debt woes in its outsized property sector.', 11, CAST(N'2021-10-17' AS Date), N'ISSN6369-1736 ')
INSERT [dbo].[Newspapers] ([PaperID], [Title], [CityID], [YearOfPublishing], [PublishingOfficeID], [PagesCount], [Description], [PaperNumber], [Date], [ISSN]) VALUES (16, N'Protest in Athens as Beijing prepares for Olympic Games flame', 5, 2021, 1002, 2, N'Athens, Greece (CNN)Greek police detained two human rights activists after they unfurled banners at the Athens Acropolis on Sunday opposing the Beijing 2022 Winter Olympics as China''s Games organizers prepare to receive the Olympic flame.', 12, CAST(N'2021-10-17' AS Date), N'ISSN6369-1737 ')
SET IDENTITY_INSERT [dbo].[Newspapers] OFF
GO
SET IDENTITY_INSERT [dbo].[Patents] ON 

INSERT [dbo].[Patents] ([PatentID], [Title], [PatentRegistrationNumber], [CountryID], [ApplicationDate], [PublicationDate], [PagesCount], [Description]) VALUES (3, N'Patent landscape of brain–machine interface technology', 1001, 1, CAST(N'2021-09-07' AS Date), CAST(N'2021-10-07' AS Date), 39, N'A study of the brain–machine interface patent landscape suggests that the technology is in its early stages of development, but patent applications have been increasing exponentially in recent years.')
INSERT [dbo].[Patents] ([PatentID], [Title], [PatentRegistrationNumber], [CountryID], [ApplicationDate], [PublicationDate], [PagesCount], [Description]) VALUES (1002, N'Organoids', 1002, 1, CAST(N'2021-06-06' AS Date), CAST(N'2021-07-06' AS Date), 45, N'Methods of inducing formation of gastric cells and/or gastric tissues, such as in the form of a gastric organoid. Also, methods for using the disclosed gastric cells, gastric tissues and/or gastric organoids derived from precursor cells.')
SET IDENTITY_INSERT [dbo].[Patents] OFF
GO
SET IDENTITY_INSERT [dbo].[PublishingOffices] ON 

INSERT [dbo].[PublishingOffices] ([PublishingOfficeID], [PublishingOfficeName]) VALUES (1, N'Macmillan')
INSERT [dbo].[PublishingOffices] ([PublishingOfficeID], [PublishingOfficeName]) VALUES (2, N'Park Row')
INSERT [dbo].[PublishingOffices] ([PublishingOfficeID], [PublishingOfficeName]) VALUES (1002, N'CNN')
INSERT [dbo].[PublishingOffices] ([PublishingOfficeID], [PublishingOfficeName]) VALUES (1003, N'Pamela Dorman Books')
INSERT [dbo].[PublishingOffices] ([PublishingOfficeID], [PublishingOfficeName]) VALUES (1004, N'Knopf Publishing Group')
INSERT [dbo].[PublishingOffices] ([PublishingOfficeID], [PublishingOfficeName]) VALUES (1005, N'Atria Books')
INSERT [dbo].[PublishingOffices] ([PublishingOfficeID], [PublishingOfficeName]) VALUES (1006, N'Ballantine Books')
INSERT [dbo].[PublishingOffices] ([PublishingOfficeID], [PublishingOfficeName]) VALUES (1007, N'HarperCollins')
INSERT [dbo].[PublishingOffices] ([PublishingOfficeID], [PublishingOfficeName]) VALUES (1008, N'Berkley Books')
INSERT [dbo].[PublishingOffices] ([PublishingOfficeID], [PublishingOfficeName]) VALUES (1009, N'Walker Books')
INSERT [dbo].[PublishingOffices] ([PublishingOfficeID], [PublishingOfficeName]) VALUES (1010, N'Bloomsbury Publishing')
INSERT [dbo].[PublishingOffices] ([PublishingOfficeID], [PublishingOfficeName]) VALUES (1011, N'One World')
INSERT [dbo].[PublishingOffices] ([PublishingOfficeID], [PublishingOfficeName]) VALUES (1012, N'Grand Central Publishing')
SET IDENTITY_INSERT [dbo].[PublishingOffices] OFF
GO
ALTER TABLE [dbo].[AuthorBooks]  WITH CHECK ADD  CONSTRAINT [FK_AuthorBooks_Authors] FOREIGN KEY([AuthorID])
REFERENCES [dbo].[Authors] ([AuthorID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AuthorBooks] CHECK CONSTRAINT [FK_AuthorBooks_Authors]
GO
ALTER TABLE [dbo].[AuthorBooks]  WITH CHECK ADD  CONSTRAINT [FK_AuthorBooks_Books] FOREIGN KEY([BookID])
REFERENCES [dbo].[Books] ([BookID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AuthorBooks] CHECK CONSTRAINT [FK_AuthorBooks_Books]
GO
ALTER TABLE [dbo].[Books]  WITH CHECK ADD  CONSTRAINT [FK_Books_Cities] FOREIGN KEY([CityID])
REFERENCES [dbo].[Cities] ([CityID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Books] CHECK CONSTRAINT [FK_Books_Cities]
GO
ALTER TABLE [dbo].[Books]  WITH CHECK ADD  CONSTRAINT [FK_Books_PublishingOffices] FOREIGN KEY([PublishingOfficeID])
REFERENCES [dbo].[PublishingOffices] ([PublishingOfficeID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Books] CHECK CONSTRAINT [FK_Books_PublishingOffices]
GO
ALTER TABLE [dbo].[InventorPatents]  WITH CHECK ADD  CONSTRAINT [FK_InventorPatents_Authors] FOREIGN KEY([AuthorID])
REFERENCES [dbo].[Authors] ([AuthorID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[InventorPatents] CHECK CONSTRAINT [FK_InventorPatents_Authors]
GO
ALTER TABLE [dbo].[InventorPatents]  WITH CHECK ADD  CONSTRAINT [FK_InventorPatents_Patents] FOREIGN KEY([PatentID])
REFERENCES [dbo].[Patents] ([PatentID])
GO
ALTER TABLE [dbo].[InventorPatents] CHECK CONSTRAINT [FK_InventorPatents_Patents]
GO
ALTER TABLE [dbo].[Newspapers]  WITH CHECK ADD  CONSTRAINT [FK_Newspapers_Cities] FOREIGN KEY([CityID])
REFERENCES [dbo].[Cities] ([CityID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Newspapers] CHECK CONSTRAINT [FK_Newspapers_Cities]
GO
ALTER TABLE [dbo].[Newspapers]  WITH CHECK ADD  CONSTRAINT [FK_Newspapers_PublishingOffices] FOREIGN KEY([PublishingOfficeID])
REFERENCES [dbo].[PublishingOffices] ([PublishingOfficeID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Newspapers] CHECK CONSTRAINT [FK_Newspapers_PublishingOffices]
GO
ALTER TABLE [dbo].[Patents]  WITH CHECK ADD  CONSTRAINT [FK_Patents_Countries] FOREIGN KEY([CountryID])
REFERENCES [dbo].[Countries] ([CountryID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Patents] CHECK CONSTRAINT [FK_Patents_Countries]
GO
/****** Object:  StoredProcedure [dbo].[GetAllBooks]    Script Date: 10/25/2021 2:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetAllBooks]
AS
BEGIN

	SELECT [Books].[BookID], [Books].[Title], [Cities].[CityName], [PublishingOffices].[PublishingOfficeName], [Books].[PagesCount], [Books].[YearOfPublishing], [Books].[Description], [Books].[ISBN]
	FROM[dbo].[Books]
	LEFT JOIN[dbo].[Cities] ON[dbo].[Books].[CityID] = [dbo].[Cities].[CityID]
	LEFT JOIN[dbo].[PublishingOffices] ON[dbo].[Books].[PublishingOfficeID] = [dbo].[PublishingOffices].[PublishingOfficeID]

END
GO
/****** Object:  StoredProcedure [dbo].[GetAllBooksDateASC]    Script Date: 10/25/2021 2:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetAllBooksDateASC]
AS
BEGIN

	SELECT [Books].[BookID], [Books].[Title], [Cities].[CityName], [PublishingOffices].[PublishingOfficeName], [Books].[PagesCount], [Books].[YearOfPublishing], [Books].[Description], [Books].[ISBN]
	FROM[dbo].[Books]
	LEFT JOIN[dbo].[Cities] ON[dbo].[Books].[CityID] = [dbo].[Cities].[CityID]
	LEFT JOIN[dbo].[PublishingOffices] ON[dbo].[Books].[PublishingOfficeID] = [dbo].[PublishingOffices].[PublishingOfficeID]
	ORDER BY [Books].[YearOfPublishing] ASC

END
GO
/****** Object:  StoredProcedure [dbo].[GetAllBooksDateDESC]    Script Date: 10/25/2021 2:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetAllBooksDateDESC]
AS
BEGIN

	SELECT [Books].[BookID], [Books].[Title], [Cities].[CityName], [PublishingOffices].[PublishingOfficeName], [Books].[PagesCount], [Books].[YearOfPublishing], [Books].[Description], [Books].[ISBN]
	FROM[dbo].[Books]
	LEFT JOIN[dbo].[Cities] ON[dbo].[Books].[CityID] = [dbo].[Cities].[CityID]
	LEFT JOIN[dbo].[PublishingOffices] ON[dbo].[Books].[PublishingOfficeID] = [dbo].[PublishingOffices].[PublishingOfficeID]
	ORDER BY [Books].[YearOfPublishing] DESC

END
GO
/****** Object:  StoredProcedure [dbo].[GetAllNewspapers]    Script Date: 10/25/2021 2:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetAllNewspapers]
AS
BEGIN

SELECT [Newspapers].[PaperID], [Newspapers].[Title], [Newspapers].[YearOfPublishing], [Newspapers].[PagesCount], [Newspapers].[Description], [Newspapers].[PaperNumber], [Newspapers].[Date], [Newspapers].[ISSN], [Cities].[CityName], [PublishingOffices].[PublishingOfficeName]
FROM[dbo].[Newspapers]
LEFT JOIN [dbo].[Cities] ON [dbo].[Newspapers].[CityID] = [dbo].[Cities].[CityID]
LEFT JOIN [dbo].[PublishingOffices] ON [dbo].[Newspapers].[PublishingOfficeID] = [dbo].[PublishingOffices].[PublishingOfficeID]

END
GO
/****** Object:  StoredProcedure [dbo].[GetAllNewspapersDateASC]    Script Date: 10/25/2021 2:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetAllNewspapersDateASC]
AS
BEGIN

	SELECT [Newspapers].[PaperID], [Newspapers].[Title], [Newspapers].[YearOfPublishing], [Newspapers].[PagesCount], [Newspapers].[Description], [Newspapers].[PaperNumber], [Newspapers].[Date], [Newspapers].[ISSN], [Cities].[CityName], [PublishingOffices].[PublishingOfficeName]
	FROM[dbo].[Newspapers]
	LEFT JOIN [dbo].[Cities] ON [dbo].[Newspapers].[CityID] = [dbo].[Cities].[CityID]
	LEFT JOIN [dbo].[PublishingOffices] ON [dbo].[Newspapers].[PublishingOfficeID] = [dbo].[PublishingOffices].[PublishingOfficeID]
	ORDER BY [Newspapers].[YearOfPublishing] ASC

END
GO
/****** Object:  StoredProcedure [dbo].[GetAllNewspapersDateDESC]    Script Date: 10/25/2021 2:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetAllNewspapersDateDESC]
AS
BEGIN

	SELECT [Newspapers].[PaperID], [Newspapers].[Title], [Newspapers].[YearOfPublishing], [Newspapers].[PagesCount], [Newspapers].[Description], [Newspapers].[PaperNumber], [Newspapers].[Date], [Newspapers].[ISSN], [Cities].[CityName], [PublishingOffices].[PublishingOfficeName]
	FROM[dbo].[Newspapers]
	LEFT JOIN [dbo].[Cities] ON [dbo].[Newspapers].[CityID] = [dbo].[Cities].[CityID]
	LEFT JOIN [dbo].[PublishingOffices] ON [dbo].[Newspapers].[PublishingOfficeID] = [dbo].[PublishingOffices].[PublishingOfficeID]
	ORDER BY [Newspapers].[YearOfPublishing] DESC

END
GO
/****** Object:  StoredProcedure [dbo].[GetAllPatents]    Script Date: 10/25/2021 2:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetAllPatents]
AS
BEGIN

	SELECT [Patents].[PatentID], [Patents].[Title], [Patents].[PatentRegistrationNumber], [Countries].[CountryName], [Patents].[ApplicationDate], [Patents].[PublicationDate], [Patents].[PagesCount], [Patents].[Description]  
	FROM[dbo].[Patents]  
	LEFT JOIN[dbo].[Countries] ON[dbo].[Patents].[CountryID] = [dbo].[Countries].[CountryID] 

END
GO
/****** Object:  StoredProcedure [dbo].[GetAllPatentsDateASC]    Script Date: 10/25/2021 2:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetAllPatentsDateASC]
AS
BEGIN

	SELECT [Patents].[PatentID], [Patents].[Title], [Patents].[PatentRegistrationNumber], [Countries].[CountryName], [Patents].[ApplicationDate], [Patents].[PagesCount], [Patents].[Description]  
	FROM[dbo].[Patents]  
	LEFT JOIN[dbo].[Countries] ON[dbo].[Patents].[CountryID] = [dbo].[Countries].[CountryID] 
	ORDER BY [Patents].[ApplicationDate] ASC

END
GO
/****** Object:  StoredProcedure [dbo].[GetAllPatentsDateDESC]    Script Date: 10/25/2021 2:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetAllPatentsDateDESC]
AS
BEGIN

	SELECT [Patents].[PatentID], [Patents].[Title], [Patents].[PatentRegistrationNumber], [Countries].[CountryName], [Patents].[ApplicationDate], [Patents].[PagesCount], [Patents].[Description]  
	FROM[dbo].[Patents]  
	LEFT JOIN[dbo].[Countries] ON[dbo].[Patents].[CountryID] = [dbo].[Countries].[CountryID] 
	ORDER BY [Patents].[ApplicationDate] DESC

END
GO
/****** Object:  StoredProcedure [dbo].[GetAuthorsByBookIDs]    Script Date: 10/25/2021 2:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetAuthorsByBookIDs]
	@ids [dtIntEntity] READONLY
AS
BEGIN
	SELECT a.[AuthorID], [FIrstName], [LastName], ab.BookID FROM [Authors] a
	LEFT JOIN [AuthorBooks] ab on ab.AuthorID = a.AuthorID
	WHERE ab.BookID in (SELECT [EntityID] from @ids)
END

GO
/****** Object:  StoredProcedure [dbo].[GetAuthorsByPatentIDs]    Script Date: 10/25/2021 2:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetAuthorsByPatentIDs]
	@ids [dtIntEntity] READONLY
AS
BEGIN
	SELECT a.[AuthorID], a.[FIrstName], a.[LastName], a.[Description], ap.[PatentID] FROM [Authors] a
	LEFT JOIN [InventorPatents] ap on ap.AuthorID = a.AuthorID
	WHERE ap.PatentID in (SELECT [EntityID] from @ids)
END

GO
/****** Object:  StoredProcedure [dbo].[GetBooksByAuthor]    Script Date: 10/25/2021 2:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetBooksByAuthor]
	@authorid int
AS
BEGIN

SELECT [Books].[BookID], [Books].[Title], [Cities].[CityName], [PublishingOffices].[PublishingOfficeName], [Books].[PagesCount], [Books].[YearOfPublishing], [Books].[Description], [Books].[ISBN]
FROM[dbo].[Books] 
LEFT JOIN[dbo].[Cities] ON[dbo].[Books].[CityID] = [dbo].[Cities].[CityID]
LEFT JOIN[dbo].[PublishingOffices] ON[dbo].[Books].[PublishingOfficeID] = [dbo].[PublishingOffices].[PublishingOfficeID]
JOIN [AuthorBooks] ON [Books].[BookID] = [AuthorBooks].[BookID]
WHERE [AuthorBooks].[AuthorId] = @authorid

END
GO
/****** Object:  StoredProcedure [dbo].[GetBooksByAuthorDateASC]    Script Date: 10/25/2021 2:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetBooksByAuthorDateASC]
	@authorid int
AS
BEGIN

SELECT [Books].[BookID], [Books].[Title], [Cities].[CityName], [PublishingOffices].[PublishingOfficeName], [Books].[PagesCount], [Books].[YearOfPublishing], [Books].[Description], [Books].[ISBN]
FROM[dbo].[Books] 
LEFT JOIN[dbo].[Cities] ON[dbo].[Books].[CityID] = [dbo].[Cities].[CityID]
LEFT JOIN[dbo].[PublishingOffices] ON[dbo].[Books].[PublishingOfficeID] = [dbo].[PublishingOffices].[PublishingOfficeID]
JOIN [AuthorBooks] ON [Books].[BookID] = [AuthorBooks].[BookID]
WHERE [AuthorBooks].[AuthorId] = @authorid
ORDER BY [Books].[YearOfPublishing] ASC

END
GO
/****** Object:  StoredProcedure [dbo].[GetBooksByAuthorDateDESC]    Script Date: 10/25/2021 2:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetBooksByAuthorDateDESC]
	@authorid int
AS
BEGIN

SELECT [Books].[BookID], [Books].[Title], [Cities].[CityName], [PublishingOffices].[PublishingOfficeName], [Books].[PagesCount], [Books].[YearOfPublishing], [Books].[Description], [Books].[ISBN]
FROM[dbo].[Books] 
LEFT JOIN[dbo].[Cities] ON[dbo].[Books].[CityID] = [dbo].[Cities].[CityID]
LEFT JOIN[dbo].[PublishingOffices] ON[dbo].[Books].[PublishingOfficeID] = [dbo].[PublishingOffices].[PublishingOfficeID]
JOIN [AuthorBooks] ON [Books].[BookID] = [AuthorBooks].[BookID]
WHERE [AuthorBooks].[AuthorId] = @authorid
ORDER BY [Books].[YearOfPublishing] DESC

END
GO
/****** Object:  StoredProcedure [dbo].[GetBooksByPublishingOffice]    Script Date: 10/25/2021 2:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetBooksByPublishingOffice] 
	@publishingOfficeName nvarchar(300)
AS
BEGIN

SELECT [Books].[BookID], [Books].[Title], [Cities].[CityName], [PublishingOffices].[PublishingOfficeName], [Books].[PagesCount], 
[Books].[YearOfPublishing], [Books].[Description], [Books].[ISBN]
FROM[dbo].[Books] LEFT JOIN[dbo].[Cities] ON[dbo].[Books].[CityID] = [dbo].[Cities].[CityID] 
LEFT JOIN[dbo].[PublishingOffices] ON[dbo].[Books].[PublishingOfficeID] = [dbo].[PublishingOffices].[PublishingOfficeID] 
WHERE [PublishingOffices].[PublishingOfficeName] = @publishingOfficeName

END;
GO
/****** Object:  StoredProcedure [dbo].[GetBooksByPublishingOfficeDateASC]    Script Date: 10/25/2021 2:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetBooksByPublishingOfficeDateASC] 
	@publishingOfficeName nvarchar(300)
AS
BEGIN

SELECT [Books].[BookID], [Books].[Title], [Cities].[CityName], [PublishingOffices].[PublishingOfficeName], [Books].[PagesCount], 
[Books].[YearOfPublishing], [Books].[Description], [Books].[ISBN]
FROM[dbo].[Books] LEFT JOIN[dbo].[Cities] ON[dbo].[Books].[CityID] = [dbo].[Cities].[CityID] 
LEFT JOIN[dbo].[PublishingOffices] ON[dbo].[Books].[PublishingOfficeID] = [dbo].[PublishingOffices].[PublishingOfficeID] 
WHERE [PublishingOffices].[PublishingOfficeName] = @publishingOfficeName
ORDER BY [Books].[YearOfPublishing] ASC

END;
GO
/****** Object:  StoredProcedure [dbo].[GetBooksByPublishingOfficeDateDESC]    Script Date: 10/25/2021 2:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetBooksByPublishingOfficeDateDESC] 
	@publishingOfficeName nvarchar(300)
AS
BEGIN

SELECT [Books].[BookID], [Books].[Title], [Cities].[CityName], [PublishingOffices].[PublishingOfficeName], [Books].[PagesCount], 
[Books].[YearOfPublishing], [Books].[Description], [Books].[ISBN]
FROM[dbo].[Books] LEFT JOIN[dbo].[Cities] ON[dbo].[Books].[CityID] = [dbo].[Cities].[CityID] 
LEFT JOIN[dbo].[PublishingOffices] ON[dbo].[Books].[PublishingOfficeID] = [dbo].[PublishingOffices].[PublishingOfficeID] 
WHERE [PublishingOffices].[PublishingOfficeName] = @publishingOfficeName
ORDER BY [Books].[YearOfPublishing] DESC

END;
GO
/****** Object:  StoredProcedure [dbo].[GetBooksByTitle]    Script Date: 10/25/2021 2:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetBooksByTitle]
	 @title nvarchar(300)
AS
BEGIN

SELECT [Books].[BookID], [Books].[Title], [Cities].[CityName], [PublishingOffices].[PublishingOfficeName], [Books].[PagesCount], [Books].[YearOfPublishing], [Books].[Description], [Books].[ISBN]
FROM[dbo].[Books]
LEFT JOIN[dbo].[Cities] ON[dbo].[Books].[CityID] = [dbo].[Cities].[CityID]
LEFT JOIN[dbo].[PublishingOffices] ON[dbo].[Books].[PublishingOfficeID] = [dbo].[PublishingOffices].[PublishingOfficeID]
WHERE [Books].[Title] LIKE '%'+@title+'%'

END
GO
/****** Object:  StoredProcedure [dbo].[GetBooksByTitleDateASC]    Script Date: 10/25/2021 2:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetBooksByTitleDateASC]
	 @title nvarchar(300)
AS
BEGIN

SELECT [Books].[BookID], [Books].[Title], [Cities].[CityName], [PublishingOffices].[PublishingOfficeName], [Books].[PagesCount], [Books].[YearOfPublishing], [Books].[Description], [Books].[ISBN]
FROM[dbo].[Books]
LEFT JOIN[dbo].[Cities] ON[dbo].[Books].[CityID] = [dbo].[Cities].[CityID]
LEFT JOIN[dbo].[PublishingOffices] ON[dbo].[Books].[PublishingOfficeID] = [dbo].[PublishingOffices].[PublishingOfficeID]
WHERE [Books].[Title] LIKE '%'+@title+'%'
ORDER BY [Books].[YearOfPublishing] ASC
END
GO
/****** Object:  StoredProcedure [dbo].[GetBooksByTitleDateDESC]    Script Date: 10/25/2021 2:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetBooksByTitleDateDESC]
	 @title nvarchar(300)
AS
BEGIN

SELECT [Books].[BookID], [Books].[Title], [Cities].[CityName], [PublishingOffices].[PublishingOfficeName], [Books].[PagesCount], [Books].[YearOfPublishing], [Books].[Description], [Books].[ISBN]
FROM[dbo].[Books]
LEFT JOIN[dbo].[Cities] ON[dbo].[Books].[CityID] = [dbo].[Cities].[CityID]
LEFT JOIN[dbo].[PublishingOffices] ON[dbo].[Books].[PublishingOfficeID] = [dbo].[PublishingOffices].[PublishingOfficeID]
WHERE [Books].[Title] LIKE '%'+@title+'%'
ORDER BY [Books].[YearOfPublishing] DESC
END
GO
/****** Object:  StoredProcedure [dbo].[GetNewspapersByPublishingOffice]    Script Date: 10/25/2021 2:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetNewspapersByPublishingOffice]
	@publishingOfficeName nvarchar(300)
AS
BEGIN

SELECT [Newspapers].[PaperID], [Newspapers].[Title], [Newspapers].[YearOfPublishing], [Newspapers].[PagesCount], [Newspapers].[Description], [Newspapers].[PaperNumber], [Newspapers].[Date], [Newspapers].[ISSN], [Cities].[CityName], [PublishingOffices].[PublishingOfficeName] 
FROM[dbo].[Newspapers]  
LEFT JOIN [dbo].[Cities] ON [dbo].[Newspapers].[CityID] = [dbo].[Cities].[CityID] 
LEFT JOIN [dbo].[PublishingOffices] ON [dbo].[Newspapers].[PublishingOfficeID] = [dbo].[PublishingOffices].[PublishingOfficeID]  
WHERE [PublishingOffices].[PublishingOfficeName] = @publishingOfficeName

END
GO
/****** Object:  StoredProcedure [dbo].[GetNewspapersByPublishingOfficeDateASC]    Script Date: 10/25/2021 2:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetNewspapersByPublishingOfficeDateASC]
	@publishingOfficeName nvarchar(300)
AS
BEGIN

SELECT [Newspapers].[PaperID], [Newspapers].[Title], [Newspapers].[YearOfPublishing], [Newspapers].[PagesCount], [Newspapers].[Description], [Newspapers].[PaperNumber], [Newspapers].[Date], [Newspapers].[ISSN], [Cities].[CityName], [PublishingOffices].[PublishingOfficeName] 
FROM[dbo].[Newspapers]  
LEFT JOIN [dbo].[Cities] ON [dbo].[Newspapers].[CityID] = [dbo].[Cities].[CityID] 
LEFT JOIN [dbo].[PublishingOffices] ON [dbo].[Newspapers].[PublishingOfficeID] = [dbo].[PublishingOffices].[PublishingOfficeID]  
WHERE [PublishingOffices].[PublishingOfficeName] = @publishingOfficeName
ORDER BY [Newspapers].[Date] ASC
END
GO
/****** Object:  StoredProcedure [dbo].[GetNewspapersByPublishingOfficeDateDESC]    Script Date: 10/25/2021 2:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetNewspapersByPublishingOfficeDateDESC]
	@publishingOfficeName nvarchar(300)
AS
BEGIN

SELECT [Newspapers].[PaperID], [Newspapers].[Title], [Newspapers].[YearOfPublishing], [Newspapers].[PagesCount], [Newspapers].[Description], [Newspapers].[PaperNumber], [Newspapers].[Date], [Newspapers].[ISSN], [Cities].[CityName], [PublishingOffices].[PublishingOfficeName] 
FROM[dbo].[Newspapers]  
LEFT JOIN [dbo].[Cities] ON [dbo].[Newspapers].[CityID] = [dbo].[Cities].[CityID] 
LEFT JOIN [dbo].[PublishingOffices] ON [dbo].[Newspapers].[PublishingOfficeID] = [dbo].[PublishingOffices].[PublishingOfficeID]  
WHERE [PublishingOffices].[PublishingOfficeName] = @publishingOfficeName
ORDER BY [Newspapers].[Date] DESC
END
GO
/****** Object:  StoredProcedure [dbo].[GetNewspapersByTitle]    Script Date: 10/25/2021 2:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetNewspapersByTitle]
	@title nvarchar(300)
AS
BEGIN

SELECT [Newspapers].[PaperID], [Newspapers].[Title], [Newspapers].[YearOfPublishing], [Newspapers].[PagesCount], [Newspapers].[Description], [Newspapers].[PaperNumber], [Newspapers].[Date], [Newspapers].[ISSN], [Cities].[CityName], [PublishingOffices].[PublishingOfficeName]
FROM[dbo].[Newspapers]
LEFT JOIN [dbo].[Cities] ON [dbo].[Newspapers].[CityID] = [dbo].[Cities].[CityID]
LEFT JOIN [dbo].[PublishingOffices] ON [dbo].[Newspapers].[PublishingOfficeID] = [dbo].[PublishingOffices].[PublishingOfficeID]
WHERE [Newspapers].[Title] LIKE '%'+@title+'%'

END
GO
/****** Object:  StoredProcedure [dbo].[GetNewspapersByTitleDateASC]    Script Date: 10/25/2021 2:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetNewspapersByTitleDateASC]
	@title nvarchar(300)
AS
BEGIN

SELECT [Newspapers].[PaperID], [Newspapers].[Title], [Newspapers].[YearOfPublishing], [Newspapers].[PagesCount], [Newspapers].[Description], [Newspapers].[PaperNumber], [Newspapers].[Date], [Newspapers].[ISSN], [Cities].[CityName], [PublishingOffices].[PublishingOfficeName]
FROM[dbo].[Newspapers]
LEFT JOIN [dbo].[Cities] ON [dbo].[Newspapers].[CityID] = [dbo].[Cities].[CityID]
LEFT JOIN [dbo].[PublishingOffices] ON [dbo].[Newspapers].[PublishingOfficeID] = [dbo].[PublishingOffices].[PublishingOfficeID]
WHERE [Newspapers].[Title] LIKE '%'+@title+'%'
ORDER BY [Newspapers].[Date] ASC;
END
GO
/****** Object:  StoredProcedure [dbo].[GetNewspapersByTitleDateDESC]    Script Date: 10/25/2021 2:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetNewspapersByTitleDateDESC]
	@title nvarchar(300)
AS
BEGIN

SELECT [Newspapers].[PaperID], [Newspapers].[Title], [Newspapers].[YearOfPublishing], [Newspapers].[PagesCount], [Newspapers].[Description], [Newspapers].[PaperNumber], [Newspapers].[Date], [Newspapers].[ISSN], [Cities].[CityName], [PublishingOffices].[PublishingOfficeName]
FROM[dbo].[Newspapers]
LEFT JOIN [dbo].[Cities] ON [dbo].[Newspapers].[CityID] = [dbo].[Cities].[CityID]
LEFT JOIN [dbo].[PublishingOffices] ON [dbo].[Newspapers].[PublishingOfficeID] = [dbo].[PublishingOffices].[PublishingOfficeID]
WHERE [Newspapers].[Title] LIKE '%'+@title+'%'
ORDER BY [Newspapers].[Date] DESC
END
GO
/****** Object:  StoredProcedure [dbo].[GetPatentsByAuthor]    Script Date: 10/25/2021 2:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetPatentsByAuthor]
	@authorid int
AS
BEGIN

SELECT [Patents].[PatentID], [Patents].[Title], [Patents].[PatentRegistrationNumber], [Countries].[CountryName], [Patents].[ApplicationDate], [Patents].[PublicationDate], [Patents].[PagesCount], [Patents].[Description]  
FROM[dbo].[Patents]  
LEFT JOIN[dbo].[Countries] ON[dbo].[Patents].[CountryID] = [dbo].[Countries].[CountryID]  
JOIN [InventorPatents] ON [Patents].[PatentID] = [InventorPatents].[PatentID] 
WHERE [InventorPatents].[AuthorId] = @authorid

END
GO
/****** Object:  StoredProcedure [dbo].[GetPatentsByAuthorDateASC]    Script Date: 10/25/2021 2:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetPatentsByAuthorDateASC]
	@authorid int
AS
BEGIN

	SELECT [Patents].[PatentID], [Patents].[Title], [Patents].[PatentRegistrationNumber], [Countries].[CountryName], [Patents].[ApplicationDate], [Patents].[PublicationDate], [Patents].[PagesCount], [Patents].[Description]  
	FROM[dbo].[Patents]  
	LEFT JOIN[dbo].[Countries] ON[dbo].[Patents].[CountryID] = [dbo].[Countries].[CountryID]  
	JOIN [InventorPatents] ON [Patents].[PatentID] = [InventorPatents].[PatentID] 
	WHERE [InventorPatents].[AuthorId] = @authorid
	ORDER BY [Patents].[PublicationDate] ASC

END
GO
/****** Object:  StoredProcedure [dbo].[GetPatentsByAuthorDateDESC]    Script Date: 10/25/2021 2:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetPatentsByAuthorDateDESC]
	@authorid int
AS
BEGIN

SELECT [Patents].[PatentID], [Patents].[Title], [Patents].[PatentRegistrationNumber], [Countries].[CountryName], [Patents].[ApplicationDate], [Patents].[PublicationDate], [Patents].[PagesCount], [Patents].[Description]  
FROM[dbo].[Patents]  
LEFT JOIN[dbo].[Countries] ON[dbo].[Patents].[CountryID] = [dbo].[Countries].[CountryID]  
JOIN [InventorPatents] ON [Patents].[PatentID] = [InventorPatents].[PatentID] 
WHERE [InventorPatents].[AuthorId] = @authorid
ORDER BY [Patents].[PublicationDate] DESC
END
GO
/****** Object:  StoredProcedure [dbo].[GetPatentsByTitle]    Script Date: 10/25/2021 2:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetPatentsByTitle]
	@title nvarchar(300)
AS
BEGIN

SELECT [Patents].[PatentID], [Patents].[Title], [Patents].[PatentRegistrationNumber], [Countries].[CountryName], [Patents].[ApplicationDate], [Patents].[PublicationDate], [Patents].[PagesCount], [Patents].[Description]  
FROM[dbo].[Patents]  
LEFT JOIN[dbo].[Countries] ON[dbo].[Patents].[CountryID] = [dbo].[Countries].[CountryID] 
WHERE [Patents].[Title] LIKE '%'+@title+'%'

END
GO
/****** Object:  StoredProcedure [dbo].[GetPatentsByTitleDateASC]    Script Date: 10/25/2021 2:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetPatentsByTitleDateASC]
	@title nvarchar(300)
AS
BEGIN

	SELECT [Patents].[PatentID], [Patents].[Title], [Patents].[PatentRegistrationNumber], [Countries].[CountryName], [Patents].[ApplicationDate], [Patents].[PublicationDate], [Patents].[PagesCount], [Patents].[Description]  
	FROM[dbo].[Patents]  
	LEFT JOIN[dbo].[Countries] ON[dbo].[Patents].[CountryID] = [dbo].[Countries].[CountryID] 
	WHERE [Patents].[Title] LIKE '%'+@title+'%'
	ORDER BY [Patents].[PublicationDate] ASC

END
GO
/****** Object:  StoredProcedure [dbo].[GetPatentsByTitleDateDESC]    Script Date: 10/25/2021 2:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetPatentsByTitleDateDESC]
	@title nvarchar(300)
AS
BEGIN

	SELECT [Patents].[PatentID], [Patents].[Title], [Patents].[PatentRegistrationNumber], [Countries].[CountryName], [Patents].[ApplicationDate], [Patents].[PublicationDate], [Patents].[PagesCount], [Patents].[Description]  
	FROM[dbo].[Patents]  
	LEFT JOIN[dbo].[Countries] ON[dbo].[Patents].[CountryID] = [dbo].[Countries].[CountryID] 
	WHERE [Patents].[Title] LIKE '%'+@title+'%'
	ORDER BY [Patents].[PublicationDate] DESC

END
GO
USE [master]
GO
ALTER DATABASE [LibraryDB] SET  READ_WRITE 
GO
