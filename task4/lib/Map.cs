﻿using System.Collections.Generic;

namespace Lib
{
    public class Map
    {
        public int Height { get; set; }
        public int Width { get; set; }
        public int LevelId { get; set; }
        public List<Obstacle> Obstacles { get; }
        public List<Monster> Monsters { get; }
        public List<Bonus> Bonuses { get; }
        public int Counter { get; set; }
        public Player player;

        public Map(int height, int width, List<Obstacle> obstacles, List<Bonus> bonuses, List<Monster> monsters)
        {
            Height = height;
            Width = width;
            Obstacles = obstacles;
            Bonuses = bonuses;
            Monsters = monsters;
            Counter = 0;
        }

        public Map()
        {

            Counter = 0;
        }

        public bool IsThereFreePlace(int x, int y, bool allObstaclesImpassable = true)
        {
            if (x < 1 || x > Width)
                return false;
            if (y < 1 || y > Height)
                return false;
            if (allObstaclesImpassable)
            {
                foreach (var obstacle in Obstacles)
                {
                    if (obstacle.X == x && obstacle.Y == y)
                        return false;
                }
                foreach (var monster in Monsters)
                {
                    if (monster.X == x && monster.Y == y)
                        return false;
                }
            }
            else
            {
                foreach (var obstacle in Obstacles)
                {
                    if (obstacle.X == x && obstacle.Y == y && !obstacle.IsPassable)
                        return false;
                }
            }
            return true;
        }

        public bool IsThereNoBorder(int x, int y, bool allObstaclesImpassable = true)
        {
            if (x < 1 || x > Width)
                return false;
            if (y < 1 || y > Height)
                return false;
            return true;
        }

        public Bonus TryFindBonus(int x, int y)
        {
            foreach (var bonus in Bonuses)
            {
                if (bonus.X == x && bonus.Y == y)
                    return bonus;
            }
            return null;
        }
    }
}
