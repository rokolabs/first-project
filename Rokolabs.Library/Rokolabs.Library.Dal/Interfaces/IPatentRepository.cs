﻿using Rokolabs.Library.Dal.DTOs;
using System.Collections.Generic;

namespace Rokolabs.Library.Dal.Interfaces
{
    public interface IPatentRepository
    {
        void DeletePatent(int patentId);
        List<PatentDto> GetAllPatents();
        List<PatentDto> GetAllPatentsDateAsc();
        List<PatentDto> GetAllPatentsDateDesc();
        int InsertPatent(PatentDto patent);
        List<PatentDto> SearchPatents(PatentDto patent);
        List<PatentDto> SearchPatentsDateAsc(PatentDto patent);
        List<PatentDto> SearchPatentsDateDesc(PatentDto patent);
        List<PatentDto> SearchPatentsByAuthor(AuthorDto author);
        List<PatentDto> SearchPatentsByAuthorDateAsc(AuthorDto author);
        List<PatentDto> SearchPatentsByAuthorDateDesc(AuthorDto author);
        PatentDto GetPatent(PatentDto patent);
    }
}
