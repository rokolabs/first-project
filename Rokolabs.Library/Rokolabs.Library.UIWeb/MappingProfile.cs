﻿using AutoMapper;
using Rokolabs.Library.Dal.DTOs;
using Rokolabs.Library.Data;
using System.Linq;

namespace Rokolabs.Library.UIWeb
{
	public class MappingProfile : Profile
	{
		public MappingProfile()
		{
			CreateMap<Author, AuthorDto>();
			CreateMap<AuthorDto, Author>();
			CreateMap<Book, BookDto>();
			CreateMap<BookDto, Book>();
			CreateMap<CityDto, City>();
			CreateMap<City, CityDto>();
			CreateMap<Patent, PatentDto>();
			CreateMap<PatentDto, Patent>();
			CreateMap<PublishingOfficeDto, PublishingOffice>();
			CreateMap<PublishingOffice, PublishingOfficeDto>();
			CreateMap<Newspaper, NewspaperDto>();
			CreateMap<NewspaperDto, Newspaper>();
		}
	}
}
