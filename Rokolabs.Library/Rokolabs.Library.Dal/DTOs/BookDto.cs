﻿using System;

namespace Rokolabs.Library.Dal.DTOs
{
	public class BookDto
	{
		public int BookId { get; set; }
		public string Title { get; set; }
		public string Description { get; set; }
		public int CityID { get; set; }
		public string CityName { get; set; }
		public string PublishingOfficeName { get; set; }
		public int PublishingOfficeID { get; set; }
		public short YearOfPublishing { get; set; }
		public short PagesCount { get; set; }
		public string ISBN { get; set; }
	}
}
