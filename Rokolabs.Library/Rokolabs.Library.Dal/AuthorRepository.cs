﻿using Dapper;
using Microsoft.Extensions.Logging;
using Rokolabs.Library.Dal.DTOs;
using Rokolabs.Library.Dal.Interfaces;
using Rokolabs.Library.Data;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Rokolabs.Library.Dal
{
    public class AuthorRepository : IAuthorRepository
    {
        private readonly ILogger<AuthorRepository> _logger;
        private readonly DBOptions _options;

        public AuthorRepository(ILogger<AuthorRepository> logger, DBOptions options)
        {
            _logger = logger;
            _options = options;
        }

        public List<AuthorDto> GetAllAuthors()
        {
            using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
                var query = "SELECT * FROM [Authors]";
                return db.Query<AuthorDto>(query).ToList();
            }
        }

        public int InsertAuthor(AuthorDto author)
        {
            using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
                var query = "INSERT INTO [Authors] ([FirstName], [LastName], [Description]) VALUES (@firstname, @lastname, @description); SELECT CAST(SCOPE_IDENTITY() as int);";
                return db.Query<int>(query, author).FirstOrDefault();
            }
        }

        public void UpdateAuthor(int authorId, AuthorDto author)
        {
            using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
                var query = "UPDATE [Authors] SET [FirstName] = @firstname, [LastName] = @lastname WHERE [AuthorId] = @authorId";
                db.Execute(query, author);
            }
        }
        public List<AuthorDto> GetAuthorsByBooksIds(IEnumerable<int> ids)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("EntityID");
            foreach (var id in ids)
            {
                dt.Rows.Add(id);
            }

            using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
                return db.Query<AuthorDto>("GetAuthorsByBookIDs",
                    new { ids = dt.AsTableValuedParameter("dtIntEntity") },
                    commandType: CommandType.StoredProcedure
                    ).ToList();
            }
        }

        public void DeleteAuthor(int authorId)
        {
            using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
                var query = "DELETE FROM [Authors] WHERE [AuthorId] = @authorId";
                db.Execute(query, new { authorId } );
            }
        }

        public List<AuthorDto> SearchAuthors(AuthorDto author)
        {
            using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
                var query = "SELECT [AuthorID], [FirstName], [LastName], [Description] FROM [Authors] WHERE [Authors].[FirstName] LIKE '%'+@firstname+'%' AND [Authors].[LastName] LIKE '%'+@lastname+'%'";
                return db.Query<AuthorDto>(query, author).ToList();
            }
        }

        public AuthorDto GetAuthor(AuthorDto author)
        {
            using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
                var query = "SELECT [AuthorID], [FirstName], [LastName], [Description] FROM [Authors] WHERE [Authors].[FirstName] = @firstname AND [Authors].[LastName] = @lastname";
                return db.Query<AuthorDto>(query, author).FirstOrDefault();
            }
        }

        public List<AuthorDto> GetAuthorsByPatentsIds(IEnumerable<int> ids)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("EntityID");
            foreach (var id in ids)
            {
                dt.Rows.Add(id);
            }

            using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
                return db.Query<AuthorDto>("GetAuthorsByPatentIDs",
                    new { ids = dt.AsTableValuedParameter("dtIntEntity") },
                    commandType: CommandType.StoredProcedure
                    ).ToList();
            }
        }

        public void AddAuthorToBook(BookDto book, AuthorDto author)
        {
            using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
                var query = "INSERT INTO [AuthorBooks] ([AuthorID], [BookID]) VALUES (@authorID, @bookID);";
                db.Execute(query, new { authorID = author.AuthorId, bookID = book.BookId });
            }
        }

        public void AddAuthorToPatent(PatentDto patent, AuthorDto author)
        {
            using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
                var query = "INSERT INTO [InventorPatents] ([AuthorID], [PatentID]) VALUES (@authorID, @patentID);";
                db.Execute(query, new { authorID = author.AuthorId, patentID = patent.PatentID });
            }
        }

        public List<AuthorDto> SearchAuthors(string search)
        {
            using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
                var query = "SELECT [AuthorID], [FirstName], [LastName], [Description] FROM [Authors] WHERE [Authors].[FirstName] LIKE @search OR [Authors].[LastName] LIKE @search";
                return db.Query<AuthorDto>(query, new { search = $"%{search}%"}).ToList();
            }
        }
    }
}
