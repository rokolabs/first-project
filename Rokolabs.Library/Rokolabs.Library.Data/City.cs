﻿namespace Rokolabs.Library.Data
{
    public class City
    {
        public int CityID { get; set; }
        public string CityName { get; set; }
    }
}
