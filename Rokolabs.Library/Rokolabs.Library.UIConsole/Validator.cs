﻿using Microsoft.Extensions.Logging;
using Rokolabs.Library.Bll.Interfaces;
using Rokolabs.Library.Data;
using System;

namespace Rokolabs.Library.UIConsole
{
    public class Validator
    {
        private readonly IAuthorService _authorService;
        private readonly IBookService _bookService;
        private readonly IPatentService _patentService;
        private readonly ICityService _cityService;
        private readonly ICountryService _countryService;
        private readonly IPublishingOfficeService _publishingOfficeService;
        private readonly INewspaperService _newspaperService;
        private readonly Drawer _drawer;
        private readonly ILogger<Worker> _logger;

        public Validator(IAuthorService authorService, IBookService bookService, IPatentService patentService, ICityService cityService, 
            ICountryService countryService, IPublishingOfficeService publishingOfficeService, INewspaperService newspaperService, Drawer drawer, ILogger<Worker> logger)
        {
            _authorService = authorService;
            _bookService = bookService;
            _patentService = patentService;
            _cityService = cityService;
            _countryService = countryService;
            _publishingOfficeService = publishingOfficeService;
            _newspaperService = newspaperService;
            _drawer = drawer;
            _logger = logger;
        }
        
        
        public int CheckEntityInDatabase(string name, IDefiningEntity entityRepostitory, string type)
        {
            int id = entityRepostitory.FindThisEntity(name);
            if (id == 0)
            {
                Console.WriteLine($"Can't find same {type} in datebase. It will'be added");
                id = entityRepostitory.InsertThisEntity(name);
            }
            return id;
        }

        public bool ValidateBook(Book book)
        {
            book.CityID = CheckEntityInDatabase(book.CityName, _cityService, "city");
            book.PublishingOfficeID = CheckEntityInDatabase(book.PublishingOfficeName, _publishingOfficeService, "publishing office");
            Book dbBook;
            if (book.ISBN != String.Empty)
            {
                dbBook = _bookService.GetBookByISBN(book);
                if (dbBook.BookId > 0)
                {
                    Console.WriteLine("There is the same book in library already");
                    _logger.LogWarning($"Attempt to add book with ISBN {dbBook.ISBN} that already in library");
                    return false;
                }
                else
                {
                    book.BookId = _bookService.InsertBook(book);
                    return true;
                }
            }
            else
            {
                dbBook = _bookService.GetBook(book);
                if (dbBook.BookId > 0)
                    if (dbBook.Authors == book.Authors)
                    {
                        Console.WriteLine("There is the same book in library already");
                        _logger.LogWarning($"Attempt to add book with parameters {book.Title} ({book.YearOfPublishing}) that already in library");
                        return false;
                    }
                book.BookId = _bookService.InsertBook(book);
                return true;
            }
        }

        public bool ValidatePatent(Patent patent)
        {
            patent.CountryID = CheckEntityInDatabase(patent.CountryName, _countryService, "country");
            var dbPatent = _patentService.GetPatent(patent);
            if (dbPatent != null)
            {
                Console.WriteLine("Patent with this registration number already in database");
                _logger.LogError($"Patent from {patent.CountryName} and number {dbPatent.PatentRegistrationNumber} already in database");
                return false;
            }
            patent.PatentID = _patentService.InsertPatent(patent);
            return true;
        }

        public bool ValidateNewspaper(Newspaper newspaper)
        {
            newspaper.CityID = CheckEntityInDatabase(newspaper.CityName, _cityService, "city");
            newspaper.PublishingOfficeID = CheckEntityInDatabase(newspaper.PublishingOfficeName, _publishingOfficeService, "publishing office");
            if (newspaper.ISSN != string.Empty)
            {
                var dbNewspaper = _newspaperService.GetNewspaperByISSN(newspaper);
                if (dbNewspaper != null)
                {
                    Console.WriteLine("Newspaper with this ISSN  already in database");
                    _logger.LogError($"Newspaper with ISSN {newspaper.ISSN} already in database");
                    return false;
                }
            }
            else
            {
                var dbNewspaper = _newspaperService.GetNewspaper(newspaper);
                if (dbNewspaper != null)
                {
                    Console.WriteLine("Same newspaper already in database");
                    _logger.LogError($"Newspaper with parameters {newspaper.Title} - {newspaper.PublishingOfficeName} by {newspaper.Date} already in database");
                    return false;
                }
            }
            newspaper.PaperID = _newspaperService.InsertNewspaper(newspaper);
            return true;
        }

        internal void ValidateAuthor(Author author)
        {
            var dbAuthor = _authorService.GetAuthor(author);
            if (dbAuthor != null)
            {
                var result = _drawer.DrawResolveAuthorProblem(dbAuthor, author);
                var answer = _drawer.CheckSelectedItem(5, 2);
                if (result && answer == 1)
                    _authorService.InsertAuthor(author);
                if (!result && answer == 1)
                {
                    Console.SetCursorPosition(0, 7);
                    Console.Write("New description: ");
                    string description = Console.ReadLine();
                    if (description != author.Description)
                    {
                        author.Description = description;
                        _authorService.InsertAuthor(author);
                    }
                }
            }
            else
                _authorService.InsertAuthor(author);
        }
    }
}
