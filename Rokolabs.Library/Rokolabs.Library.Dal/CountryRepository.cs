﻿using Dapper;
using Microsoft.Extensions.Logging;
using Rokolabs.Library.Dal.Interfaces;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Rokolabs.Library.Dal
{
    public class CountryRepository : ICountryRepository
    {
        private readonly ILogger<CountryRepository> _logger;
        private readonly DBOptions _options;

        public CountryRepository(ILogger<CountryRepository> logger, DBOptions options)
        {
            _logger = logger;
            _options = options;
        }

        public int FindCountry(string countryName)
        {
            using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
                var query = "SELECT ([CountryID]) FROM [Countries] WHERE [Countries].[CountryName] = @countryName";
                return db.Query<int>(query, new { countryName }).FirstOrDefault();
            }
        }

        public int InsertCountry(string countryName)
        {
            using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
                var query = "INSERT INTO [Countries] ([CountryName]) VALUES (@countryName); SELECT CAST(SCOPE_IDENTITY() as int);";
                return db.Query<int>(query, new { countryName }).FirstOrDefault();
            }
        }
    }
}
