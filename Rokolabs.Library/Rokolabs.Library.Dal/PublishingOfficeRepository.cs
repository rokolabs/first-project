﻿using Dapper;
using Microsoft.Extensions.Logging;
using Rokolabs.Library.Dal.DTOs;
using Rokolabs.Library.Dal.Interfaces;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Rokolabs.Library.Dal
{
    public class PublishingOfficeRepository : IPublishngOfficeRepository
    {
        private readonly ILogger<PublishingOfficeRepository> _logger;
        private readonly DBOptions _options;

        public PublishingOfficeRepository(ILogger<PublishingOfficeRepository> logger, DBOptions options)
        {
            _logger = logger;
            _options = options;
        }

        public int FindPublishingOffice(string publishingOfficeName)
        {
            using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
                var query = "SELECT [PublishingOfficeID] FROM [PublishingOffices] WHERE [PublishingOffices].[PublishingOfficeName] LIKE '%'+@publishingOfficeName+'%'";
                return db.Query<int>(query, new { publishingOfficeName }).FirstOrDefault();
            }
        }

        public List<PublishingOfficeDto> GetSuitablePublishingOffices(string name)
        {
            using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
                var query = "SELECT [PublishingOfficeID], [PublishingOfficeName] FROM [PublishingOffices] WHERE [PublishingOffices].[PublishingOfficeName] LIKE '%'+@name+'%'";
                return db.Query<PublishingOfficeDto>(query, new {name}).ToList();
            }
        }

        public int InsertPublishingOffice(string publishingOfficeName)
        {
            using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
                var query = "INSERT INTO [PublishingOffices] ([PublishingOfficeName]) VALUES (@publishingOfficeName); SELECT CAST(SCOPE_IDENTITY() as int);";
                return db.Query<int>(query, new { publishingOfficeName }).FirstOrDefault();
            }
        }
    }
}
