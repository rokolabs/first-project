﻿using System;

namespace Lib
{
    public class Blueberry : Bonus
    {
        public override void Draw()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.SetCursorPosition(X, Y);
            Console.Write("o");
        }

        public Blueberry(int x, int y)
            : base(x, y, 10)
        {
        }

        public Blueberry()
        {
            Energy = 10;
        }
    }
}
