﻿using System;

namespace Lib
{
    class Stone : Obstacle
    {
        public Stone(int x, int y)
            : base(x, y)
        {
            IsPassable = false;
            HitRange = 0;
        }

        public Stone()
        {
            IsPassable = false;
            HitRange = 0;
        }

        public override void Draw()
        {
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.SetCursorPosition(X, Y);
            Console.Write("S");
        }
    }
}
