﻿using System;
using System.Collections.Generic;

namespace Lib
{
    public class Bear : Monster
    {
        private List<Action<IsMovingAllowed, Map>> stepActions;

        public Bear(int x, int y) : base(x, y)
        {
            HitRange = 2;
            PassNextSteps = 0;
            stepActions = new List<Action<IsMovingAllowed, Map>>
            {
                CatchPlayer, EatRaspberry, MoveRandom
            };
        }

        public Bear()
        {
            HitRange = 2;
            PassNextSteps = 0;
            stepActions = new List<Action<IsMovingAllowed, Map>>
            {
                CatchPlayer, EatRaspberry, MoveRandom
            };
        }  

        public override void DoNextStep(Map map)
        {
            DoneStep = false;
            IsMovingAllowed isMovingAllowed = map.IsThereFreePlace;
            if (IgnoreObstaclesCounter > 0)
            {
                isMovingAllowed = map.IsThereNoBorder;
                IgnoreObstaclesCounter--;
            }
            foreach (var action in stepActions)
            {
                if (!DoneStep)
                    action(isMovingAllowed, map);
            }
        }

        private void CatchPlayer(IsMovingAllowed isMovingAllowed, Map map)
        {
            if (PassNextSteps == 0)
            {
                Previous_X = X;
                Previous_Y = Y;
                if (map.player.X == X || map.player.Y == Y)
                {
                    if (map.player.Y > Y && isMovingAllowed(X, Y + 1))
                        Y++;
                    else
                    if (map.player.Y < Y && isMovingAllowed(X, Y - 1))
                        Y--;
                    if (map.player.X > X && isMovingAllowed(X + 1, Y))
                        X++;
                    else
                    if (map.player.X < X && isMovingAllowed(X - 1, Y))
                        X--;
                }
                if (Previous_X != X || Previous_Y != Y)
                    DoneStep = true;
            }
            else
                PassNextSteps--;
        }

        private void EatRaspberry(IsMovingAllowed isMovingAllowed, Map map)
        {
            foreach (var bonus in map.Bonuses)
            {
                if (bonus is Raspberry)
                {
                    Previous_X = X;
                    Previous_Y = Y;
                    bool raspberryNear = Math.Abs(bonus.X - X) <= 5 && Math.Abs(bonus.Y - Y) <= 5;
                    if (raspberryNear)
                    {
                        if (bonus.X < X && isMovingAllowed(X - 1, Y))
                            X--;
                        if (bonus.X > X && isMovingAllowed(X + 1, Y))
                            X++;
                        if (bonus.Y < Y && isMovingAllowed(X, Y - 1))
                            Y--;
                        if (bonus.Y > Y && isMovingAllowed(X, Y + 1))
                            Y++;
                    }
                    if (Previous_X != X || Previous_Y != Y)
                    {
                        DoneStep = true;
                        break;
                    }
                }
            }
        }

        public override void Draw()
        {
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.SetCursorPosition(X, Y);
            Console.Write("M");
        }

        public override void GetBonus(Bonus bonus)
        {
            if (bonus is Raspberry)
            { 
                HitRange *= 2;
                bonus.Availiable = false;
            }
            else
            if (bonus is Blueberry)
            {
                IgnoreObstaclesCounter = 7;
                bonus.Availiable = false;
            }
        }

        public override void Hit(Player player)
        {
            player.Hurt(HitRange);
            PassNextSteps = 1;
        }
    }
}
