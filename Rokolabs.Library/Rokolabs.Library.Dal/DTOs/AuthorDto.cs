﻿namespace Rokolabs.Library.Dal.DTOs
{
    public class AuthorDto
    {
		public int AuthorId { get; set; }
		public int? BookId { get; set; }
		public int? PatentId { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Description { get; set; }

	}
}
