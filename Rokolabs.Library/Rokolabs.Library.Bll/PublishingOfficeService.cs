﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using Rokolabs.Library.Bll.Interfaces;
using Rokolabs.Library.Dal.DTOs;
using Rokolabs.Library.Dal.Interfaces;
using Rokolabs.Library.Data;
using System.Collections.Generic;

namespace Rokolabs.Library.Bll
{
    public class PublishingOfficeService : IPublishingOfficeService
    {
        private readonly ILogger<PublishingOfficeService> _logger;
        private readonly IPublishngOfficeRepository _publishngOfficeRepository;
        private readonly IMapper _mapper;

        public PublishingOfficeService(ILogger<PublishingOfficeService> logger, IMapper mapper, IPublishngOfficeRepository publishngOfficeRepository)
        {
            _logger = logger;
            _publishngOfficeRepository = publishngOfficeRepository;
            _mapper = mapper;
        }

        public int FindThisEntity(string name)
        {
            return _publishngOfficeRepository.FindPublishingOffice(name);
        }

        public List<PublishingOffice> GetSuitablePublishingOffices(string name)
        {
            return _mapper.Map<List<PublishingOffice>>(_publishngOfficeRepository.GetSuitablePublishingOffices(name));
        }

        public int InsertThisEntity(string name)
        {
            return _publishngOfficeRepository.InsertPublishingOffice(name);
        }
    }
}
