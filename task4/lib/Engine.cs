﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Lib
{
    public class Engine
    {
        private Map _map;
        private List<Bonus> _bonuses;
        private List<IActor> _actors;
        private List<BaseObject> _drawable;
        private List<IHitter> _hitters;
        private Player _player;
        public bool successfulPassage { get; set; }
        public string playerNickname { get; set; }

        public delegate void NewGameMode();
        public delegate void LoadingGameMode();
        public delegate void SavingGameMode();

        public NewGameMode StartNewgame { get; set; }
        public LoadingGameMode LoadGame { get; set; }
        public SavingGameMode SaveGame { get; set; }

        public Engine()
        {
            _bonuses = new List<Bonus>();
            _actors = new List<IActor>();
            _drawable = new List<BaseObject>();
            _hitters = new List<IHitter>();
            playerNickname = String.Empty;
            StartNewgame = ChooseLocalNewgameLevel;
            LoadGame = LoadLocalGame;
            SaveGame = SaveLocal;
            successfulPassage = true;
        }

        public void Start()
        {
            ChooseGameMode();
            Console.WriteLine("CHOOSE GAME MODE");
            Console.Write("PRESS N FOR NEW GAME AND L FOR LOADING LAST GAME: ");
            ConsoleKeyInfo info = Console.ReadKey();
            switch (info.Key)
            {
                case ConsoleKey.N:
                    StartNewgame();
                    break;
                case ConsoleKey.L:
                    try
                    {
                        LoadGame();
                        break;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        break;
                    }
                default:
                    Console.WriteLine("\nINVALID KEY");
                    break;
            }
        }

        public void Run()
        {
            DrawMap();
            foreach (var item in _drawable)
            {
                item.Draw();
            }
            DrawFooter();
            Console.SetCursorPosition(0, _map.Height + 9);
            ConsoleKeyInfo keyInfo = Console.ReadKey();
            while (_player.IsAlive() && _bonuses.Any() && keyInfo.Key != ConsoleKey.S)
            {
                _player.Key = keyInfo.Key;
                NextStep();
                MeetHitter();
                GetBonus();
                CleanBonuses();
                CleanAfterStep();
                Draw();
                DrawFooter();
                _map.Counter++;
                Console.SetCursorPosition(0, _map.Height + 9);
                keyInfo = Console.ReadKey();
            }
            if (keyInfo.Key == ConsoleKey.S)
            {
                SaveGame();
            }
            else
            {
                if (!_player.IsAlive())
                {
                    Console.SetCursorPosition(0, _map.Height + 7);
                    Console.WriteLine("GAME OVER  \n");
                    successfulPassage = false;
                }
                else
                {
                    Console.SetCursorPosition(0, _map.Height + 7);
                    Console.WriteLine("VICTORY \n");
                    SaveResult();
                }
                Console.SetCursorPosition(0, _map.Height + 9);
                Console.WriteLine("PRESS ESC");
                ConsoleKeyInfo info = Console.ReadKey();
                while (info.Key != ConsoleKey.Escape)
                {
                    info = Console.ReadKey();
                }
            }
        }

        public void SaveResult()
        {
            Saver saver = new Saver(AppDomain.CurrentDomain.BaseDirectory);
            saver.SaveResult(_map);
        }

        public void Draw()
        {
            foreach (var item in _drawable)
            {
                item.Draw();
            }
        }

        public void CleanAfterStep()
        {
            foreach (var actor in _actors)
            {
                actor.Clean();
            }
        }

        public void NextStep()
        {
            foreach (var actor in _actors)
            {
                actor.DoNextStep(_map);
            }
        }

        public void ChooseGameMode()
        {
            Console.Write("CHOOSE GAME MODE: LOCAL / SERVER ");
            Console.WriteLine("IN SERVER MODE:");
            Console.WriteLine("ALL LEVELS ARE AVAILIABLE");
            Console.WriteLine("GAME SAVES ON THE SERVER");
            Console.WriteLine("YOUR GAME RESULTS WILL BE SAVED ON THE SERVER");
            Console.WriteLine("IN LOCAL MODE:");
            Console.WriteLine("2 DEMONSTRATION LEVELS ARE AVAILIABLE");
            Console.WriteLine("GAME SAVES ON DISK");
            Console.WriteLine("PRESS L FOR LOCAL GAME AND S FOR SERVER");
            var gameMode = Console.ReadKey();
            Console.WriteLine();
            if (gameMode.Key == ConsoleKey.L)
            {
                StartNewgame = ChooseLocalNewgameLevel;
                LoadGame = LoadLocalGame;
                SaveGame = SaveLocal;
            }
            else if (gameMode.Key == ConsoleKey.S)
            {
                StartNewgame = ChooseServerNewgameLevel;
                LoadGame = LoadServerGame;
                SaveGame = SaveOnServer;
            }
        }

        public void ChooseServerNewgameLevel()
        {
            int level = 1;
            Console.Write("\nINPUT YOUR NICKNAME: ");
            playerNickname = Console.ReadLine();
            while (level <= 5 && successfulPassage)
            {
                GoToNextLevel(level);
                NewServerGame(level);
                level++;
            }
        }

        public void ChooseLocalNewgameLevel()
        {
            int level = 1;
            while (level <= 2 && successfulPassage)
            {
                GoToNextLevel(level);
                NewLocalGame(level);
            }
        }

        public static void GoToNextLevel(int level)
        {
            Console.Clear();
            Console.Write($"\nENTRANCE TO THE LEVEL {level}\n");
            Console.ReadKey();
            Console.Clear();
        }

        public void LoadLocalGame()
        {
            Loader loader = new Loader(AppDomain.CurrentDomain.BaseDirectory);
            GameState gameState = new GameState();
            loader.LoadPreviousGame(gameState);
            _map = gameState.MapExample;
            _player = gameState.PlayerExample;
            _drawable = gameState.DrawableObjects;
            _actors = gameState.ActorsList;
            _hitters = gameState.HittersList;
            _bonuses = gameState.BonusesList;
            Run();
            int level = _map.LevelId;
            if (level < 2 && successfulPassage)
            {
                GoToNextLevel(level);
                NewLocalGame(2); 
            }
        }

        public void LoadServerGame()
        {
            Console.Write("\nINPUT YOUR NICKNAME: ");
            playerNickname = Console.ReadLine();

            Loader loader = new Loader(AppDomain.CurrentDomain.BaseDirectory);
            GameState gameState = new GameState();
            loader.LoadPreviousGameFromServer(playerNickname, gameState);
            _map = gameState.MapExample;
            _player = gameState.PlayerExample;
            _drawable = gameState.DrawableObjects;
            _actors = gameState.ActorsList;
            _hitters = gameState.HittersList;
            _bonuses = gameState.BonusesList;
            Run();
            int level = _map.LevelId;
            while (level <= 5 && successfulPassage)
            {
                level++;
                GoToNextLevel(level);
                NewServerGame(level);
            }
        }

        public void NewLocalGame(int level)
        {
            LevelGenerator newLevel = new LevelGenerator();
            GameState gameState = new GameState();
            Loader loader = new Loader(AppDomain.CurrentDomain.BaseDirectory);
            string levelName = newLevel.CreateLevelFile(30, 60, level);
            loader.LoadLevelFromFile(level, levelName, playerNickname, gameState);
            _map = gameState.MapExample;
            _player = gameState.PlayerExample;
            _drawable = gameState.DrawableObjects;
            _actors = gameState.ActorsList;
            _hitters = gameState.HittersList;
            _bonuses = gameState.BonusesList;
            Run();
        }
        public void NewServerGame(int level)
        {
            GameState gameState = new GameState();
            Loader loader = new Loader(AppDomain.CurrentDomain.BaseDirectory);
            loader.LoadLevelFromServer(level, playerNickname, gameState);
            _map = gameState.MapExample;
            _player = gameState.PlayerExample;
            _drawable = gameState.DrawableObjects;
            _actors = gameState.ActorsList;
            _hitters = gameState.HittersList;
            _bonuses = gameState.BonusesList;
            Run();
        }

        public void SaveLocal()
        {
            Saver saver = new Saver(AppDomain.CurrentDomain.BaseDirectory);
            List<string> serializedObjects = saver.GetDataFromGame(_map, _drawable);
            saver.Save(serializedObjects);
            successfulPassage = false;
        }

        public void SaveOnServer()
        {
            Saver saver = new Saver(AppDomain.CurrentDomain.BaseDirectory);
            saver.SaveGameToServer(_map, _drawable);
            successfulPassage = false;
        }

        public void MeetHitter()
        {
            foreach (var hitter in _hitters)
            {
                if (hitter.X == _player.X && hitter.Y == _player.Y)
                    hitter.Hit(_player);
            }
        }

        public void GetBonus()
        {
            foreach (var actor in _actors)
            {
                Bonus bonus = _map.TryFindBonus(((BaseObject)actor).X, ((BaseObject)actor).Y);
                if (bonus != null)
                {
                    actor.GetBonus(bonus);
                }
            }
        }

        public void DrawMap()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.White;
            Console.SetCursorPosition(0, 0);
            Console.Write("\x250c");
            Console.SetCursorPosition(_map.Width + 1, 0);
            Console.Write("\x2510");

            for (int i = 1; i <= _map.Width; i++)
            {
                Console.SetCursorPosition(i, 0);
                Console.Write("\x2500");
                Console.SetCursorPosition(i, _map.Height + 1);
                Console.Write("\x2500");
            }
            for (int i = 1; i <= _map.Height + 1; i++)
            {
                Console.SetCursorPosition(0, i);
                Console.Write("\x2502");
                Console.SetCursorPosition(_map.Width + 1, i);
                Console.Write("\x2502");
            }

            Console.ForegroundColor = ConsoleColor.White;
            Console.SetCursorPosition(0, _map.Height + 1);
            Console.Write("\x2514");
            Console.SetCursorPosition(_map.Width + 1, _map.Height + 1);
            Console.Write("\x2518");
        }

        public void DrawFooter()
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.SetCursorPosition(0, _map.Height + 3);
            Console.WriteLine("                        ");
            Console.SetCursorPosition(0, _map.Height + 3);
            Console.WriteLine($"PLAYER HEALTH: {_player.Health}");
            Console.SetCursorPosition(0, _map.Height + 4);
            Console.WriteLine("                        ");
            Console.SetCursorPosition(0, _map.Height + 4);
            Console.WriteLine($"BONUSES LEFT: {_bonuses.Count}");
            Console.WriteLine("                             ");
            Console.SetCursorPosition(0, _map.Height + 5);
            Console.WriteLine($"STEPS MADE: {_map.Counter}");
            Console.WriteLine("                             ");
            Console.SetCursorPosition(0, _map.Height + 6);
            Console.WriteLine("PRESS S TO SAVE AND EXIT");
        }

        public void DrawStatistics()
        {
            Loader loader = new Loader(AppDomain.CurrentDomain.BaseDirectory);
            loader.LoadStatistics();
        }

        public void CleanBonuses()
        {
            for (int i = 0; i < _bonuses.Count; i++)
            {
                if (!_bonuses[i].Availiable)
                {
                    _drawable.Remove(_bonuses[i]);
                    _bonuses.Remove(_bonuses[i]);
                }
            }
        }
    }
}
