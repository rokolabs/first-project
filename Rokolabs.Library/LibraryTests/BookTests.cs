﻿using System;
using NUnit.Framework;
using Rokolabs.Library.Data;


namespace LibraryTests
{
    public class BookTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestPagesCount()
        {
            Book book = new Book();
            book.PagesCount = -12;
            Assert.True(book.PagesCount >= 0);
        }

        [Test]
        public void TestISBN()
        {
            Book book = new Book();
            book.ISBN = "dsfdsf";
            Assert.AreEqual(book.ISBN, String.Empty);
            book.ISBN = "ISBN 12345678912345678";
            Assert.AreEqual(book.ISBN, String.Empty);
            book.ISBN = "ISBN 978-008-455-8";
            Assert.AreEqual(book.ISBN, "ISBN 978-008-455-8");
        }
        [Test]
        public void TestAuthors()
        {
            Book book = new Book();
            Assert.NotNull(book.Authors);
        }
        [Test]
        public void TestBookCompare()
        {
            Book book1 = new Book() { YearOfPublishing = 2020 };
            Book book2 = new Book() { YearOfPublishing = 2021 };
            Assert.True(book1.CompareTo(book2) < 0);
        }
    }
}
