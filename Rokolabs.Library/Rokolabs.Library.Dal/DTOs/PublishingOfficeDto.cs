﻿namespace Rokolabs.Library.Dal.DTOs
{
    public class PublishingOfficeDto
    {
        public int PublishingOfficeID { get; set; }
        public string PublishingOfficeName { get; set; }
    }
}
