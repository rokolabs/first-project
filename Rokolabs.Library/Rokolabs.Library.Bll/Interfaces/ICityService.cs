﻿using Rokolabs.Library.Data;
using System.Collections.Generic;

namespace Rokolabs.Library.Bll.Interfaces
{
    public interface ICityService: IDefiningEntity
    {
        List<City> GetCitiesNames(string name);
    }
}
