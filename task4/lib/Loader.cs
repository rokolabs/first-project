﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Text.Json;

namespace Lib
{
    public class Loader
    {
        public string ConfigPath { get; set; }
        public string DataPath { get; set; }
        public string ConfigFullName { get; set; }
        public string SavingsName { get; }

        private string connectionString;

        public Loader(string path)
        {
            ConfigPath = path;
            DataPath = Path.Combine(ConfigPath, "data");
            ConfigFullName = Path.Combine(ConfigPath, "Config.txt");
            SavingsName = GetSavingsName();
            connectionString = GetConnectionData();
        }

        private string GetConnectionData()
        {
            string[] data = File.ReadAllLines(ConfigFullName);
            for (int i = 0; i < data.Length; i++)
            {
                if (data[i] == "connection_data")
                    return data[i + 1];
            }
            return string.Empty;

        }

        private string GetSavingsName()
        {
            string[] data = File.ReadAllLines(ConfigFullName);
            for (int i = 0; i < data.Length; i++)
            {
                if (data[i] == "saves_name")
                    return data[i + 1];
            }
            return string.Empty;
        }

        public void LoadLevelFromFile(int level, string levelname, string playerNickname, GameState gameState)
        {
            string[] data = File.ReadAllLines(Path.Combine(DataPath, levelname));
            for (int i = 0; i < data.Length; i++)
            {
                switch (data[i])
                {
                    case "map":
                        int[] coordinate = GetCoordinate(data[i + 1]);
                        gameState.MapExample = new Map(coordinate[0], coordinate[1], gameState.ObstaclesList, gameState.BonusesList, gameState.MonstersList);
                        gameState.MapExample.LevelId = level;
                        break;
                    case "player":
                        coordinate = GetCoordinate(data[i + 1]);
                        gameState.PlayerExample = new Player(coordinate[0], coordinate[1], playerNickname);
                        gameState.DrawableObjects.Add(gameState.PlayerExample);
                        gameState.ActorsList.Add(gameState.PlayerExample);
                        gameState.MapExample.player = gameState.PlayerExample;
                        break;
                    case "wolf":
                        coordinate = GetCoordinate(data[i + 1]);
                        Wolf wolf = new Wolf(coordinate[0], coordinate[1]);
                        gameState.ActorsList.Add(wolf);
                        gameState.MonstersList.Add(wolf);
                        gameState.DrawableObjects.Add(wolf);
                        gameState.HittersList.Add(wolf);
                        break;
                    case "bear":
                        coordinate = GetCoordinate(data[i + 1]);
                        Bear bear = new Bear(coordinate[0], coordinate[1]);
                        gameState.ActorsList.Add(bear);
                        gameState.MonstersList.Add(bear);
                        gameState.DrawableObjects.Add(bear);
                        gameState.HittersList.Add(bear);
                        break;
                    case "fox":
                        coordinate = GetCoordinate(data[i + 1]);
                        Fox fox = new Fox(coordinate[0], coordinate[1]);
                        gameState.ActorsList.Add(fox);
                        gameState.MonstersList.Add(fox);
                        gameState.DrawableObjects.Add(fox);
                        gameState.HittersList.Add(fox);
                        break;
                    case "tree":
                        coordinate = GetCoordinate(data[i + 1]);
                        Tree tree = new Tree(coordinate[0], coordinate[1]);
                        gameState.ObstaclesList.Add(tree);
                        gameState.DrawableObjects.Add(tree);
                        break;
                    case "wall":
                        coordinate = GetCoordinate(data[i + 1]);
                        Wall wall = new Wall(coordinate[0], coordinate[1]);
                        gameState.ObstaclesList.Add(wall);
                        gameState.DrawableObjects.Add(wall);
                        break;
                    case "stone":
                        coordinate = GetCoordinate(data[i + 1]);
                        Stone stone = new Stone(coordinate[0], coordinate[1]);
                        gameState.ObstaclesList.Add(stone);
                        gameState.DrawableObjects.Add(stone);
                        break;
                    case "apple":
                        coordinate = GetCoordinate(data[i + 1]);
                        Apple apple = new Apple(coordinate[0], coordinate[1]);
                        gameState.BonusesList.Add(apple);
                        gameState.DrawableObjects.Add(apple);
                        break;
                    case "raspberry":
                        coordinate = GetCoordinate(data[i + 1]);
                        Raspberry raspberry = new Raspberry(coordinate[0], coordinate[1]);
                        gameState.BonusesList.Add(raspberry);
                        gameState.DrawableObjects.Add(raspberry);
                        break;
                    case "blueberry":
                        coordinate = GetCoordinate(data[i + 1]);
                        Blueberry blueberry = new Blueberry(coordinate[0], coordinate[1]);
                        gameState.BonusesList.Add(blueberry);
                        gameState.DrawableObjects.Add(blueberry);
                        break;
                    case "cherry":
                        coordinate = GetCoordinate(data[i + 1]);
                        Cherry cherry = new Cherry(coordinate[0], coordinate[1]);
                        gameState.BonusesList.Add(cherry);
                        gameState.DrawableObjects.Add(cherry);
                        break;
                    case "pineapple":
                        coordinate = GetCoordinate(data[i + 1]);
                        Pineapple pineapple = new Pineapple(coordinate[0], coordinate[1]);
                        gameState.BonusesList.Add(pineapple);
                        gameState.DrawableObjects.Add(pineapple);
                        break;
                    case "bush":
                        coordinate = GetCoordinate(data[i + 1]);
                        Bush bush = new Bush(coordinate[0], coordinate[1]);
                        gameState.ObstaclesList.Add(bush);
                        gameState.DrawableObjects.Add(bush);
                        gameState.HittersList.Add(bush);
                        break;
                    default:
                        break;
                }
            }
        }

        public int[] GetCoordinate(string numbers)
        {
            int[] coordinate = new int[2];
            string[] string_coordinate = numbers.Split(",");
            coordinate[0] = int.Parse(string_coordinate[0]);
            coordinate[1] = int.Parse(string_coordinate[1]);
            return coordinate;
        }

        public void LoadPreviousGame(GameState gameState)
        {
             string[] data = File.ReadAllLines(Path.Combine(DataPath, SavingsName));
            for (int i = 0; i < data.Length; i++)
            {
                switch (data[i])
                {
                    case "map":
                        string[] mapCoordinate = data[i + 1].Split(",");
                        gameState.MapExample = new Map(int.Parse(mapCoordinate[0]), int.Parse(mapCoordinate[1]), gameState.ObstaclesList, gameState.BonusesList, gameState.MonstersList);
                        break;
                    case "player":
                        gameState.PlayerExample = JsonSerializer.Deserialize<Player>(data[i + 1]);
                        gameState.DrawableObjects.Add(gameState.PlayerExample);
                        gameState.ActorsList.Add(gameState.PlayerExample);
                        gameState.MapExample.player = gameState.PlayerExample;
                        break;
                    case "apple":
                        Apple apple = JsonSerializer.Deserialize<Apple>(data[i + 1]);
                        gameState.DrawableObjects.Add(apple);
                        gameState.BonusesList.Add(apple);
                        break;
                    case "blueberry":
                        Blueberry blueberry = JsonSerializer.Deserialize<Blueberry>(data[i + 1]);
                        gameState.DrawableObjects.Add(blueberry);
                        gameState.BonusesList.Add(blueberry);
                        break;
                    case "cherry":
                        Cherry cherry = JsonSerializer.Deserialize<Cherry>(data[i + 1]);
                        gameState.DrawableObjects.Add(cherry);
                        gameState.BonusesList.Add(cherry);
                        break;
                    case "pineapple":
                        Pineapple pineapple = JsonSerializer.Deserialize<Pineapple>(data[i + 1]);
                        gameState.DrawableObjects.Add(pineapple);
                        gameState.BonusesList.Add(pineapple);
                        break;
                    case "raspberry":
                        Raspberry raspberry = JsonSerializer.Deserialize<Raspberry>(data[i + 1]);
                        gameState.DrawableObjects.Add(raspberry);
                        gameState.BonusesList.Add(raspberry);
                        break;
                    case "bear":
                        Bear bear = JsonSerializer.Deserialize<Bear>(data[i + 1]);
                        gameState.DrawableObjects.Add(bear);
                        gameState.MonstersList.Add(bear);
                        gameState.HittersList.Add(bear);
                        gameState.ActorsList.Add(bear);
                        break;
                    case "wolf":
                        Wolf wolf = JsonSerializer.Deserialize<Wolf>(data[i + 1]);
                        gameState.DrawableObjects.Add(wolf);
                        gameState.MonstersList.Add(wolf);
                        gameState.HittersList.Add(wolf);
                        gameState.ActorsList.Add(wolf);
                        break;
                    case "fox":
                        Fox fox = JsonSerializer.Deserialize<Fox>(data[i + 1]);
                        gameState.DrawableObjects.Add(fox);
                        gameState.MonstersList.Add(fox);
                        gameState.HittersList.Add(fox);
                        gameState.ActorsList.Add(fox);
                        break;
                    case "bush":
                        Bush bush = JsonSerializer.Deserialize<Bush>(data[i + 1]);
                        gameState.DrawableObjects.Add(bush);
                        gameState.ObstaclesList.Add(bush);
                        gameState.HittersList.Add(bush);
                        break;
                    case "tree":
                        Tree tree = JsonSerializer.Deserialize<Tree>(data[i + 1]);
                        gameState.DrawableObjects.Add(tree);
                        gameState.ObstaclesList.Add(tree);
                        break;
                    case "stone":
                        Stone stone = JsonSerializer.Deserialize<Stone>(data[i + 1]);
                        gameState.DrawableObjects.Add(stone);
                        gameState.ObstaclesList.Add(stone);
                        break;
                    case "wall":
                        Wall wall = JsonSerializer.Deserialize<Wall>(data[i + 1]);
                        gameState.DrawableObjects.Add(wall);
                        gameState.ObstaclesList.Add(wall);
                        break;
                    default:
                        break;
                }
            }
        }

        public void LoadPreviousGameFromServer(string Nickname, GameState gameState)
        {
            int PlayerId = 0;
            int MapId = 0;

            List<SqlCommand> commands = new List<SqlCommand>();
            using SqlConnection connection = new SqlConnection(connectionString);

            string selectPlayer = $"SELECT [PlayerID], [Nickname], [Previous_X], [Previous_Y], [X], [Y], [Health], [IgnoreObstaclesCounter], [DoneStep], [PressedKey] FROM [Players] p WHERE p.Nickname = '{Nickname}'";
            SqlCommand selectPlayerCommand = new SqlCommand(selectPlayer, connection);
            string selectMap = $"SELECT [MapID], [Height], [Width], [LevelID], [StepsDone] FROM [Maps] m WHERE m.PLayerID = @PlayerId";
            SqlCommand selectMapCommand = new SqlCommand(selectMap, connection);
            string selectMonsters = $"SELECT [PassNextSteps], [HitRange], [Previous_X], [Previous_Y], [IgnoreObstaclesCounter], [DoneStep], [X], [Y], [Type] FROM [Monsters] m WHERE m.MapID = @MapID";
            SqlCommand selectMonstersCommand = new SqlCommand(selectMonsters, connection);
            string selectBonuses = $"SELECT [Energy], [Availiable], [X], [Y], [Type] FROM [Bonuses] b WHERE b.MapID = @MapID";
            SqlCommand selectBonusesCommand = new SqlCommand(selectBonuses, connection);
            string selectObstacles = $"SELECT [HitRange], [IsPassable], [X], [Y], [Type] FROM [Obstacles] o WHERE o.MapID = @MapID";
            SqlCommand selectObstaclesCommand = new SqlCommand(selectObstacles, connection);

            connection.Open();
            using (SqlDataReader reader = selectPlayerCommand.ExecuteReader())
            { 
                while (reader.Read())
                {
                    gameState.PlayerExample = new Player
                    {
                        Nickname = Nickname,
                        Previous_X = (int)reader["Previous_X"],
                        Previous_Y = (int)reader["Previous_Y"],
                        X = (int)reader["X"],
                        Y = (int)reader["Y"],
                        Health = (int)reader["Health"],
                        IgnoreObstaclesCounter = (int)reader["IgnoreObstaclesCounter"],
                        DoneStep = (bool)reader["DoneStep"],
                        Key = (ConsoleKey)reader["PressedKey"]
                    };
                    gameState.ActorsList.Add(gameState.PlayerExample);
                    gameState.DrawableObjects.Add(gameState.PlayerExample);
                    PlayerId = (int)reader["PlayerID"];
                }
            }
            if (gameState.PlayerExample == null)
                throw new Exception("No saves for this player");
            selectMapCommand.Parameters.AddWithValue("@PlayerId", PlayerId);
            using (SqlDataReader reader = selectMapCommand.ExecuteReader())
            {
                while (reader.Read())
                {
                    gameState.MapExample = new Map((int)reader["Height"], (int)reader["Width"], gameState.ObstaclesList, gameState.BonusesList, gameState.MonstersList)
                    {
                        LevelId = (int)reader["LevelID"],
                        player = gameState.PlayerExample
                    };
                    gameState.MapExample.Counter = (int)reader["StepsDone"];
                    MapId = (int)reader["MapID"];

                }
            }
            selectBonusesCommand.Parameters.AddWithValue("@MapID", MapId);
            using (SqlDataReader reader = selectBonusesCommand.ExecuteReader())
            {
                while (reader.Read())
                {
                    Bonus bonus = DefineBonusType((int)(byte)reader["Type"]);
                    bonus.Availiable = (bool)reader["Availiable"];
                    bonus.Energy = (int)reader["Energy"];
                    bonus.X = (int)reader["X"];
                    bonus.Y = (int)reader["Y"];
                    gameState.BonusesList.Add(bonus);
                    gameState.DrawableObjects.Add(bonus);
                }
            }
            selectMonstersCommand.Parameters.AddWithValue("@MapID", MapId);
            using (SqlDataReader reader = selectMonstersCommand.ExecuteReader())
            {
                while (reader.Read())
                {
                    Monster monster = DefineMonsterType((int)(byte)reader["Type"]);
                    monster.HitRange = (int)reader["HitRange"];
                    monster.PassNextSteps = (int)reader["PassNextSteps"];
                    monster.X = (int)reader["X"];
                    monster.Y = (int)reader["Y"];
                    monster.Previous_X = (int)reader["Previous_X"];
                    monster.Previous_Y = (int)reader["Previous_Y"];
                    monster.DoneStep = (bool)reader["DoneStep"];
                    monster.IgnoreObstaclesCounter = (int)reader["IgnoreObstaclesCounter"];
                    gameState.MonstersList.Add(monster);
                    gameState.DrawableObjects.Add(monster);
                    gameState.ActorsList.Add(monster);
                    gameState.HittersList.Add(monster);
                }
            }
            selectObstaclesCommand.Parameters.AddWithValue("@MapID", MapId);
            using (SqlDataReader reader = selectObstaclesCommand.ExecuteReader())
            {
                while (reader.Read())
                {
                    Obstacle obstacle = DefineObstacleType((int)(byte)reader["Type"]);
                    obstacle.HitRange = (int)reader["HitRange"];
                    obstacle.IsPassable = (bool)reader["IsPassable"];
                    obstacle.X = (int)reader["X"];
                    obstacle.Y = (int)reader["Y"];
                    gameState.ObstaclesList.Add(obstacle);
                    gameState.DrawableObjects.Add(obstacle);

                    var obstacleType = (BaseObject.ObjectTypes) Enum.Parse(typeof(BaseObject.ObjectTypes), obstacle.GetType().Name);
                    if (obstacleType == BaseObject.ObjectTypes.Bush)
                        gameState.HittersList.Add((Bush)obstacle);
                }
            }
        }

        public List<string> LoadStatistics()
        {
            using SqlConnection connection = new SqlConnection(connectionString);
            string selectStatistic = "SELECT [Statistics].[PlayerID], [Players].[Nickname], [Statistics].[DateTime], [Statistics].[Result], [Statistics].[LevelID] FROM[GameDb].[dbo].[Statistics] INNER JOIN[GameDb].[dbo].[Players] ON[Statistics].[PlayerID] = [Players].[PlayerID]";
            SqlCommand selectPlayerCommand = new SqlCommand(selectStatistic, connection);
            List<string> stats = new List<string>();
            connection.Open();
            using (SqlDataReader reader = selectPlayerCommand.ExecuteReader())
            {
                while (reader.Read())
                {
                    stats.Add($"Player {reader["Nickname"]} - {reader["DateTime"]} - Level {reader["LevelID"]} Result = {reader["Result"]}");
                }
            }
            return stats;
        }

        private Bonus DefineBonusType(int objectTypeNumber)
        {
            switch ((BaseObject.ObjectTypes)objectTypeNumber)       
            {
                case BaseObject.ObjectTypes.Apple:
                    return new Apple();
                case BaseObject.ObjectTypes.Blueberry:
                    return new Blueberry();
                case BaseObject.ObjectTypes.Cherry:
                    return new Cherry();
                case BaseObject.ObjectTypes.Pineapple:
                    return new Pineapple();
                case BaseObject.ObjectTypes.Raspberry:
                    return new Raspberry();
                default:
                    return null;
            }
        }
        private Obstacle DefineObstacleType(int objectTypeNumber)
        {
            switch ((BaseObject.ObjectTypes)objectTypeNumber)
            {
                case BaseObject.ObjectTypes.Bush:
                    return new Bush();
                case BaseObject.ObjectTypes.Stone:
                    return new Stone();
                case BaseObject.ObjectTypes.Tree:
                    return new Tree();
                case BaseObject.ObjectTypes.Wall:
                    return new Wall();
                default:
                    return null;
            }
        }
        private Monster DefineMonsterType(int objectTypeNumber)
        {
            switch ((BaseObject.ObjectTypes)objectTypeNumber)
            {
                case BaseObject.ObjectTypes.Bear:
                    return new Bear();
                case BaseObject.ObjectTypes.Fox:
                    return new Fox();
                case BaseObject.ObjectTypes.Wolf:
                    return new Wolf();
                default:
                    return null;
            }
        }

        public void LoadLevelFromServer(int level, string playerNickname, GameState gameState)
        {
            string[] levelData;
            List<SqlCommand> commands = new List<SqlCommand>();
            using SqlConnection connection = new SqlConnection(connectionString);
            string selectPlayer = $"SELECT [Data] FROM levels l WHERE l.LevelID = '{level}'";
            SqlCommand selectPlayerCommand = new SqlCommand(selectPlayer, connection);
            connection.Open();
            using (SqlDataReader reader = selectPlayerCommand.ExecuteReader())
            {
                while (reader.Read())
                {
                   levelData = reader["Data"].ToString().Split("\r\n");
                   for (int i = 0; i < levelData.Length; i++)
                    {
                        switch (levelData[i])
                        {
                            case "map":
                                int[] coordinate = GetCoordinate(levelData[i + 1]);
                                gameState.MapExample = new Map(coordinate[0], coordinate[1], gameState.ObstaclesList, gameState.BonusesList, gameState.MonstersList);
                                gameState.MapExample.LevelId = level;
                                break;
                            case "player":
                                coordinate = GetCoordinate(levelData[i + 1]);
                                gameState.PlayerExample = new Player(coordinate[0], coordinate[1], playerNickname);
                                gameState.DrawableObjects.Add(gameState.PlayerExample);
                                gameState.ActorsList.Add(gameState.PlayerExample);
                                gameState.MapExample.player = gameState.PlayerExample;
                                break;
                            case "wolf":
                                coordinate = GetCoordinate(levelData[i + 1]);
                                Wolf wolf = new Wolf(coordinate[0], coordinate[1]);
                                gameState.ActorsList.Add(wolf);
                                gameState.MonstersList.Add(wolf);
                                gameState.DrawableObjects.Add(wolf);
                                gameState.HittersList.Add(wolf);
                                break;
                            case "bear":
                                coordinate = GetCoordinate(levelData[i + 1]);
                                Bear bear = new Bear(coordinate[0], coordinate[1]);
                                gameState.ActorsList.Add(bear);
                                gameState.MonstersList.Add(bear);
                                gameState.DrawableObjects.Add(bear);
                                gameState.HittersList.Add(bear);
                                break;
                            case "fox":
                                coordinate = GetCoordinate(levelData[i + 1]);
                                Fox fox = new Fox(coordinate[0], coordinate[1]);
                                gameState.ActorsList.Add(fox);
                                gameState.MonstersList.Add(fox);
                                gameState.DrawableObjects.Add(fox);
                                gameState.HittersList.Add(fox);
                                break;
                            case "tree":
                                coordinate = GetCoordinate(levelData[i + 1]);
                                Tree tree = new Tree(coordinate[0], coordinate[1]);
                                gameState.ObstaclesList.Add(tree);
                                gameState.DrawableObjects.Add(tree);
                                break;
                            case "wall":
                                coordinate = GetCoordinate(levelData[i + 1]);
                                Wall wall = new Wall(coordinate[0], coordinate[1]);
                                gameState.ObstaclesList.Add(wall);
                                gameState.DrawableObjects.Add(wall);
                                break;
                            case "stone":
                                coordinate = GetCoordinate(levelData[i + 1]);
                                Stone stone = new Stone(coordinate[0], coordinate[1]);
                                gameState.ObstaclesList.Add(stone);
                                gameState.DrawableObjects.Add(stone);
                                break;
                            case "apple":
                                coordinate = GetCoordinate(levelData[i + 1]);
                                Apple apple = new Apple(coordinate[0], coordinate[1]);
                                gameState.BonusesList.Add(apple);
                                gameState.DrawableObjects.Add(apple);
                                break;
                            case "raspberry":
                                coordinate = GetCoordinate(levelData[i + 1]);
                                Raspberry raspberry = new Raspberry(coordinate[0], coordinate[1]);
                                gameState.BonusesList.Add(raspberry);
                                gameState.DrawableObjects.Add(raspberry);
                                break;
                            case "blueberry":
                                coordinate = GetCoordinate(levelData[i + 1]);
                                Blueberry blueberry = new Blueberry(coordinate[0], coordinate[1]);
                                gameState.BonusesList.Add(blueberry);
                                gameState.DrawableObjects.Add(blueberry);
                                break;
                            case "cherry":
                                coordinate = GetCoordinate(levelData[i + 1]);
                                Cherry cherry = new Cherry(coordinate[0], coordinate[1]);
                                gameState.BonusesList.Add(cherry);
                                gameState.DrawableObjects.Add(cherry);
                                break;
                            case "pineapple":
                                coordinate = GetCoordinate(levelData[i + 1]);
                                Pineapple pineapple = new Pineapple(coordinate[0], coordinate[1]);
                                gameState.BonusesList.Add(pineapple);
                                gameState.DrawableObjects.Add(pineapple);
                                break;
                            case "bush":
                                coordinate = GetCoordinate(levelData[i + 1]);
                                Bush bush = new Bush(coordinate[0], coordinate[1]);
                                gameState.ObstaclesList.Add(bush);
                                gameState.DrawableObjects.Add(bush);
                                gameState.HittersList.Add(bush);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
