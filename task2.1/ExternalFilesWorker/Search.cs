﻿using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ExternalFilesWorker
{
    public class Search
    {
        public static FileInfo[] GetAllFiles(string path)
        {
            DirectoryInfo info = new DirectoryInfo(path)
            {
                Attributes = FileAttributes.Normal
            };
            return info.GetFiles("*.txt", SearchOption.AllDirectories);
        }
    }
}
