﻿using System;

namespace Lib
{
    public class Tree: Obstacle
    {
        public Tree(int x, int y)
            : base(x, y)
        {
            IsPassable = false;
            HitRange = 0;
        }

        public Tree()
        {
            IsPassable = false;
            HitRange = 0;
        }

        public override void Draw()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.SetCursorPosition(X, Y);
            Console.Write("T");
        }
    }
}
