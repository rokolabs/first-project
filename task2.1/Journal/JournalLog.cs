﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace JournalMaker
{
    public class JournalLog
    {
        public string journalName = "HistoryLog.txt";
        public string firstLine = "program started";
        public string finalSeparator = "----";
        public string separator = "|";
        public bool isJournalFileCorrect;
        public string JournalPath { get; set; }
        public string SourcePath { get; set; }
        public List<JournalRecord> Records { get; set; }

        public JournalLog(string pathToJournal, string pathToSource)
        {
            Records = new List<JournalRecord>();
            JournalPath = pathToJournal;
            SourcePath = pathToSource;
        }
        public JournalLog(List<JournalRecord> records, string pathToJournal, string pathToSource)
        {
            Records = records;
            JournalPath = pathToJournal;
            SourcePath = pathToSource;
        }

        public void PrintJournal()
        {
            Console.WriteLine(firstLine);
            Console.WriteLine($"Length: {Records.Count}");
            for (int i = 0; i < Records.Count; i++)
            {
                Records[i].PrintJournalRecord();
            }
        }

        public void CreateFirstRecord(FileInfo[] filesInSource)
        {
            JournalRecord jr = new JournalRecord();
            jr.DateTimeProperty = DateTime.Now;
            List<FileInRecord> filesInRecord = new List<FileInRecord>();
            for (int i = 0; i < filesInSource.Length; i++)
            {
                filesInRecord.Add( new FileInRecord(GenerateLocalName("1", SourcePath, filesInSource[i].FullName), filesInSource[i].FullName));
            }
            jr.Files = filesInRecord;
            Records.Add(jr);
        }

        public void CreateRecordForCreatingFile(string fullName)
        {
            JournalRecord jr = new JournalRecord
            {
                DateTimeProperty = DateTime.Now
            };
            for (int i = 0; i < Records[^1].Files.Count; i++)
            {
                if (Records[^1].Files[i].Id == -1)
                    continue;
                jr.Files.Add(FileInRecord.CreateNotChangedFile(Records[^1].Files[i]));
            }
            string newFileLocalname = GenerateLocalName("1", SourcePath, fullName);
            newFileLocalname = GetCorrectFileName(newFileLocalname, JournalPath);
            jr.Files.Add(new FileInRecord(newFileLocalname, fullName));
            Records.Add(jr);
        }

        public void CreateRecordForRenamingFile(string previousFullName, string newFullname)
        {
            JournalRecord jr = new JournalRecord
            {
                DateTimeProperty = DateTime.Now
            };
            for (int i = 0; i < Records[^1].Files.Count; i++)
            {
                if (Records[^1].Files[i].Id == -1)
                    continue;
                if (Records[^1].Files[i].Fullname == previousFullName)
                {
                    jr.Files.Add(FileInRecord.CreateDeletedFile(Records[^1].Files[i]));
                    string newFileLocalname = GenerateLocalName("1", SourcePath, newFullname);
                    jr.Files.Add(new FileInRecord(newFileLocalname, newFullname));
                }
                else
                {
                    jr.Files.Add(FileInRecord.CreateNotChangedFile(Records[^1].Files[i]));
                }
            }
            Records.Add(jr);
        }

        public void CreateRecordForChangingFile(string fullname)
        {
            JournalRecord jr = new JournalRecord();
            jr.DateTimeProperty = DateTime.Now;
            for (int i = 0; i < Records[^1].Files.Count; i++)
            {
                if (Records[^1].Files[i].Id == -1)
                    continue;
                if (Records[^1].Files[i].Fullname == fullname)
                {
                    
                    jr.Files.Add(FileInRecord.CreateChangedFile(Records[^1].Files[i]));
                    Console.WriteLine(jr.Files[^1].Id);
                }
                else
                {
                    jr.Files.Add(FileInRecord.CreateNotChangedFile(Records[^1].Files[i]));
                }
            }
            Records.Add(jr);
        }

        public void CreateRecordForDeletingFile(string fullname)
        {
            JournalRecord jr = new JournalRecord();
            jr.DateTimeProperty = DateTime.Now;
            for (int i = 0; i < Records[^1].Files.Count; i++)
            {
                if (Records[^1].Files[i].Id == -1)
                    continue;
                if (Records[^1].Files[i].Fullname == fullname)
                {
                    jr.Files.Add(FileInRecord.CreateDeletedFile(Records[^1].Files[i]));
                }
                else
                {
                    jr.Files.Add(FileInRecord.CreateNotChangedFile(Records[^1].Files[i]));
                }
            }
            Records.Add(jr);
        }

        public static string GenerateFileInfo(string localName, string fullName, string change, string index = "1", string separator = "|")
        {
            return index + separator + localName + separator + fullName + separator + change;
        }

        public static string GenerateLocalName(string index, string sourcePath, string fullName)
        {
            string localName = index + "_" + fullName.Replace(sourcePath + "\\", "");
            localName = localName.Replace("\\", "_");
            return localName;
        }

        public static FileInfo[] CheckBackupFiles(string pathToBackup)
        {
            DirectoryInfo info = new DirectoryInfo(pathToBackup)
            {
                Attributes = FileAttributes.Normal
            };
            var files = info.GetFiles("*.txt");
            for (int i = 0; i < files.Length; i++)
            {
                Console.WriteLine(files[i].FullName);
            }
            return files;
        }

        public static string GetCorrectFileName(string nameForTest, string pathToBackup)
        {
            StringBuilder newFileLocalNameTest = new StringBuilder(nameForTest);
            FileInfo[] existingFiles = CheckBackupFiles(pathToBackup);
            while (IsThisFileExists(newFileLocalNameTest, existingFiles))
            {
                newFileLocalNameTest = newFileLocalNameTest.Insert(0, "1");
            }
            string finalFileName = newFileLocalNameTest.ToString();
            return finalFileName;
        }

        public static bool IsThisFileExists(StringBuilder fileName, FileInfo[] files)
        {
            for (int i = 0; i < files.Length; i++)
                if (fileName.ToString() == files[i].Name)
                    return true;
            return false;
        }

    }
}
