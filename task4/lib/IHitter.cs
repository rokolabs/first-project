﻿namespace Lib
{
    public interface IHitter
    {
        public int X { get; set; }
        public int Y { get; set; }
        public abstract void Hit (Player player);
    }
}
