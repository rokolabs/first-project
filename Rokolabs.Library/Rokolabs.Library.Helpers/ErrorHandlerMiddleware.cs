﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using Rokolabs.Library.Data.Exceptions;
using System;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;

namespace Rokolabs.Library.Helpers
{
    public class ErrorHandlerMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IWebHostEnvironment _env;
        public ErrorHandlerMiddleware (RequestDelegate next, IWebHostEnvironment env)
        {
            _next = next;
            _env = env;
        }
        public async Task Invoke(HttpContext context)
        {
            try 
            {
                await _next(context);
            }
            catch (Exception error)
            {
                var response = context.Response;
                var message = error?.Message;
                response.ContentType = "application/json";

                switch(error)
                {
                    case AppException e:
                        // custom application error
                        response.StatusCode = (int)e.statusCode;
                    break;
                    default:
                        //if (_env.IsDevelopment())
                        //{
                        //    throw;
                        //}
                        response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        message = "Smth went wrong";
                    break;
                }

                var result = JsonSerializer.Serialize(new { statusCode = response.StatusCode, message = message });
                await response.WriteAsync(result);
            }
        }
    }
}
