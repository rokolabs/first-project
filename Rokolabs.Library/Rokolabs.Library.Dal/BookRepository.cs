﻿using Dapper;
using Microsoft.Extensions.Logging;
using Rokolabs.Library.Dal.DTOs;
using Rokolabs.Library.Dal.Interfaces;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Rokolabs.Library.Dal
{
    public class BookRepository : IBookRepository
    {
        private readonly ILogger<BookRepository> _logger;
        private readonly DBOptions _options;

        public BookRepository(ILogger<BookRepository> logger, DBOptions options)
        {
            _logger = logger;
            _options = options;        
        }

        public void DeleteBook(int bookId)
        {
            using IDbConnection db = new SqlConnection(_options.ConnectionString);
            var query = "DELETE FROM [Books] WHERE [BookID] = @bookId";
            db.Execute(query, new { bookId });
        }

        public List<BookDto> GetAllBooks()
        {
            using IDbConnection db = new SqlConnection(_options.ConnectionString);
            return db.Query<BookDto>("GetAllBooks",
                commandType: CommandType.StoredProcedure
                ).ToList();
        }

        public List<BookDto> GetAllBooksDateAsc()
        {
            using IDbConnection db = new SqlConnection(_options.ConnectionString);
            return db.Query<BookDto>("GetAllBooksDateASC",
                commandType: CommandType.StoredProcedure
                ).ToList();
        }

        public List<BookDto> GetAllBooksDateDesc()
        {
            using IDbConnection db = new SqlConnection(_options.ConnectionString);
            return db.Query<BookDto>("GetAllBooksDateDESC",
                commandType: CommandType.StoredProcedure
                ).ToList();
        }

        public int InsertBook(BookDto book)
        {
            using IDbConnection db = new SqlConnection(_options.ConnectionString);
            var query = "INSERT INTO [Books] ([Title], [Description], [YearOfPublishing], [PagesCount], [ISBN], [CityID], [PublishingOfficeID]) VALUES (@title, @description, @yearofpublishing, @pagescount, @isbn, @cityid, @publishingofficeid); SELECT CAST(SCOPE_IDENTITY() as int);";
            return db.Query<int>(query, book).FirstOrDefault();
        }

        public List<BookDto> SearchBooks(BookDto book)
        {
            using IDbConnection db = new SqlConnection(_options.ConnectionString);
            return db.Query<BookDto>("GetBooksByTitle",
                new { book.Title },
                commandType: CommandType.StoredProcedure
                ).ToList();
        }

        public List<BookDto> SearchBooksByAuthor(AuthorDto author)
        {
            using IDbConnection db = new SqlConnection(_options.ConnectionString);
            return db.Query<BookDto>("GetBooksByAuthor",
                new { author.AuthorId },
                commandType: CommandType.StoredProcedure
                ).ToList();
        }

        public List<BookDto> SearchBooksByAuthorDateAsc(AuthorDto author)
        {
            using IDbConnection db = new SqlConnection(_options.ConnectionString);
            return db.Query<BookDto>("GetBooksByAuthorDateASC",
                new { author.AuthorId },
                commandType: CommandType.StoredProcedure
                ).ToList();
        }

        public List<BookDto> SearchBooksByAuthorDateDesc(AuthorDto author)
        {
            using IDbConnection db = new SqlConnection(_options.ConnectionString);
            return db.Query<BookDto>("GetBooksByAuthorDateDESC",
                new { author.AuthorId },
                commandType: CommandType.StoredProcedure
                ).ToList();
        }

        public List<BookDto> SearchBooksByPublishingOffice(string publishingOfficeName)
        {
            using IDbConnection db = new SqlConnection(_options.ConnectionString);
            return db.Query<BookDto>("GetBooksByPublishingOffice",
                new { publishingOfficeName = publishingOfficeName },
                commandType: CommandType.StoredProcedure
                ).ToList();
        }

        public List<BookDto> SearchBooksByPublishingOfficeDateAsc(string publishingOfficeName)
        {
            using IDbConnection db = new SqlConnection(_options.ConnectionString);
            return db.Query<BookDto>("GetBooksByPublishingOfficeDateASC",
                new { publishingOfficeName = publishingOfficeName },
                commandType: CommandType.StoredProcedure
                ).ToList();
        }

        public List<BookDto> SearchBooksByPublishingOfficeDateDesc(string publishingOfficeName)
        {
            using IDbConnection db = new SqlConnection(_options.ConnectionString);
            return db.Query<BookDto>("GetBooksByPublishingOfficeDateDESC",
                new { publishingOfficeName = publishingOfficeName },
                commandType: CommandType.StoredProcedure
                ).ToList();
        }

        public List<BookDto> SearchBooksDateAsc(BookDto book)
        {
            using IDbConnection db = new SqlConnection(_options.ConnectionString);
            return db.Query<BookDto>("GetBooksByTitleDateASC",
                new { book.Title },
                commandType: CommandType.StoredProcedure
                ).ToList();
        }

        public List<BookDto> SearchBooksDateDesc(BookDto book)
        {
            using IDbConnection db = new SqlConnection(_options.ConnectionString);
            return db.Query<BookDto>("GetBooksByTitleDateDESC",
                new { book.Title },
                commandType: CommandType.StoredProcedure
                ).ToList();
        }

        public int GetBookByISBN(BookDto book)
        {
            using IDbConnection db = new SqlConnection(_options.ConnectionString);
            var query = "SELECT [Books].[BookID] FROM [BOOKS] WHERE [Books].[ISBN] = @isbn";
            return db.Query<int>(query, book).FirstOrDefault();
        }
        public int GetBook(BookDto book)
        {
            using IDbConnection db = new SqlConnection(_options.ConnectionString);
            var query = "SELECT [Books].[BookID] FROM [BOOKS] WHERE [Books].[Title] = @title AND [Books].[YearOfPublishing] = @yearOfPublishing";
            return db.Query<int>(query, book).FirstOrDefault();
        }
    }
}
