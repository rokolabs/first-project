﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Rokolabs.Library.Bll.Interfaces;
using Rokolabs.Library.Data;
using System.Collections.Generic;

namespace Rokolabs.Library.UIWeb.Controllers
{
    public class PatentController : Controller
    {
        private readonly ILogger<PatentController> _logger;
        private readonly IPatentService _patentService;
        private readonly ICountryService _countryService;

        public PatentController(ILogger<PatentController> logger, IPatentService patentService, ICountryService countryService)
        {
            _logger = logger;
            _patentService = patentService;
            _countryService = countryService;
        }
        // GET: PatentController
        public ActionResult Index()
        {
            var patents = _patentService.GetAllPatents();
            return View(patents);
        }

        [HttpPost]
        public ActionResult Index(Patent patent)
        {
            List<Patent> patents;
            if (patent.Title == null)
                patents = _patentService.GetAllPatents();
            else
                patents = _patentService.SearchPatent(patent);
            return View(patents);
        }

        // GET: PatentController/Details/5
        public ActionResult Details(Patent patent)
        {
            return View(patent);
        }

        // GET: PatentController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PatentController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Patent patent)
        {
            try
            {
                patent.CountryID = CheckEntityInDatabase(patent.CountryName, _countryService);
                _patentService.InsertPatent(patent);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: PatentController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: PatentController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: PatentController/Delete/5
        public ActionResult Delete(Patent patent)
        {
            return View(patent);
        }

        // POST: PatentController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int patentID, IFormCollection collection)
        {
            try
            {
                _patentService.DeletePatent(patentID);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
        public int CheckEntityInDatabase(string name, IDefiningEntity entityRepostitory)
        {
            int id = entityRepostitory.FindThisEntity(name);
            if (id == 0)
            {
                id = entityRepostitory.InsertThisEntity(name);
            }
            return id;
        }
    }
}
