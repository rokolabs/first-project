﻿

namespace Rokolabs.Library.Bll.Interfaces
{
    public interface IDefiningEntity
    {
        int FindThisEntity(string name);
        int InsertThisEntity(string name);
    }
}
