﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using Rokolabs.Library.Bll.Interfaces;
using Rokolabs.Library.Dal.DTOs;
using Rokolabs.Library.Dal.Interfaces;
using Rokolabs.Library.Data;
using System.Collections.Generic;

namespace Rokolabs.Library.Bll
{
    public class NewspaperService: INewspaperService
    {
        private readonly ILogger<NewspaperService> _logger;
        private readonly INewspaperRepository _newspaperRepository;
        private readonly IPublishngOfficeRepository _publishngOfficeRepository;
        private readonly IMapper _mapper;
        public NewspaperService(ILogger<NewspaperService> logger, IMapper mapper, INewspaperRepository newspaperRepository, IPublishngOfficeRepository publishngOfficeRepository)
        {
            _logger = logger;
            _newspaperRepository = newspaperRepository;
            _publishngOfficeRepository = publishngOfficeRepository;
            _mapper = mapper;
        }

        public void DeleteNewspaper(int newspaperId)
        {
            _newspaperRepository.DeleteNewspaper(newspaperId);
        }

        public List<Newspaper> GetAllNewspapers()
        {
            return _mapper.Map<List<Newspaper>>(_newspaperRepository.GetAllNewspapers());
        }

        public List<Newspaper> GetAllNewspapersDateAsc()
        {
            return _mapper.Map<List<Newspaper>>(_newspaperRepository.GetAllNewspapersDateAsc());
        }

        public List<Newspaper> GetAllNewspapersDateDesc()
        {
            return _mapper.Map<List<Newspaper>>(_newspaperRepository.GetAllNewspapersDateDesc());
        }

        public int InsertNewspaper(Newspaper newspaper)
        {
            return _newspaperRepository.InsertNewspaper(_mapper.Map<NewspaperDto>(newspaper));
        }

        public List<Newspaper> SearchNewspaper(Newspaper newspaper)
        {
            return _mapper.Map<List<Newspaper>>(_newspaperRepository.SearchNewspapers(_mapper.Map<NewspaperDto>(newspaper)));
        }

        public List<Newspaper> SearchNewspaperByPublicationOffice(string publishingOfficeName)
        {
            return _mapper.Map<List<Newspaper>>(_newspaperRepository.SearchNewspapersByPublicationOffice(publishingOfficeName));
        }

        public List<Newspaper> SearchNewspaperByPublicationOfficeDateAsc(string publishingOfficeName)
        {
            return _mapper.Map<List<Newspaper>>(_newspaperRepository.SearchNewspapersByPublicationOfficeDateAsc(publishingOfficeName));
        }

        public List<Newspaper> SearchNewspaperByPublicationOfficeDateDesc(string publishingOfficeName)
        {
            return _mapper.Map<List<Newspaper>>(_newspaperRepository.SearchNewspapersByPublicationOfficeDateDesc(publishingOfficeName));
        }

        public List<Newspaper> SearchNewspaperDateAsc(Newspaper newspaper)
        {
            return _mapper.Map<List<Newspaper>>(_newspaperRepository.SearchNewspapersDateAsc(_mapper.Map<NewspaperDto>(newspaper)));
        }

        public List<Newspaper> SearchNewspaperDateDesc(Newspaper newspaper)
        {
            return _mapper.Map<List<Newspaper>>(_newspaperRepository.SearchNewspapersDateDesc(_mapper.Map<NewspaperDto>(newspaper)));
        }

        public Newspaper GetNewspaper(Newspaper newspaper)
        {
            return _mapper.Map<Newspaper>(_newspaperRepository.GetNewspaper(_mapper.Map<NewspaperDto>(newspaper)));
        }

        public Newspaper GetNewspaperByISSN(Newspaper newspaper)
        {
            return _mapper.Map<Newspaper>(_newspaperRepository.GetNewspaperByISSN(_mapper.Map<NewspaperDto>(newspaper)));
        }
    }
}
