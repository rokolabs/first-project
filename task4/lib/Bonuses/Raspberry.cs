﻿using System;

namespace Lib
{
    public class Raspberry : Bonus
    {
        public Raspberry(int x, int y)
            : base(x, y, 20)
        {
        }

        public Raspberry()
        {
            Energy = 20;
        }

        public override void Draw()
        {
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.SetCursorPosition(X, Y);
            Console.Write("o");
        }
    }
}
